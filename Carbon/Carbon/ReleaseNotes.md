### 1.5.3 ###
* Minor fixes.

### 1.5.2 ###
* Seamless 3.0.2.
* No comments label in detail text centered.

### 1.5.1 ###
* Group cells are slide to the beginning when feed refreshed.
* In detail, mma and mre ads are removed when a video is presented.

### 1.5.0 ###
* Seamless version 3.0.1.
* Group cell crash with ads are fixed.
* Interstitial presenting bug while a video player is presented is fixed.

### 1.4.4 ###
* Minor fixes.

### 1.4.3 ###
* When detail rotated, detail web view layout bug fixed.
* In feed, paging cell display bug fixed.
* News detail placeholder image updated.
* Navigation bar button item icons are template images.

### 1.4.2 ###
* Images are only load once in detail.
* Duplicate feed objects are eliminated while opening the detail.

### 1.4.1 ###
* News detail UI performance enhancements for swiping between detail screens.
* Left menu selection bug fix.
* Parallax ad bug fixes.
* Suggested news in detail rotation layout bug fix.
* News detail web view user interaction enabled.

### 1.4.0 ###
* Left menu type 4 added.
* Parallax ads can be integrated to feed.
* Collection view cell configuration and styling are moved to separate classes and can be customized in subclasses now.
* Unit test crash fix.
* Network request url builder class created.

### 1.3.2 ###
* Faulty seamless version 2.8 updated again.

### 1.3.1 ###
* Seamless updated to v.2.8.

### 1.3.0 ###
* Detail nibs are removed. Detail has a separate view class.
* Enhancements and bug fixes for group cell.

### 1.2.8 ###
* Seamless updated to v2.7.
* Interstitial presentation crash from feed fixed.
* Navigation bar customization method fixed. 
* Dropdown view can include the main category.

### 1.2.7 ###
* Detail comment collection view added.
* Minor fixes.

### 1.2.6 ###
* Seamless update v2.5.1.

### 1.2.5 ###
* GAITrackerProxy made public.

### 1.2.4 ###
* iOS7 comment layout fixed.
* iPad and iPhone check macros are made public.
* News detail css is updated for h1,h2 and h3.

### 1.2.3 ###
* Minor fixes in gallery.
* Minor fixes in iPad layout.

### 1.2.2 ###
* Seamless updated to version 2.5.
* Subcategory View is refactored to Dropdown View which allows further customization.
* Navigation bar related are moved to main thread to prevent layout bugs while pushing and popping.
* Orientation related fixes.
* Url scheme bug fix.
* Minor fixes in gallery.

### 1.2.1 ###
* Feed view controller conforms to flow layout delegate.

### 1.2.0 ###
* iPad support added.
* Group cell support added in feed.
* AFNetworking version updated.
* Web view loading once bug fixed.
* Navigation bar subcategory view can be customized.
* Left menu type 1 is refactored and cells can be customized to greater extend.

### 1.1.1 ###
* CBWebViewController sync loading bug fixed.
* Minor bug fix in gallery.
* Some methods and properties in info view controller, left menu 1 and news detail are made public.
* Gallery classes are made public.

### 1.1.0 ###
* Minor bug fixes and improvements.

### 1.0.1 ###
* Seamless bundle laoding with cocoapods bug fixed.
* Multiple ad loading bug in detail is fixed.

### 1.0.0 ###
* Initial relase.