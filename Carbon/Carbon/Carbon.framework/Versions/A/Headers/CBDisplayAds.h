//
//  CBDisplayAds.h
//  Carbon
//
//  Created by Tolga Caner on 16/03/16.
//  Copyright © 2016 mobilike. All rights reserved.
//


//#import "CBStandaloneModel.h"
#import "CBFallbackInterstitialModel.h"
#import "CBFeedAdModel.h"

@interface CBDisplayAds : CBBaseModel

@property (nonatomic,strong) CBStandaloneModel* standalone;
@property (nonatomic,strong) CBFeedAdModel* feed;
@property (nonatomic,strong) CBFallbackInterstitialModel* videoFallback;

@end
