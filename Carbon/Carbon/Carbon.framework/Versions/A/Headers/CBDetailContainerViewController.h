//
//  CBDetailContainerViewController.h
//  Carbon
//
//  Created by Necati Aydın on 19/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Utility.h"
#import "CBStartType.h"

@class CBDetailContainerLogic;


@interface CBDetailContainerViewController : UIPageViewController

@property (nonatomic, assign) StartType startType;
/**
 Logic class of the view controller.
 */
@property (nonatomic, strong) CBDetailContainerLogic *logic;

/**
 Index of the view controller to be shown.
 */
@property (assign, nonatomic) NSInteger currentIndex;

/**
 If swipe to dismiss is enabled for the app, this method can be used to switch on or off.
 */
- (void)enableNavigationBarSwipeToDismiss:(BOOL)enabled;

/**
 Paging can be enabled and disabled.
 */
- (void)enablePaging:(BOOL)enabled;

@end
