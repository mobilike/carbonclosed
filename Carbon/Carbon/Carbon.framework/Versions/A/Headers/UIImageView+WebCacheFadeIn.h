//
//  UIImageView+WebCacheFadeIn.h
//  Carbon
//
//  Created by Semih Cihan on 30/09/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface UIImageView (WebCacheFadeIn)

@end
