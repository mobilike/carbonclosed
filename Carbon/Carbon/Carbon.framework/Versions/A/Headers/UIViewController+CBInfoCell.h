//
//  UIViewController+CBInfoLabelCell.h
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBInfoCell.h"

@interface UIViewController (CBInfoCell)

- (void)configureCell:(CBInfoCell *)cell text:(NSString *)text;

@end
