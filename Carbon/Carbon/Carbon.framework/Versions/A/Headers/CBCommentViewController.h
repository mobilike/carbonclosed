//
//  CBCommentViewController.h
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import "CBCommentLogic.h"

@class CBCommentCell;

@interface CBCommentViewController : CBBaseViewController <CBDataLoaderDelegate>

@property (strong, nonatomic) CBCommentLogic *logic;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

//collection view

- (CBCommentCell *)reusableCommentCellForIndexPath:(NSIndexPath *)indexPath;
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;;
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

@end
