//
//  CBTestManifestDataModel.h
//  Pods
//
//  Created by Tolga Caner on 05/04/16.
//
//

#import "CBBaseModel.h"

/**
 @class CBTestManifestDataModel
 @superclass SuperClass: CBBaseModel\n
 @discussion A simple model for test manifest meta info.
 */
@interface CBTestManifestDataModel : CBBaseModel
/**
 @brief Name of the manifest
 */
@property (nonatomic, strong) NSString* name;
/**
 @brief URL of the manifest.json
 */
@property (nonatomic, strong) NSString* url;

@end