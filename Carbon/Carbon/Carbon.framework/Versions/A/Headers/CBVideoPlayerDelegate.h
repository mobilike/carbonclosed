//
//  CBVideoPlayerDelegate.h
//  Carbon
//
//  Created by Tolga Caner on 13/04/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol CBVideoPlayerDelegate <NSObject>

@required
- (void)seekToTime:(CMTime)time;
-(float)videoPlayerRate;
-(void)playCommand;
-(void)pauseCommand;
-(void)resumeAdsManager;
-(void)pauseAdsManager;
-(void) showCentralPlayButton;
-(void) hideCentralPlayButton;

@end
