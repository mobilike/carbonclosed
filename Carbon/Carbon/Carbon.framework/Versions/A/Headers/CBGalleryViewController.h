//
//  CBGalleryViewController.h
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import "CBDetailContainerViewController.h"
#import "CBImageViewController.h"

@class CBGalleryDetailModel, CBGalleryViewController,CBGalleryDetailStyleConfig, CBGalleryContentViewController, CBNonDiagonalScrollView;

@protocol CBGalleryViewControllerDelegate <NSObject>

/**
 Returns the number of photos to be displayed in the gallery.
 */
- (NSInteger)numberOfPhotosInGalleryViewController:(CBGalleryViewController *)galleryViewController;


@optional

/**
 Returns a custom view controller at the given index (Such as an ad view). It overrides all the other behaviour. 
 @warning numberOfPhotosInGalleryViewController: should take the custom view(s) into account, it does not automatically add the custom view count to the total count.
 */
- (CBGalleryContentViewController *)galleryViewController:(CBGalleryViewController *)galleryViewController customViewControllerAtIndex:(NSInteger)index;

/**
 Returns the image url of the photo.
 @warning If galleryViewController:imageAtIndex: is not implemented in the class this method must be implemented. If both are implemented galleryViewController:imageAtIndex: is used.
 */
- (NSURL *)galleryViewController:(CBGalleryViewController *)galleryViewController photoUrlAtIndex:(NSInteger)index;
/**
 Returns the image for the given index.
 @warning If galleryViewController:photoUrlAtIndex is not implemented, this method must be implemented. If both are implemented, this method has the greater priority.
 */
- (UIImage *)galleryViewController:(CBGalleryViewController *)galleryViewController imageAtIndex:(NSInteger)index;

/**
 Should return the description string under the gallery image. If this method is not implemented, or returns nil, description for the given index is not shown.
 */
- (NSString *)galleryViewController:(CBGalleryViewController *)galleryViewController
            photoDescriptionAtIndex:(NSInteger)index;

/**
 Called before activity controller is displayed. 
 @warning You do not need to handle activity view controller or popover view controller behaviour here, it is implemented in the gallery.
 */
- (void)galleryViewControllerTappedShareButton:(CBGalleryViewController *)galleryViewController;

/**
 The default title while sharing.
 */
- (NSString *)galleryViewControllerShareTitle:(CBGalleryViewController *)galleryViewController;

/**
 The url to share.
 */
- (NSURL *)galleryViewControllerShareUrl:(CBGalleryViewController *)galleryViewController;

/**
 To override the default close button action, implement this method.
 */
- (void)galleryViewControllerTappedCloseButton:(CBGalleryViewController *)galleryViewController;

/**
 Called when the gallery is closed with upward or downward swipe gesture.
 */
- (void)galleryViewControllerWillCloseWithSwipeGesture:(CBGalleryViewController *)galleryViewController;

/**
 Called when the gallery is closed by tapping close button;
 */
- (void)galleryViewControllerWillCloseWithCloseButton:(CBGalleryViewController *)galleryViewController;

@end



@interface CBGalleryViewController : CBBaseViewController <CBImageViewControllerDelegate>

/**
 Outer scroll view of the gallery.
 */
@property (weak, nonatomic) IBOutlet CBNonDiagonalScrollView *scrollView;

/**
 Warning Can be set before the view is presented. In other cases it should be read only.
 */
@property (assign, nonatomic) BOOL navigationBarEnabled;

/**
 Warning Can be set before the view is presented. In other cases it should be read only.
 */
@property (assign, nonatomic) NSInteger currentPage;

/**
 Warning Can be set before the view is presented. In other cases it should be read only.
 */
@property (strong, nonatomic) UIImage *backgroundImage;

/**
 Gallery view controller delegate.
 */
@property (nonatomic, strong) id<CBGalleryViewControllerDelegate> delegate;

/**
 Contains the content view controllers.
 */
@property (nonatomic, strong) NSMutableArray *contentViewControllers;

/**
 Removes the view and loads the data starting from the current index.
 */
- (void)reload;


/**
 The view in the top. It does not contain any UI elements except a separator. It just acts like a background view.
 */
@property (weak, nonatomic) IBOutlet UIView *topBarView;

/**
 The label which shows image count.
 */
@property (weak, nonatomic) IBOutlet UILabel *imageCountLabel;

/**
 Close button.
 */
@property (weak, nonatomic) IBOutlet UIButton *backButton;

/**
 The view which acts as a background view for the complete view. It is important in rotations. It covers the extra spaces during rotation.
 @warning It is initialized in viewDidLoad.
 */
@property (strong, nonatomic) UIView *blackView;

/**
 The separator view contained in the topBarView;
 @warning It is initialized in viewDidLoad.
 */
@property (strong, nonatomic) UIView *separator;

/**
 Font of the photo description label.
 */
@property (strong, nonatomic) UIFont *photoDescriptionFont;

/**
 Color of the photo description label.
 */
@property (strong, nonatomic) UIColor *photoDescriptionColor;

/**
 Color of the top bar and photo description bars.
 */
@property (strong, nonatomic) UIColor *topBarColor;

/**
 Share button can be in the navigation bar or in the custom bar. When called, this method ads or show the share button.
 */
- (void)showShareButton:(BOOL)show;

@end

