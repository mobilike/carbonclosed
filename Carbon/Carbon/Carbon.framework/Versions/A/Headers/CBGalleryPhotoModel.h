//
//  CBGalleryPhotoModel.h
//  Carbon
//
//  Created by Semih Cihan on 27/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"

@interface CBGalleryPhotoModel : CBBaseModel

@property (strong, nonatomic) NSString *photoId;
@property (strong, nonatomic) NSString *photoDescription;
@property (strong, nonatomic) NSURL *imageUrl;

- (BOOL)hasDescription;

@end
