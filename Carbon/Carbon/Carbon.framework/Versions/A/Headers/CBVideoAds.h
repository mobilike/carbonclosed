//
//  CBVideoAds.h
//  Pods
//
//  Created by Tolga Caner on 18/04/16.
//
//

#import "CBVideoAds.h"
#import "CBVideoRollAd.h"

/**
 @class CBVideoAds
 @superclass CBBaseModel
 @brief Model that contains information on a video ad sequence to be played for a specific content.
 */
@interface CBVideoAds : CBBaseModel

/**
 @brief The list of ad categories this video ad bundle is compatible with.
 */
@property (nonatomic, strong) NSArray* adCategory;
/**
 @brief The pre-roll video ad object, plays before the content starts playing
 */
@property (nonatomic, strong) CBVideoRollAd* preroll;
/**
 @brief The mid-roll video ad object, that is to be played during the content playback
 */
@property (nonatomic, strong) CBVideoRollAd* midroll;
/**
 @brief The post-roll video ad object, plays before the content starts playing
 */
@property (nonatomic, strong) CBVideoRollAd* postroll;

@end
