//
//  CBGAITrackerProxy.h
//  Carbon
//
//  Created by Necati Aydın on 14/10/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GAI.h"

@interface CBGAITrackerProxy : NSProxy <GAITracker>

- (id)initWithTrackingIDs:(NSArray *)trackingIDs;

@end
