//
//  CBPagingCell.h
//  Carbon
//
//  Created by Semih Cihan on 26/10/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"

@interface CBPagingCell : CBBaseCollectionViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

+ (CGSize)sizeForWidth:(CGFloat)width;



@end
