//
//  CBParallaxCell.h
//  Carbon
//
//  Created by Necati Aydın on 07/12/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"

@interface CBParallaxCell : CBBaseCollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIImageView *frameImageView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageViewCenterY;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageViewAspectRatio;

+ (NSString *)reuseIdentifier;
+ (CGFloat)maxParallaxOffsetForCollectionViewSize:(CGSize)collectionViewSize cellSizeRatio:(CGFloat)cellSizeRatio;

- (void)updateImageViewAspectRatio:(CGFloat)aspectRatio;



@end
