//
//  CBCollectionViewCellStyler.h
//  Carbon
//
//  Created by Necati Aydın on 19/01/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBGroupNewsCell, CBFeedCell, CBPagingCell, CBCommentCell, CBBaseModel, CBGroupNewsFeedModel, CBFeedModel, CBCommentModel, CBFeed4Cell, CBBaseCollectionViewCell;
/**
 @class CBCollectionViewCellStyler
 @superclass SuperClass: NSObject\n
 @description A manager to handle cell stylings.
 */
@interface CBCollectionViewCellStyler : NSObject

/**
 @brief Static singleton access to the CBCollectionViewCellStyler
 @return CBCollectionViewCellStyler singleton object
 */
+ (instancetype)sharedInstance;

/**
 @discussion Styles a given cell of CBBaseCollectionViewCell type for a given model.
 @param cell CBBaseCollectionViewCell object to be styled.
 @param model to fill the cell after styling.
 */
- (void)styleCell:(CBBaseCollectionViewCell *)cell withModel:(CBBaseModel *)model;

/**
 @discussion Styles a given cell of CBGroupNewsCell type for a given CBGroupNewsFeedModel type model.
 @param cell CBGroupNewsCell object to be styled.
 @param groupNewsFeedModel to fill the cell after styling.
 */
- (void)styleGroupCell:(CBGroupNewsCell *)cell withGroupNewsFeedModel:(CBGroupNewsFeedModel *)groupNewsFeedModel;

/**
 @discussion Styles a given cell of CBFeedCell type for a given CBFeedModel type model.
 @param cell CBFeedCell object to be styled.
 @param feedModel to fill the cell after styling.
 */
- (void)styleFeedCell:(CBFeedCell *)cell withFeedModel:(CBFeedModel *)feedModel;

/**
 @discussion colors the cell's activity indicator color with feedPagingActivityIndicatorColor from CBAppConfig
 @param cell CBPagingCell object to be styled.
 */
- (void)stylePagingCell:(CBPagingCell *)cell;

/**
 @discussion Styles a given cell of CBCommentCell type for a given CBCommentModel type model.
 @param cell CBCommentCell object to be styled.
 @param commentModel to fill the cell after styling.
 */
- (void)styleCommentCell:(CBCommentCell *)cell withCommentModel:(CBCommentModel *)commentModel;

/**
 @discussion Styles a given cell of CBFeed4Cell type for a given CBFeedModel type model.
 @param cell CBFeed4Cell object to be styled.
 @param feedModel to fill the cell after styling.
 */
- (void)styleSuggestedVideoCell:(CBFeed4Cell *)cell withFeedModel:(CBFeedModel *)feedModel;

@end
