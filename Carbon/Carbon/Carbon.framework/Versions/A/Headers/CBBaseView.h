//
//  CBBaseView.h
//  Carbon
//
//  Created by Necati Aydın on 04/01/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBBaseView : UIView

- (void)loadSubviews;
- (void)addSubviews;
- (void)addLayoutConstraints;

@end
