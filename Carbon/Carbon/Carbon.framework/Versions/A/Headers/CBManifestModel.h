//
//  CBManifestModel.h
//  Carbon
//
//  Created by Tolga Caner on 16/03/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "CBDataModel.h"

@interface CBManifestModel : CBBaseModel

@property (nonatomic,strong) NSNumber* code;
@property (nonatomic,strong) NSString* message;
@property (nonatomic,strong) CBDataModel* data;

-(void)persist;
+ (id)loadPersistentManifest;
@end
