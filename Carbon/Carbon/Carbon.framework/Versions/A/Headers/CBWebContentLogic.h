//
//  CBWebContentLogic.h
//  Carbon
//
//  Created by Fırat Karataş on 14/07/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@class CBCategoryModel;

@interface CBWebContentLogic : CBBaseLogic

@property (strong, nonatomic) CBCategoryModel *category;

@property (nonatomic, strong) CBFeedModel *feed;

@end
