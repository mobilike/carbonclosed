//
//  CBVideoPremiumStrategy.h
//  Pods
//
//  Created by Tolga Caner on 10/05/16.
//
//

#import <Foundation/Foundation.h>
#import "CBVideoPlayerViewController.h"
#import "CBVideoStrategy.h"

@interface CBVideoPremiumStrategy : NSObject <CBVideoStrategy> {}

- (instancetype) initWithVideoPlayerViewController:(CBVideoPlayerViewController*)viewController;

@property (weak,nonatomic) CBVideoPlayerViewController* viewController;

@end
