//
//  CBFirstInterstitialDelegate.h
//  Pods
//
//  Created by Tolga Caner on 05/08/16.
//
//

#import <Foundation/Foundation.h>

/**
 @protocol CBFirstInterstitialDelegate
 @discussion Protocol that is called when the first interstitial of the app is loaded. The first interstitial ad is a special one that needs to be displayed after the splash animation and before the first feed of the app is shown, thus the need for such an event raise.
 */
@protocol CBFirstInterstitialDelegate <NSObject>
/**
 @brief Called when the first interstitial is loaded.
 */
- (void) gotFirstInterstitial;

@end
