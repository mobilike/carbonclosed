//
//  CBAnalyticsManager.h
//  Carbon
//
//  Created by Semih Cihan on 19/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "CBFeedModel.h"
#import "Flurry.h"
#import "GAIFields.h"
#import "gemius_mobile_plugin.h"
#import "CBNewsDetailModel.h"
#import "CBGalleryDetailModel.h"
#import "CBStartType.h"
#import "CBAppConfig.h"
#import "CBGAITrackerProxy.h"

static NSString * const kActionCategory = @"ui_action";
static NSString * const kActionShare = @"share_press";
static NSString * const kActionSendComment = @"send_comment_press";
static NSString * const kActionGallerySwipeClose = @"gallery_swipe_close";
static NSString * const kActionGallerySwipePage = @"gallery_swipe_page";
static NSString * const kActionGalleryButtonClose = @"gallery_button_close";
static NSString * const kActionVideoPlay = @"video_play_press : %@";
static NSString * const kActionVideoPlayWithoutCategory = @"video_play_press";
static NSString * const kActionPush = @"push_press";
static NSString * const kActionSlide = @"Slide";
static NSString * const kActionComment = @"Comment";
static NSString * const kActionCommentWrite = @"WriteComment";
static NSString * const kFeedDetailCategory = @"feed_detail";
static NSString * const kFeedCategory = @"feed";
static NSString * const kFeedDetailAction = @"Feed Category : %@ - News Category : %@ - Detail Type : %@";
static NSString * const kFeedDetailActionWithoutDetail = @"Feed Category : %@ - Detail Type : %@";
static NSString * const kFeedDetailScreen = @"%@ - %@ - %@";
static NSString * const kFeedDetailScreenExtra = @"%@ - %@ - %@ - %@";
static NSString * const kAction = @"action";
static NSString * const kFromPushNotification = @"Push Notification";
static NSString * const kFromSuggested = @"Suggested";
static NSString * const kFromSlider = @"Slider";
static NSString * const kFromURL = @"URL Scheme";

typedef NS_ENUM(NSUInteger, TrackedScreen) {
    TrackedScreenInfo,
    TrackedScreenSettings
};

@interface CBAnalyticsManager : NSObject

/**
 *
 *  @return Shared instance of the class.
 */
+ (instancetype)sharedInstance;

@property (assign, nonatomic) BOOL isGAEnabled;
@property (assign, nonatomic) BOOL isFlurryEnabled;
@property (assign, nonatomic) BOOL isGemiusEnabled;

/**
 GAITracker proxy. Use this one instead of [[GAI sharedInstance] defaultTracker] to use multiple tracking ids.
 */
@property (nonatomic, strong) CBGAITrackerProxy *trackerProxy;


/**
 Start analytics tracking. Uses the tracking id values in AppConfig.plist
 */
- (void)startTracking;

/**
 *  Detail screen name, set when a detail page is opened.
 */
@property (strong, nonatomic) NSString *detailScreenName;

/**
 Track share button tapped action. Category is set to "ui_action", action is set to "share_press".
 */
- (void)shareButtonTapped;

/**
 Track video play button tapped action. Category is set to "ui_action", action is set to "video_play_press : videoCategory"
 @param mode News detail model.
 */
- (void)videoPlayButtonTappedWithNewsDetaillModel:(CBNewsDetailModel *)model;

/**
 *  Track news detail page opening.
 *
 *  @param model     News detail model to be used to get data.
 *  @param startType Indicates how the detail page is opened.
 */
- (void)newsDetailScreenWithModel:(CBNewsDetailModel *)model
                        startType:(StartType)startType;

/**
 *  Track video detail page opening.
 *
 *  @param model     Video detail model to be used to get data.
 *  @param startType Indicates how the detail page is opened.
 */
- (void)videoDetailScreenWithModel:(CBNewsDetailModel *)model
                         startType:(StartType)startType;

/**
 *  Track video detail page opening.
 *
 *  @param model      Gallery detail model to be used to get data.
 *  @param feedTitle  Gallery feed title.
 *  @param startType  Indicates how the detail page is opened.
 */
- (void)galleryDetailScreenWithModel:(CBGalleryDetailModel *)model
                           feedTitle:(NSString *)feedTitle
                           startType:(StartType)startType;

/**
 *  Track news detail web page opening.
 *
 *  @param model     Feed model to be used to get data.
 *  @param startType Indicates how the detail page is opened.
 */
- (void)webDetailScreenWithModel:(CBFeedModel *)model
                       startType:(StartType)startType;

/**
 Track feed screen.
 */
- (void)feedScreen;

/**
 *  Track comment screen.
 */
- (void)commentScreen;

/**
 *  Track write comment screen.
 */
- (void)commentWriteScreen;

/**
 *  Send comment action.
 */
- (void)commentSendAction;

/**
 Gallery swipe to dismiss action.
 */
- (void)galleryCloseActionWithSwipe;

/**
 Gallery close button action.
 */
- (void)galleryCloseActionWithButton;

/**
 Gallery page change action.
 */
- (void)galleryPageChangeActionWithSwipe;

/**
 *  Track push actions.
 *
 *  @param title Title of the push.
 */
- (void)pushActionWithTitle:(NSString *)title;

/**
 
 */
- (void)slideActionWithTitle:(NSString *)title index:(NSInteger)index;

/**
 Used to set the category for a search to be used in analytics.
 @param category Category of the search.
 */
+ (void)setSearchCategory:(CBCategoryModel *)category;

/**
 Track screens with static names.
 @param screenType Screen to be sent to analytics.
 */
- (void)trackScreen:(TrackedScreen)screenType;

@end
