//
//  CBAdDetailsModel.h
//  Carbon
//
//  Created by Tolga Caner on 16/03/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <Carbon/Carbon.h>

@interface CBAdDetailsModel : CBBaseModel

@property (nonatomic,strong) NSString* adType;
@property (nonatomic,strong) NSString* adSize;
@property (nonatomic,strong) NSString* adUnitId;

@end
