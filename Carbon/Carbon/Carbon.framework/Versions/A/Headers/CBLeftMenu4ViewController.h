//
//  CBLeftMenu4ViewController.h
//  Carbon
//
//  Created by Semih Cihan on 08/01/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import "CBLeftMenu.h"
#import "CBLeftMenu4Logic.h"

@class CBCategoryModel;

@interface CBLeftMenu4ViewController : CBBaseViewController <CBLeftMenuViewControllerProtocol, UITableViewDataSource, UITableViewDelegate, CBLeftMenu4LogicDelegate>

@property (weak, nonatomic) id<CBLeftMenuContainedDelegate> leftMenuViewControllerDelegate;
@property (strong, nonatomic) CBLeftMenu4Logic *logic;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableViewRightSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
- (IBAction)infoButtonTapped:(id)sender;
- (IBAction)settingsButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingsButtonTrailingMarginConstraint;

- (void)dataLoaded;

- (NSArray *)cellClassesToBeRegistered;
- (UITableViewCell *)tableView:(UITableView *)tableView defaultCellForRowAtIndexPath:(NSIndexPath *)indexPath cellLevel:(NSInteger)cellLevel;

- (void)style;
- (void)styleCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath cellLevel:(NSInteger)cellLevel category:(CBCategoryModel *)category;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath cellLevel:(NSInteger)cellLevel category:(CBCategoryModel *)category;

@end