//
//  AppDelegate.h
//  CarbonApp
//
//  Created by Necati Aydın on 29/12/14.
//  Copyright (c) 2014 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBStartType.h"

#if defined(DEBUG) || defined(SEAMLESS_TEST) || defined(SEAMLESSTEST)
#import "FLEXManager.h"
#endif

#if defined(DEBUG) || defined(SEAMLESS_TEST) || defined(SEAMLESSTEST)
#import "FLEXManager.h"
#endif

@class CBPushModel;

@interface CBAppDelegate : UIResponder <UIApplicationDelegate>

/**
 @brief The main window of the app
 */
@property (strong, nonatomic) UIWindow *window;
/**
 @brief When set YES, only allows Portrait orientation
 */
@property (assign,nonatomic) BOOL restrictDeviceOrientationPortrait;
/**
 @brief When set YES, only allows Landscape orientation
 */
@property (assign,nonatomic) BOOL restrictDeviceOrientationLandscapeAll;
/**
 *  Starts the app with a push
 *
 *  @param pushModel Push model to start the app with.
 *  @param startType Start type of the view controller to be opened. // URL Scheme or Push etc. needed for analytics
 *  @param applicationOffline Application is offline or not when the push comes.
 */
- (void)openViewControllerWithPushModel:(CBPushModel *)pushModel startType:(StartType)startType applicationOffline:(BOOL)applicationOffline;

/**
 *  Dismisses all the view controllers but the root if the root is not modified and the default root is used.
 */
- (void)dismissViewControllers;

/**
 *  returns a value if the orientation of the device is restricted at the time, else returns nil.
 */
- (UIInterfaceOrientationMask) restrictedOrientation;

@end

