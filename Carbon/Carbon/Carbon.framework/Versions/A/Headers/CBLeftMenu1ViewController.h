//
//  CBLeftMenuViewController.h
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu.h"
#import "CBLeftMenu1View.h"
#import "CBBaseViewController.h"

@class CBLeftMenu1View;

/**
 Type 1 left menu.
 */
@interface CBLeftMenu1ViewController : CBBaseViewController <CBLeftMenuViewControllerProtocol,CBLeftMenu1ViewDelegate, CBLeftMenuStylingDelegate>

#pragma mark - UI

/**
 The back button at the top of the menu.
 */
@property (nonatomic, weak) IBOutlet UIButton *backButton;

/**
 The separator view between the menu and the back button.
 */
@property (nonatomic, weak) IBOutlet UIView *separatorView;

/**
 App icon image view positioned on the top of the menu.
 */
@property (weak, nonatomic) IBOutlet UIImageView *appIconImageView;

/**
 Left menu view.
 */
@property (nonatomic, weak) IBOutlet CBLeftMenu1View *leftMenuView;

/**
 The button that contains the category title.
 */
@property (nonatomic, weak) IBOutlet UIButton *headerTitleButton;

/**
 Settings button in the menu.
 */
@property (nonatomic, weak) IBOutlet UIButton *settingsButton;

/**
 Info button in the menu.
 */
@property (nonatomic, weak) IBOutlet UIButton *infoButton;

/**
 The back button in the menu's left space.
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *menuBackButtonLeftSpace;

/**
 Bottom space constraint for the settings button.
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *settingsButtonBottomSpace;

/**
 Left space constraint for the settings button.
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *settingsButtonLeftSpace;

/**
 Menu center distance to general view. Default value is -20;
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *leftMenuVerticalCenterDistance;

/**
 Scroll view height constraint.
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollViewHeight;

/**
 Scroll view right space to view constraint.
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollViewRightSpace;

#pragma mark - Styling

/**
 General styling method of the menu.
 */
- (void)style;

/**
 Styling method of the menu cell.
 */
- (void)styleLeftMenuTableViewCell:(CBLeftMenu1TableViewCell *)leftMenuTableViewCell;

#pragma mark - Data

/**
 The array of categories that shows the path of the selection in the menu.
 */
@property (nonatomic, strong) NSMutableArray *selectedCategories;

/**
 Should be called when categories are loaded.
 */
- (void)dataLoaded;


#pragma mark - Left Menu 1 Delegate

- (void)leftMenuViewDidSelectFirstRow:(CBLeftMenu1View *)leftMenuView;
- (void)leftMenuView:(CBLeftMenu1View *)leftMenuView pageDisplayed:(NSInteger)page scrollingDirection:(CBLeftMenu1ViewScrollingDirection)direction;

@end
