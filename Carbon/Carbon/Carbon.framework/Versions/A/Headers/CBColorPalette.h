//
//  CBColorPalette.h
//  Carbon
//
//  Created by Fırat Karataş on 04/09/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 @class CBColorPalette
 @superclass SuperClass: NSObject\n
 @discussion The singleton class that manages your app's main colors schemes. 
 */
@interface CBColorPalette : NSObject
/**
 @brief Static singleton access to the color palette
 @return CBColorPalette singleton object
 */
+ (instancetype)sharedPalette;

/**
 @brief Used for feel cell's title color, feed cell's spot color, feel cell's date color, info screen-bottom bar's text color, info screen-section header color and some of the more obscure labels throughout the app. Can always be overridden at the instance-level.
 */
@property (nonatomic, strong) UIColor *primaryTextColor;
/**
 @brief Used for feel cell's category label text color and comment cell's detail label text color. Can always be overridden at the instance-level.
 */
@property (nonatomic, strong) UIColor *secondaryTextColor;
/**
 @brief Not used by Carbon framework directly, but still open to use for apps.
 */
@property (nonatomic, strong) UIColor *color1 DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but apps using carbon still use it.
 */
@property (nonatomic, strong) UIColor *color2 DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but apps using carbon still use it.
 */
@property (nonatomic, strong) UIColor *color3 DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but apps using carbon still use it.
 */
@property (nonatomic, strong) UIColor *color4 DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but apps using carbon still use it. Black Color
 */
@property (nonatomic, strong) UIColor *color5 DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but apps using carbon still use it. White Color
 */
@property (nonatomic, strong) UIColor *color6 DEPRECATED_ATTRIBUTE;

@end
