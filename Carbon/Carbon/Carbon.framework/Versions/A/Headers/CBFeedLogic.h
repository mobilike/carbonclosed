//
//  CBHomeLogic.h
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@class CBCategoryModel, CBFeedConfigm, CBParallaxAdModel;


#pragma mark - CBDataLoaderWithPagingAndRefreshProtocol

@protocol CBDataLoaderWithPagingAndRefreshProtocol <CBDataLoaderProtocol>

/**
 Call to initiate refreshing.
 */
- (void)refreshData;

/**
 Call to initiate data loading of the next page.
 @return Yes if there are more data to load, No otherwise.
 */
- (BOOL)loadNextPage;

/**
 @description NO if there is no more data to load, YES otherwise.
 */
@property (assign, nonatomic, readonly) BOOL hasMorePageToLoad;

/**
 @description YES if loading in progress, NO otherwise.
 */
@property (assign, nonatomic, readonly) BOOL isLoading;

@end


#pragma mark - CBDataLoaderWithPagingAndRefreshDelegate

@protocol CBDataLoaderWithPagingAndRefreshDelegate <CBDataLoaderDelegate>

/**
 Called to inform the delegate that the data is loaded for the given range.
 @param range Range of the loaded data.
 */
- (void)dataLoadedForRange:(NSRange)range;

/**
 Called when the data loading is triggered by paging. 
 */
- (void)dataLoadingWithPaging;

/**
 Check if paging is enabled or not.
 */
- (BOOL)pagingEnabled;

@end

#pragma mark - CBFeedLogic

@interface CBFeedLogic : CBBaseLogic <CBDataLoaderWithPagingAndRefreshProtocol>

/**
 Category of the feed models.
 */
@property (strong, nonatomic) CBCategoryModel *category;

/**
 @description Data for FeedViewController instances.
 */
@property (strong, nonatomic) NSMutableArray *data;

/**
 Parallax ad model.
 */
@property (strong, nonatomic) CBParallaxAdModel *parallaxAd;

/**
 @description Search string for searching the feed content.
 */
@property (strong, nonatomic) NSString *searchString;

/**
 @description Delegate of CBFeedLogic.
 */
@property (weak, nonatomic) id<CBDataLoaderWithPagingAndRefreshDelegate> delegate;

/**
 @description Shows the current page index.
 */
@property (assign, nonatomic) NSInteger currentPage;

/**
 Select the feed at given index.
 */
- (CBBaseLogic *)getLogicWithIndex:(NSInteger)index;

/**
 Select the feed for given model.
 */
- (CBBaseLogic *)getLogicWithFeed:(CBFeedModel *)feed;

/**
 Subcategory array of the feed
 */
@property (nonatomic, strong) NSMutableArray *subCategoryArray;

/**
 Finds the feed index in the given feeds array.
 @return Index of the feed id. If not found, returns -1.
 */
- (NSInteger)findFeedId:(NSString *)feedId inFeedModels:(NSArray *)feeds;

/**
 In this method, network request is made. Can be modified in subclasses to make different network requests.
 @param page Page number to be fetched.
 @param successBlock To be executed after the request is successfull.
 @param failureBlock To be executed after the request is unsuccessfull.
 */
- (void)makeNetworkRequestWithPage:(NSInteger)page
                      successBlock:(void (^)(NSArray *))successBlock
                      failureBlock:(void (^)(NSError *))failureBlock;

/**
 Handles feed fetch request. If parallax ad is available, this method is called after fetching parallax ads and it includes the parallax in the feeds.
 @param CBFeedModel objects and the CBParallaxAd object if available.
 @param refresh If it is a result of refresh operation.
 */
- (void)handleNetworkSuccessWithFeeds:(NSArray *)feeds refresh:(BOOL)refresh;

/**
 Default error handler for feed request.
 */
- (void)handleNetworkError:(NSError *)error;

@end
