//
//  CBFeedCell.h
//  Carbon
//
//  Created by Necati Aydın on 31/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"
#import "CBFeedCellProtocol.h"

@interface CBFeedCell : CBBaseCollectionViewCell <CBFeedCellProtocol>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *spotLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *typeImageView;

/**
 Separator views around the cell.
 */
@property (strong, nonatomic) NSArray *separators;

/**
 @return Height / Width ratio of the cell.
 */
+ (CGFloat)cellRatio;


@end
