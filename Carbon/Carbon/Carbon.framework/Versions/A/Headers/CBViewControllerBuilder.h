//
//  CBViewControllerBuilder.h
//  Carbon
//
//  Created by Necati Aydın on 23/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu2ContentViewController.h"
#import "CBStartType.h"
#import "CBImageViewController.h"

@class CBDetailContainerLogic, CBNewsDetailLogic, CBCommentLogic, CBWriteCommentLogic, CBGalleryDetailLogic;

@interface CBViewControllerBuilder : NSObject

/**
 Singleton view controller builder instance.
 */
+ (instancetype)sharedInstance;

/**
 Root view controller to be given to the window.
 */
- (UIViewController *)rootViewController;

/**
 CBFeedViewController to display feeds.
 @param withNavigationController If set to yes, it includes the feed view controller in a navigation controller.
 */
- (UIViewController *)feedViewControllerWithDelegate:(id<CBMenuContentViewControllerDelegate>)delegate withNavigationController:(BOOL)withNavigationController;

/**
 CBLeftMenu1ViewController instance. (Left Menu Type 1).
 */
- (UIViewController *)leftMenu1ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate;


/**
 CBLeftMenu2ViewController instance. (Left Menu Type 2).
 */
- (UINavigationController *)leftMenu2ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel;

/**
 CBLeftMenu2ContentViewController instance.
 */
- (UIViewController *)leftMenu2ContentViewControllerWithDelegate:(id<CBLeftMenu2ContentViewControllerDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel;

/**
 CBLeftMenu3ViewController instance.
 */
- (UIViewController *)leftMenu3ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate;

/**
 CBLeftMenu4ViewController instance.
 */
- (UIViewController *)leftMenu4ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate;

/**
 CBGalleryDetailViewController instance to display a set of photos.
 @param logic CBGalleryLogic object which has feedId.
 @param feedTitle The title of the feed to be used for analytics purposes.
 @param backgroundImage The image to be used in the background while swiping to dismiss. (Take the previous controller's or navigation controller's snapshot and give it as a background). If set to nil, the background is black while dismissing.
 @return CBGalleryDetailViewController instance.
 */
- (UIViewController *)galleryDetailViewControllerWithLogic:(CBGalleryDetailLogic *)logic feedTitle:(NSString *)feedTitle backgroundImage:(UIImage *)backgroundImage;

/**
 CBGalleryDetailViewController instance to be used when a push notification sent.
 @param feed Feed model of the detail.
 @param backgroundImage The image to be used in the background while swiping to dismiss. (Take the previous controller's or navigation controller's snapshot and give it as a background). If set to nil, the background is black while dismissing.
 @return CBGalleryDetailViewController instance.
 */
- (UIViewController *)galleryDetailViewControllerWithFeed:(CBFeedModel *)feed startType:(StartType)startType backgroundImage:(UIImage *)backgroundImage;

/**
 CBGalleryDetailViewController instance to display a set of photos with initial page.
 @param logic CBGalleryLogic object which has feedId.
 @param feedTitle The title of the feed to be used for analytics purposes.
 @param backgroundImage The image to be used in the background while swiping to dismiss. (Take the previous controller's or navigation controller's snapshot and give it as a background). If set to nil, the background is black while dismissing.
 @param page The initial page number
 @return CBGalleryDetailViewController instance.
 */
- (UIViewController *)galleryDetailViewControllerWithLogic:(CBGalleryDetailLogic *)logic feedTitle:(NSString *)feedTitle backgroundImage:(UIImage *)backgroundImage withPage:(NSInteger)page;

/**
 CBDetailContainerViewController instance to contain detail view controllers.
 */
- (UIViewController *)detailContainerViewControllerWithLogic:(CBDetailContainerLogic *)logic
                                               startingIndex:(NSInteger)index
                                                   startType:(StartType)startType;

/**
 CBVideoDetailViewControler instance to be used after push notifications.
 */
- (UIViewController *)newsDetailViewControllerWithFeed:(CBFeedModel *)feed startType:(StartType)startType;

/**
 CBVideoDetailViewController instance.
 */
- (UIViewController *)newsDetailViewControllerWithLogic:(CBNewsDetailLogic *)logic startType:(StartType)startType;

/**
 CBVideoDetailViewController instance to be used after push notifications.
 */
- (UIViewController *)videoDetailViewControllerWithFeed:(CBFeedModel *)feed startType:(StartType)startType;

/**
 CBCommentViewController instance.
 */
- (UIViewController *)commentViewControllerWithLogic:(CBCommentLogic *)logic;

/**
 CBWriteCommentViewController instance
 */
- (UINavigationController *)writeCommentViewControllerWithFeed:(CBFeedModel *)feed;

/**
 CBSettingsViewController instance.
 */
- (UINavigationController *)settingsViewController;

/**
 CBInfoViewController instance.
 */
- (UINavigationController *)infoViewController;

/**
 CBWebViewController instance.
 @param request NSURLRequest to be loaded by the webview.
 @param title Title of the news to be used when sharing.
 */
- (UIViewController *)webViewWithUrlRequest:(NSURLRequest *)request title:(NSString *)title;

/**
 Web content view controller instance to be used with categories with web links.
 */
- (UIViewController *)webContentViewControllerWithDelegate:(id<CBMenuContentViewControllerDelegate>)delegate categoryModel:(CBCategoryModel *)categoryModel;

/**
 Web content view controller instance to be used with feeds with web links.
 */
- (UIViewController *)webContentViewControllerWithFeedModel:(CBFeedModel *)feedModel;


/**
 Creates and returns a CBFeedSearchViewController instance.
 @oaram searchString The results of the given search string is displayed in the view controller.
 */
- (UIViewController *)feedSearchViewControllerWithSearchString:(NSString *)searchString;

/**
 Creates and returns a CBGalleryAdContentViewController instance.
 @param entityName Entity string of the MRE to be displayed.
 @param delegate Image View Controller Delegate to handle tapping.
 */
- (UIViewController *)galleryAdContentViewControllerWithEntityName:(NSString *)entityName delegate:(id<CBImageViewControllerDelegate>)delegate;

@end

@interface CBViewControllerBuilder (Deprecated)

- (UIViewController *)leftMenu1ViewControllerWithDelegate:(id<CBLeftMenuContainedDelegate>)delegate rootCategory:(CBCategoryModel *)categoryModel __attribute((deprecated(("Use the method leftMenu1ViewControllerWithDelegate: instead."))));

@end
