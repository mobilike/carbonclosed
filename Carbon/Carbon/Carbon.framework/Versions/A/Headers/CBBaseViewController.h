//
//  CarbonViewController.h
//  Carbon
//
//  Created by Necati Aydın on 29/12/14.
//  Copyright (c) 2014 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBBaseLogic.h"
#import "UIViewController+Utility.h"

#define AdSizeMMA             CGSizeMake(320, 50)
#define AdSizeMRect           CGSizeMake(300, 250)
#define AdSizeLeaderboard     CGSizeMake(728, 90)

/**
 @description This a base class for UIViewControllers used in the framework.
 */
@interface CBBaseViewController : UIViewController

/**
 Base logic of the view controller.
 */
@property (nonatomic, strong) CBBaseLogic *logic;

/**
 Customizations for the navigation bar are applied in this method. Sublcasses can override this method. This method is called in viewDidLoad.
 */
- (void)customizeNavigationBar;

/**
 Setter method to assign class of subclass. Use this if your subclass name is other than without-prefix class name.
 @param classToUse class that will be set
 */
+ (void)setClassToUse:(Class)classToUse;

@end

