//
//  UITableView+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 26/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Additions)

/**
 Registers the cell nib in framework bundle with the default reuse identifier. Class name is used for default reuse identifier.
 */
- (void)registerClassInFrameworkBundleForDefaultReuseIdentifier:(Class)cellClass;


/**
 Registers the cell in main bundle nib with the default reuse identifier. Class name is used for default reuse identifier.
 */
- (void)registerClassInMainBundleForDefaultReuseIdentifier:(Class)cellClass;

@end
