//
//  CBFontPalette.h
//  Carbon
//
//  Created by Fırat Karataş on 04/09/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 @class CBFontPalette
 @superclass SuperClass: NSObject\n
 @discussion The singleton class that manages your app's main font schemes.
 */
@interface CBFontPalette : NSObject
/**
 @brief Static singleton access to the font palette
 @return CBFontPalette singleton object
 */
+ (instancetype)sharedPalette;

/**
 @brief Not used by Carbon framework directly, but still open to use for apps.
 */
@property (nonatomic, strong) UIFont *medium DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but still open to use for apps.
 */
@property (nonatomic, strong) UIFont *regular DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but still open to use for apps.
 */
@property (nonatomic, strong) UIFont *bold DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but still open to use for apps.
 */
@property (nonatomic, strong) UIFont *light DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but still open to use for apps.
 */
@property (nonatomic, strong) UIFont *oswaldRegular DEPRECATED_ATTRIBUTE;
/**
 @brief Not used by Carbon framework directly, but still open to use for apps.
 */
@property (nonatomic, strong) UIFont *oswaldLight DEPRECATED_ATTRIBUTE;

/**
 @discussion Loads and returns HelveticaNeue-Medium font for size
 @param size in CGFloat point
 @return UIFont instance with HelveticaNeue-Medium font for size
 */
- (UIFont *)mediumFontWithSize:(CGFloat)size;
/**
 @discussion Loads and returns HelveticaNeue font for size
 @param size in CGFloat point
 @return UIFont instance with HelveticaNeue font for size
 */
- (UIFont *)regularFontWithSize:(CGFloat)size;
/**
 @discussion Loads and returns HelveticaNeue-Bold font for size
 @param size in CGFloat point
 @return UIFont instance with HelveticaNeue-Bold font for size
 */
- (UIFont *)boldFontWithSize:(CGFloat)size;
/**
 @discussion Loads and returns HelveticaNeue-Light font for size
 @param size in CGFloat point
 @return UIFont instance with HelveticaNeue-Light font for size
 */
- (UIFont *)lightFontWithSize:(CGFloat)size;
/**
 @discussion Loads and returns Oswald-Regular font for size
 @param size in CGFloat point
 @return UIFont instance with Oswald-Regular font for size
 */
- (UIFont *)oswaldRegularFontWithSize:(CGFloat)size;
/**
 @discussion Loads and returns Oswald-Light font for size
 @param size in CGFloat point
 @return UIFont instance with Oswald-Light font for size
 */
- (UIFont *)oswaldLightFontWithSize:(CGFloat)size;

/**
 @discussion Loads and returns a custom font given its name and the extension of its file.
 @param name the name of the font
 @param type of the font file, e.g. ttf, otf etc.
 */
- (void)loadFontWithName:(NSString *)name type:(NSString *)type;

@end
