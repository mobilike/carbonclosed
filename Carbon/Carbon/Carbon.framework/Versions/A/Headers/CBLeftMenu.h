//
//  CBLeftMenu.h
//  Carbon
//
//  Created by Necati Aydın on 19/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CBCategoryModel;
@protocol CBLeftMenuViewControllerProtocol;

/**
 It handles the interaction from a left menu instance to the left menu container.
 */
@protocol CBLeftMenuContainedDelegate <NSObject>

/**
 If a category selected from a left menu, notifies the CBLeftMenuContainer instance.
 */
- (void)leftMenu:(UIViewController <CBLeftMenuViewControllerProtocol> *)leftMenuViewController didSelectCategory:(CBCategoryModel *)categoryModel;

/**
 If the settings is tapped in the view, CBLeftMenuContainer instance is notified via this method.
 */
- (void)settingsTappedFromViewController:(UIViewController<CBLeftMenuViewControllerProtocol> *)viewController;

/**
 If the info is tapped in the view, CBLeftMenuContainer instance is notified via this method.
 */
- (void)infoTappedFromViewController:(UIViewController<CBLeftMenuViewControllerProtocol> *)viewController;

/**
 If the view controller requests closing, CBLeftMenuContainer instance is snotified via this method.
 */
- (void)closeTappedFromViewController:(UIViewController <CBLeftMenuViewControllerProtocol> *)viewController;

@end


/**
 A left menu, which is going to be used for left menu container view controller, should conform to this protocol to include its methods and protocols.
 */
@protocol CBLeftMenuViewControllerProtocol <NSObject>

/**
 Root category for the left menu.
 */
@property (nonatomic, strong) CBCategoryModel *rootCategory;

/**
 Delegate for handling categories.
 */
@property (weak, nonatomic) id<CBLeftMenuContainedDelegate> leftMenuViewControllerDelegate;

@optional

/**
 Settings button tap action.
 */
- (IBAction)settingsTapped:(id)sender;

/**
 Info button tap action.
 */
- (IBAction)infoTapped:(id)sender;

@end


/**
 Should be used for the interaction from a contant view controller to the left menu container.
 */
@protocol CBMenuContentViewControllerDelegate <NSObject>

/**
 Menu button tap action.
 */
- (void)menuContentViewControllerTappedMenuButton:(UIViewController *)leftMenuContentViewController;

@end

