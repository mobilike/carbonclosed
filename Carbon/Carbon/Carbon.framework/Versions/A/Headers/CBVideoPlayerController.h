//
//  CBVideoPlayerController.h
//  Carbon
//
//  Created by Tolga Caner on 13/04/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBVideoPlayerDelegate.h"

@interface CBVideoPlayerController : UIToolbar {
    id <CBVideoPlayerDelegate> _delegate;
}

// UI Outlets
@property(nonatomic, weak) IBOutlet UIButton *playHeadButton;
@property(nonatomic, weak) IBOutlet UITextField *playHeadTimeText;
@property(nonatomic, weak) IBOutlet UITextField *durationTimeText;
@property(nonatomic, weak) IBOutlet UISlider *progressBar;
@property(nonatomic,weak) id delegate;

- (CMTime)getPlayerItemDuration:(AVPlayerItem *)item;
- (void)updatePlayHeadWithTime:(CMTime)time duration:(CMTime)duration;
- (void)updatePlayHeadState:(BOOL)isPlaying;
- (CMTime)getPlayerItemDuration:(AVPlayerItem *)item;
- (void)updatePlayHeadDurationWithTime:(CMTime)duration;
- (IBAction)onPlayPauseClicked:(id)sender;

@end
