//
//  CBGalleryAdContentViewController.h
//  Carbon
//
//  Created by Necati Aydın on 12/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryContentViewController.h"

@interface CBGalleryAdContentViewController : CBGalleryContentViewController

@property (nonatomic, copy) NSString *mreEntityName;

@end
