//
//  CBHomeViewController.h
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSeamlessViewController.h"
#import "CBFeedLogic.h"
#import "CBLeftMenu.h"
#import "CBFeedCellProtocol.h"
#import "CBDropdownMenuView.h"
#import "UIView+Loading.h"
#import "RFQuiltLayout.h"
#import "ADLivelyCollectionView.h"
#import "CBGroupNewsCell.h"
#import "CBAppConfig.h"

typedef NS_ENUM(NSUInteger, CustomLayoutCellPosition) {
    CustomLayoutCellPositionTopLeft,
    CustomLayoutCellPositionTopMiddle,
    CustomLayoutCellPositionTopRight,
    CustomLayoutCellPositionMiddleLeft,
    CustomLayoutCellPositionMiddleMiddle,
    CustomLayoutCellPositionMiddleRight,
    CustomLayoutCellPositionBottomLeft,
    CustomLayoutCellPositionBottomMiddle,
    CustomLayoutCellPositionBottomRight,
    CustomLayoutCellPositionUndefined
};

@class CBFeedViewController, CBLeftMenuContainerViewController, CBFeedModel, CBCollectionViewLoadingFooterView;

@interface CBFeedViewController : CBSeamlessViewController <CBDataLoaderWithPagingAndRefreshDelegate, CBDropdownMenuViewDelegate, UICollectionViewDataSource, RFQuiltLayoutDelegate, UICollectionViewDelegateFlowLayout, CBLoadingActionProtocol, UISearchBarDelegate, CBGroupNewsCellDataSource>

- (void)style;

/**
 Collection view.
 */
@property (weak, nonatomic) IBOutlet ADLivelyCollectionView *collectionView;

/**
 Logic of the view controller.
 */
@property (strong, nonatomic) CBFeedLogic *logic;

/**
 Search logic of the view controller.
 */
@property (strong, nonatomic) CBFeedLogic *searchLogic;

/**
 Delegate of the view controller.
 */
@property (weak, nonatomic) id<CBMenuContentViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomSpaceConstraint;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSDate *lastPull;
@property (strong, nonatomic) CBCollectionViewLoadingFooterView *footerView;
@property (nonatomic, assign) BOOL searchShouldBeDisplayed;
@property (nonatomic, strong) CBDropdownMenuView *dropdownMenuView;
@property (nonatomic, strong) NSLayoutConstraint *subcategoryViewHeightConstraint;
@property (nonatomic, strong) CBDropdownMenuNavigationBarView *dropdownMenuNavigationBarView;
@property (nonatomic, strong) NSMutableDictionary *groupCellsIndexes;

/**
 Starts interstitial and MMA ads.
 */
- (void)startAds;

- (Class<CBFeedCellProtocol>)cellClassForModel:(CBBaseModel *)model indexPath:(NSIndexPath *)indexPath;

/**
 Method for returning cell class of the specified feed model. Feed models can be mapped to default cell types. If not implemented in the subclass, feedStyleConfig is used to set default cell type.
 @param feedModel model object.
 @param indexPath Index path of the feed model in collection view.
 @return cellClass cell class of specified feedModel. This class needs to conform to the CBFeedCellProtocol.
 */
- (Class<CBFeedCellProtocol>)cellClassForFeedModel:(CBFeedModel *)feedModel indexPath:(NSIndexPath *)indexPath;

/**
 Method to do custom actions when selecting an item. Call super to do default action. This method is called in - (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath method.
 @param index path of selected item
 */
- (void)itemSelectedAtIndexPath:(NSIndexPath *)indexPath;

- (NSString *)entityNameWithoutType;

/**
 Method to do custom actions when an item is selected in the feed. Call super to do default action.
 @param feed Feed Model the of selected item
 @param logic Logic related with the selected item
 @param fromSlider returns YES if selected using a slider, no otherwise.
 */
- (void)itemSelectedWithFeedModel:(CBFeedModel *)feed logic:(CBBaseLogic *)logic fromSlider:(BOOL)fromslider;

/**
 Handle it by adding it to and removing from the collection view.
 */
- (void)handleRefreshControlVisibility;

- (BOOL)shouldShowPagingCellForIndexPath:(NSIndexPath *)indexPath;

/**
 Used for insets of collection view.
 */
- (CGFloat)collectionViewInnerSpace;

#pragma mark - Data Loading

/**
 FeedLogicDelegate method. Used to notify data is loading.
 */
- (void)dataLoading;

/**
 FeedLogicDelegate method. Used to notify data is loading with paging.
 */
- (void)dataLoadingWithPaging;

/**
 FeedLogicDelegate method. Used to notify data is loaded for range.
 @param range Range of the loaded data.
 */
- (void)dataLoadedForRange:(NSRange)range;

/**
 FeedLogicDelegate method. Used to notify data is loaded with error.
 @param errorMessage Error message.
 */
- (void)dataLoadedWithError:(NSString *)errorMessage;


#pragma mark - DropdownMenuView

//delegate & data source
- (void)dropdownMenuView:(CBDropdownMenuView *)dropdownMenuView didSelectAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)numberOfRowsAtDropdownMenuView:(CBDropdownMenuView *)dropdownMenuView;
- (NSString *)dropdownMenuView:(CBDropdownMenuView *)dropdownMenuView titleAtIndexPath:(NSIndexPath *)indexPath;

#pragma mark - Custom iPad CollectionViewLayout

/**
 Custom layout cell sizes are calculated using the same sized blocks. The return value here is used as a base block size for (1,1). Used for collectionView:(UICollectionView *) layout:(UICollectionViewLayout *) blockSizeForItemAtIndexPath:(NSIndexPath *) in RFQuiltLayoutDelegate. For example if a cell is going to be (200, 100) and you return (100, 100) from this method, then in the previously mentioned method in RFQuiltLayoutDelegate you need to return (2,1) for that cell.
 
 The default implementation is as follows.
 
 //size of the small cells. 3 because we want to fit 3 small cells in one row
 return CGSizeMake(CGRectGetWidth(viewSize) / 3, 150.f);
 @param viewSize Size of the view containing the collection view.
 */
- (CGSize)blockSizeForCustomLayoutWithViewSize:(CGSize)viewSize;

/**
 Use this method to give edge insets for your custom layout collection view and its cells instead of (UIEdgeInsets)collectionView:(UICollectionView *)
 layout:(UICollectionViewLayout *) insetsForItemAtIndexPath:(NSIndexPath *) (declared in RFQuiltLayoutDelegate) to make things easier. But you still can use the method in RFQuiltLayoutDelegate if you want.
 
 */
- (UIEdgeInsets)edgeInsetsForCustomLayoutCellPosition:(CustomLayoutCellPosition)customLayoutCellPosition;

/**
 Returns the cell position for a given index. Used in edgeInsetsForCustomLayoutCellPosition.
 */
- (CustomLayoutCellPosition)customLayoutCellPositionForIndexPath:(NSIndexPath *)indexPath;

- (void)setCollectionViewLayoutWithOrientation:(UIInterfaceOrientation)orientation
                            orientationChanged:(BOOL)orientationChanged
                                      viewSize:(CGSize)viewSize;

#pragma mark - Config

@property (nonatomic, assign) BOOL pullToRefreshEnabled;

@property (nonatomic, assign) BOOL pagingEnabled;

@property (assign, nonatomic) BOOL seamlessCollectionViewEnabled;

@property (assign, nonatomic) BOOL searchEnabled;

@property (nonatomic, assign) CBFeedCellType cellType;

@property (assign, nonatomic) BOOL scrollingUp;

@end
