//
//  CBCategoryAdditionsModel.h
//  Carbon
//
//  Created by Fırat Karataş on 24/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBBaseModel.h"

@interface CBCategoryAdditionsModel : CBBaseModel

/**
 Indicates that title of the category is shown in the feed.
 */
@property (nonatomic, assign) BOOL isTitleShown;

/**
 Indicates that sub categories of the category is shown in the feed of that category. Not in menu.
 */
@property (nonatomic, assign) BOOL isSubCategoriesShownInFeed;

/**
 Indicates that the category is shown in its sub category array too.
 */
@property (assign, nonatomic) BOOL shouldBeShownInSubCategories;

@end
