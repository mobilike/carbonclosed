//
//  CBFallbackInterstitialModel.h
//  Pods
//
//  Created by Tolga Caner on 22/04/16.
//
//

#import "CBBaseModel.h"

/**
 @brief It's customary for this class to have a single object which contains the attributes of the fallback interstitial ad.
 */
@interface CBFallbackInterstitialModel : CBBaseModel
/**
 Denotes the type of the fallback interstitial, by definition it should be INTERSTITIAL.
 */
@property (nonatomic,strong) NSString* adType;
/**
 Denotes the size of the fallback interstitial ad, width and height separated by an x (320x480 is the default).
 */
@property (nonatomic,strong) NSString* adSize;
/**
 The ad unit id for the fallback interstitial ad.
 */
@property (nonatomic,strong) NSString* adUnitId;

@end
