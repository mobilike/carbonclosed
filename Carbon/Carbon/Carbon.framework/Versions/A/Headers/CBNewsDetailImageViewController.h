//
//  CBNewsDetailImageViewController.h
//  Carbon
//
//  Created by Necati Aydın on 09/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBImageViewController.h"

@interface CBNewsDetailImageViewController : CBImageViewController

/**
 Creates and returns an instance with the given image and the initial image frame.
 @param image Image to be used for displaying.
 @param imageFrame Initial image frame to be displayed while starting to animate.
 @return Newly created instance.
 */
+ (instancetype)instanceWithInitialImage:(UIImage *)image imageFrame:(CGRect)imageFrame;

/**
 Fade animation can be disabled while dismissing. It is YES by default.
 */
@property (nonatomic, assign) BOOL fadeTransitionEneabledForDismiss;

@end
