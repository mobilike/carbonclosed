//
//  CBLeftMenu2ContentViewController.h
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBLeftMenu.h"
#import "CBBaseViewController.h"

@class CBLeftMenu2ContentViewController, CBLeftMenu2TableViewCell;


@protocol CBLeftMenu2ContentViewControllerDelegate <NSObject>

/**
 Called when a category is selected.
 */
- (void)leftMenuContent:(CBLeftMenu2ContentViewController *)leftMenuContentViewController didSelectCategory:(CBCategoryModel *)categoryModel;

/**
 Called when the back button is tapped.
 */
- (void)leftMenuContentTappedBackButton:(CBLeftMenu2ContentViewController *)leftMenuViewController;

@end


/**
 Displays the subcategories for the given root category.
 */
@interface CBLeftMenu2ContentViewController : CBBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

/**
 Root category for the content view. It should not be necesserily the same as the systems root category. Multiple instances of this class can be used by using recursion among categories.
 */
@property (strong, nonatomic) CBCategoryModel *rootCategory;

/**
 Delegate for the view controller.
 */
@property (weak, nonatomic) id<CBLeftMenu2ContentViewControllerDelegate> delegate;

/**
 Bottom constraint of table view
 */
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;

/**
 Back button to be used to go to the previous content view controller.
 */
@property (nonatomic, weak) IBOutlet UIButton *backButton;

/**
 Selects the first row in the table view. It only affects UI. It does not trigger selection related events.
 */
- (void)selectFirstRow;

/**
 Hides the top bar which includes the title.
 */
- (void)hideTopBar;

/**
 Class for the table view cell.
 @warning The cell class MUST be a CBLeftMenu2TableViewCell.
 */
- (Class)cellClass;

/**
 UITableView delegate method. Can be subclassed.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

- (void)style;
- (void)styleCell:(CBLeftMenu2TableViewCell *)cell;
- (void)styleSelectedCell:(CBLeftMenu2TableViewCell *)cell;

@end
