//
//  CBContentModel.h
//  Carbon
//
//  Created by Semih Cihan on 05/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"
#import "CBCategoryModel.h"

/**
 @typedef FeedType
 @description Used to indicate different feed types.
 */
typedef NS_ENUM(NSUInteger, FeedType) {
    FeedTypeArticle,
    FeedTypeGallery,
    FeedTypeVideo,
    FeedTypeAdvertorialArticle,
    FeedTypeAdvertorialVideo,
    FeedTypeWebLink,
    FeedTypeOther
};

/**
 @description This class is the model for feed data.
 */
@interface CBFeedModel : CBBaseModel

/**
 Type of the feed such as article, gallery or video.
 */
@property (assign, nonatomic) FeedType type;

/**
 Id of the feed.
 */
@property (strong, nonatomic) NSString *feedId;

/**
 Category in which this feed belongs.
 */
@property (strong, nonatomic) CBCategoryModel *category;

/**
 Date of the feed.
 */
@property (strong, nonatomic) NSString *date;

/**
 Title of the feed.
 */
@property (copy, nonatomic) NSString *title;

/**
 Spot of the feed.
 */
@property (copy, nonatomic) NSString *spot;

/**
 Url of the feed.
 */
@property (strong, nonatomic) NSURL *url;

/**
 Image url of the feed. Could be empty.
 */
@property (strong, nonatomic) NSURL *imageUrl;

/**
 Image width.
 */
@property (strong, nonatomic) NSNumber *imageWidth;

/**
 Image height.
 */
@property (strong, nonatomic) NSNumber *imageHeight;

/**
 Returns the feed type as a string.
 @param type Feed type as enum.
 @return Feed type as a string.
 */
+ (NSString *)detailPageStringOfFeedType:(FeedType)type;

/**
 Checks if an image url is included with the feed.
 */
- (BOOL)hasImage;

/**
 Cell type in feed.
 */
@property (assign, nonatomic) NSInteger cellType;

@end
