//
//  UIDevice+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

@interface UIDevice(Additions)

+ (BOOL)iOS7AndHigher;
+ (BOOL)lowerThaniOS7;
+ (BOOL)iOS8AndHigher;
+ (BOOL)lowerThaniOS8;
+ (BOOL)iOS9AndHigher;
+ (BOOL)lowerThaniOS9;
+ (BOOL)iOS10AndHigher;
+ (BOOL)lowerThaniOS10;

+ (BOOL)iPhone4OrLess;
+ (BOOL)iPhone5;
+ (BOOL)iPhone6;
+ (BOOL)iPhone6Plus;

+ (float)iPhone6_Width;
+ (float)iPhone6Plus_Width;

@end
