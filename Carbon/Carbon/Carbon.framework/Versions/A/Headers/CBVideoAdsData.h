//
//  CBVideoAdsData.h
//  Pods
//
//  Created by Tolga Caner on 05/05/16.
//
//

#import "CBVideoAds.h"

/**
 @class CBVideoAdsData
 @superclass CBBaseModel
 @description This class holds the "videos" part of the ad manifest, as a list
 */
@interface CBVideoAdsData : CBBaseModel

/**
 @brief A pseudo-NSArray containing the video ads in the manifest as CBVideoAds objects
 */
@property (nonatomic,strong) CBVideoAds* videoAds;
/**
 @description Determines whether the video player should change the web layers for convenience playing video ads. Setting it to YES causes no change in video ad displays.
 */
@property (nonatomic,assign) BOOL useDefaultImaPlayer;

@end
