//
//  CBWriteCommentViewController.h
//  Carbon
//
//  Created by Fırat Karataş on 03/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Carbon/Carbon.h>
#import "CBWriteCommentLogic.h"

@interface CBWriteCommentViewController : CBBaseViewController <CBCommentSenderDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, strong) CBWriteCommentLogic *logic;
@property (weak, nonatomic) IBOutlet UILabel *commentNoteLabel;
@property (weak, nonatomic) IBOutlet UITextField *nickNameTextField;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nickNameViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentTextViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentNoteLabelBottomConstraint;

@end
