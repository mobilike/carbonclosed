//
//  CBBaseTableViewCell.h
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kTableViewCellBorderLineWidth = 0.5f;

@interface CBBaseTableViewCell : UITableViewCell

+ (NSString *)reuseIdentifier;
+ (CGFloat)cellHeight;

@end
