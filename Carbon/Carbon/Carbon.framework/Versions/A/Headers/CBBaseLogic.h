//
//  CBBaseLogic.h
//  Carbon
//
//  Created by Necati Aydın on 05/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CBAppConfig.h"
@class CBFeedModel;
@class CBBaseModel;

@protocol CBDataLoaderProtocol <NSObject>

/**
 Call to initiate data loading.
 */
- (void)loadData;

@end


@protocol CBDataLoaderDelegate <NSObject>

/**
 Called to inform the delegate that the data loading is in progress.
 */
- (void)dataLoading;

/**
 Called to inform the delegate that the data is loaded.
 */
- (void)dataLoaded;

/**
 Called to inform the delegate that there is an error occured with loading the data.
 */
- (void)dataLoadedWithError:(NSString *)errorMessage;

@end



@interface CBBaseLogic : NSObject

/**
 Delegate of the logic.
 */
@property (nonatomic, weak) id delegate;

@end
