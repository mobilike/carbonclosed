//
//  CBGroupNewsFeedModel.h
//  Carbon
//
//  Created by Fırat Karataş on 20/10/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import <Carbon/Carbon.h>

@interface CBGroupNewsFeedModel : CBBaseModel

/**
 Array tht contains CBFeedModel objects.
 */
@property (nonatomic, strong) NSArray *groupNews;

/**
 Title of the group news.
 */
@property (nonatomic, strong) NSString *groupNewsTitle;

@end
