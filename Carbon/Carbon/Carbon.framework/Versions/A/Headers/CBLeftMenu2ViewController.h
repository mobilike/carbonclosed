//
//  CBLeftMenu2ViewController.h
//  Carbon
//
//  Created by Necati Aydın on 18/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBLeftMenu.h"

@class CBCategoryModel,CBLeftMenuLogic;

/**
 Type 2 left menu.
 */
@interface CBLeftMenu2ViewController : UIPageViewController <CBLeftMenuViewControllerProtocol>

/**
 Background image. Blurry image should be set here.
 */
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIView *backgroundView;

- (void)customizeNavigationBar;
- (void)style;

@end
