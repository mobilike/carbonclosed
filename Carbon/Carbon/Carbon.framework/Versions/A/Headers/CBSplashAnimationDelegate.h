//
//  CBSplashAnimationDelegate.h
//  Pods
//
//  Created by Tolga Caner on 06/09/16.
//
//

#import <Foundation/Foundation.h>
/**
 @protocol CBSplashAnimationDelegate
 @discussion A simple protocol that is used to inform its implementor when the app's splash animation is done.
 */
@protocol CBSplashAnimationDelegate <NSObject>
/**
 @brief called to inform when the splash animation is finished.
 */
- (void) splashFinished;

@end
