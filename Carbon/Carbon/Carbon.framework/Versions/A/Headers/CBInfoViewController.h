//
//  CBInfoViewController.h
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBTableViewController.h"
#import "CBInfoLogic.h"

@class CBInfoCell;

@interface CBInfoViewController : CBTableViewController

@property (strong, nonatomic) CBInfoLogic *logic;

#pragma mark - Style

- (void)style;
- (void)styleInfoCell:(CBInfoCell *)cell;

#pragma mark - Action
- (void)facebookButtonTapped;
- (void)twitterButtonTapped;
- (void)googlePlusButtonTapped;
- (void)youTubeButtonTapped;
- (void)closeButtonTapped:(id)sender;

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath;
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section;
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section;

#pragma mark - Web View

- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;

@end
