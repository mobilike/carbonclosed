//
//  CBLeftMenu2TableViewCell.h
//  Carbon
//
//  Created by Semih Cihan on 18/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseTableViewCell.h"

@interface CBLeftMenu2TableViewCell : CBBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) UIView *separator;
@property (nonatomic, weak) IBOutlet UIImageView *arrowImageView;

@end
