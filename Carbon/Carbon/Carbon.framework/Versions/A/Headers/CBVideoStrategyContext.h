//
//  CBVideoStrategyContext.h
//  Pods
//
//  Created by Tolga Caner on 10/05/16.
//
//

#import <Foundation/Foundation.h>
#import "CBVideoStrategy.h"


@interface CBVideoStrategyContext : NSObject

@property (strong,nonatomic) id<CBVideoStrategy> strategy;

- (void) configure;

@end
