//
//  UINavigationBar+TapRecognizing.h
//  Carbon
//
//  Created by Necati Aydın on 04/02/15.
//  Copyright (c) 2015 Mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView(TapRecognizing)

- (void)addTapGestureRecognizerByRemovingTheOldWithTarget:(id)target action:(SEL)action;

@end
