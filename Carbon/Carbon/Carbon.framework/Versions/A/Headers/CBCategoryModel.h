//
//  CBCategoryModel.h
//  Carbon
//
//  Created by Semih Cihan on 06/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"
#import "CBCategoryAdditionsModel.h"

typedef NS_ENUM(NSInteger, CBCategoryModelContentType)
{
    CBCategoryModelContentTypeFeed = 0,
    CBCategoryModelContentTypeWebContent = 1,
};

/**
 @description This class is the model for category data.
 */
@interface CBCategoryModel : CBBaseModel

@property (strong, nonatomic) NSString *categoryId;
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) BOOL hasFeed;
@property (strong, nonatomic) NSArray *subcategories;
@property (nonatomic, weak) CBCategoryModel *superCategory;
@property (strong, nonatomic) NSURL *webLink;
@property (strong, nonatomic) CBCategoryAdditionsModel *categoryAdditions;

/**
 Searchs the given cateogry and its subcategories recursively to find the category with the given category id.
 @param categoryId Id of the category to be found.
 @return Found category or nil if not found.
 */
- (CBCategoryModel *)searchCategoryForCategoryId:(NSString *)categoryId;

/**
 Searchs the given cateogry and its subcategories recursively to find the category with the given category name.
 @param categoryName Name of the category to be found.
 @return Found category or nil if not found.
 */
- (CBCategoryModel *)searchCategoryForCategoryName:(NSString *)categoryName;

- (CBCategoryModel *)searchCategoryForPropertyName:(NSString *)propertyName searchedPropertyValue:(NSString *)propertyValue ;

/**
 @return Returns a string of all the super categories' names of this category including itself. Say this category is named "A", and its super category is named "B", and "B" has a super category named "C". When getSuperCategoriesAsString is called on "A", this method returns "C, B, A".
 */
- (NSString *)getSuperCategoriesNames;

/**
 Returns all the subcategory ids recursively including the searched category id.
 @return Category Ids.
 */
- (NSArray *)findAllSubcategoryIds;

/**
 Returns true if the category has subcategories.
 @return BOOL value indicating that the category has subcategory.
 */
- (BOOL)hasSubcategory;

/**
 This returns the content type of the category using contentTypeNumber.
 @return Type of the category.
 */
- (CBCategoryModelContentType)contentType;

@end
