//
//  CBDataModel.h
//  Carbon
//
//  Created by Tolga Caner on 16/03/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "CBStandaloneModel.h"
#import "CBDisplayAds.h"
#import "CBVideoAdsData.h"

@interface CBDataModel : CBBaseModel

@property (nonatomic, strong) CBDisplayAds* displayAds;
@property (nonatomic,strong) CBVideoAdsData* videoAdsData;

@end
