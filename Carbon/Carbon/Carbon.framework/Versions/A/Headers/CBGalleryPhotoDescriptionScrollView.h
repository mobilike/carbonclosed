//
//  CBGalleryScrollView.h
//  Carbon
//
//  Created by Necati Aydın on 17/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBGalleryPhotoDescriptionScrollView : UIScrollView

@end
