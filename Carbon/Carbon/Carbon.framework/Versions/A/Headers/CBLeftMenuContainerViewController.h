//
//  CBLeftMenuContainerViewController.h
//  Carbon
//
//  Created by Necati Aydın on 17/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "RESideMenu.h"
#import "CBBaseLogic.h"
#import "CBLeftMenu.h"
#import "CBStartType.h"
#import <CoreLocation/CoreLocation.h>

@class CBLeftMenuContainerLogic;

/**
 It contains the left menu and feed. Should be used as root view controller.
 */
@interface CBLeftMenuContainerViewController : RESideMenu <RESideMenuDelegate, CBDataLoaderDelegate, CBMenuContentViewControllerDelegate>

/**
 Logic of the view controller.
 */
@property (nonatomic, strong) CBLeftMenuContainerLogic *logic;

@property (nonatomic, assign) StartType startType;


/**
 Returns the widht of the side menu's content width when the left menu is displayed.
 */
- (CGFloat)getCalculatedContentViewWidth;

/**
 *  Shows the feed by loading the given category.
 *
 *  @param categoryId Feed category to be loaded.
 */
- (void)showFeedFromPushWithCategoryId:(NSString *)categoryId;

/**
 *  Hides the left menu.
 */
- (void)hideMenu;

/**
 Called when a category is selected from a menu.
 */
- (void)leftMenu:(UIViewController <CBLeftMenuViewControllerProtocol> *)leftMenuViewController didSelectCategory:(CBCategoryModel *)categoryModel;

/**
 It is an empty method to be overriden. If the app started regularly it is executed in viewDidLoad.
 */
- (void)playSplashAnimation;

/**
Dummy
 */
+ (CLLocation*) currentLocation;

@end
