//
//  CBAdLoadingOperation.h
//  Pods
//
//  Created by Tolga Caner on 25/03/16.
//
//

#import <Foundation/Foundation.h>
@import GoogleMobileAds;
/**
 @class CBAdLoadingOperation
 @superclass SuperClass: NSOperation\n
 @description An NSOperation subclass that is specialized to use with feed ad loading, every ad loaded using CBCollectionViewAdManager is loaded through this object.
 */
@interface CBAdLoadingOperation : NSOperation

/**
 @brief The request that is the backone of the operation. The operation starts by making the request load itself, and ends when the request gets a response (success or fail).
 */
@property(nonatomic,strong) DFPRequest* request;

/**
 @brief The ad's view that the operation is working to fill.
 */
@property(nonatomic,strong) DFPBannerView* adView;

/**
 @description initialized the ad loading operation.
 @param request DFPRequest (or CBDFPRequest in most cases) to load.
 @return CBAdLoadingOperation an initialized, not-started loading operation
 */
- (id)initWithRequest:(DFPRequest*)request adView:(DFPBannerView*)adview;
/**
 @brief Called when the operation is complete.
 */
- (void) completeOperation;
@end
