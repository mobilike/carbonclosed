//
//  CBCommentCell.h
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"

@interface CBCommentCell : CBBaseCollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentLabelVerticalSpaceToContainerConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailLabelVerticalSpaceToCommentLabelConstraint;

@property (nonatomic, strong) NSArray *separators;


+ (CGSize)cellSizeForWidth:(CGFloat)width commentText:(NSString *)commentText commentFont:(UIFont *)commentFont detailText:(NSString *)detailText detailFont:(UIFont *)detailFont;

@end
