//
//  CBLeftMenu4Logic.h
//  Carbon
//
//  Created by Semih Cihan on 08/01/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"
#import "CBCategoryModel.h"

@class CBLeftMenu4Logic;

@protocol CBLeftMenu4LogicDelegate <NSObject>

- (void)leftMenu4logic:(CBLeftMenu4Logic *)logic didInsertRowsAtIndexes:(NSArray *)insertIndexes didDeleteRowsFromIndexes:(NSArray *)deleteIndexes;

@end

@interface CBLeftMenu4Logic : CBBaseLogic

@property (nonatomic, strong) CBCategoryModel *rootCategory;

@property (strong, nonatomic) NSMutableArray *shownCategories;

@property (nonatomic, weak) id<CBLeftMenu4LogicDelegate> delegate;

- (NSInteger)getCellLevelOfCategory:(CBCategoryModel *)category;
- (void)selectCategoryAtIndex:(NSInteger)index;

@end
