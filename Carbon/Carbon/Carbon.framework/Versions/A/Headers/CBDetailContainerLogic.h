//
//  CBDetailContainerLogic.h
//  Carbon
//
//  Created by Necati Aydın on 20/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNewsDetailLogic.h"

@interface CBDetailContainerLogic : CBBaseLogic

/**
 Initially given feed models;
 */
@property (nonatomic, copy, readonly) NSMutableArray *feedModels;

/**
 Creates and returns an instance with the given feed model ids.
 @param feedModels Array of CBFeedModel.
 @return CBDetailContaierLogic instance.
 */
- (instancetype)initWithFeedModels:(NSArray *)feedModels;


/**
 Returns the logic item with the given index.
 @param page Index of the selected page.
 @return Returns nil if the index out of bounds. If it is not out ouf bounds, it creates and returns the logic object. If it is already create, it just returns the previously created logic object.
 */
- (CBNewsDetailLogic *)logicWithIndex:(NSInteger)index;

/**
 Returns the logic item after the given logic item.
 @param logic Given logic item.
 @return Returns nil if the index out of bounds. If it is not out ouf bounds, it creates and returns the logic object after the given logic object. If it is already create, it just returns the previously created logic object.
 */
- (CBNewsDetailLogic *)logicAfterLogic:(CBNewsDetailLogic *)logic;

/**
 Returns the logic item before the given logic item.
 @param logic Given logic item.
 @return Returns nil if the index out of bounds. If it is not out ouf bounds, it creates and returns the logic object before the given logic object. If it is already create, it just returns the previously created logic object.
 */
- (CBNewsDetailLogic *)logicBeforeLogic:(CBNewsDetailLogic *)logic;

/**
 Returns the index of the logic item.
 @param logic Given logic item.
 @return Returns index of the logic item or -1 if the logic item does not exist.
 */
- (NSInteger)indexOfLogic:(CBNewsDetailLogic *)logic;


@end
