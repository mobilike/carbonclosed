//
//  CBNavigationBarStyler.h
//  Carbon
//
//  Created by Necati Aydın on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CBAppConfig.h"


@class CBDropdownMenuNavigationBarView;

/**
 @class CBNavigationBarStyler
 @superclass SuperClass: NSObject\n
 @description A manager to handle navigation bar style.
 */
@interface CBNavigationBarStyler : NSObject

/**
 @brief Static singleton access to the CBNavigationBarStyler
 @return CBNavigationBarStyler singleton object
 */
+ (instancetype)sharedInstance;

/**
 Styles navigation bar colors using CBNavigationBarConfig.
 */
- (void)styleNavigationBarColors;

/**
 Styles navigation bar title font and color using CBNavigationBarConfig.
 */
- (void)styleNavigationBarTitle;

/**
 Styles navigation bar title view with an UISearchBar instance.
 @param navigationItem Navigation item which has the title view.
 @param delegate UISearchBar delegate.
 */
- (UISearchBar *)styleNavigationItem:(UINavigationItem *)navigationItem
              titleViewWithSearchBar:(id<UISearchBarDelegate>)delegate;

/**
 Styles left navigation item so that it has a menu button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
- (void)styleLeftNavigationItemWithMenuIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles left navigation item so that it has a close button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
- (void)styleLeftNavigationItemWithCloseIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles right navigation item so that it has a search button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
- (void)styleRightNavigationItemWithSearchIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles right navigation item so that it has a close button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
- (void)styleRightNavigationItemWithCloseIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles right navigation item so that it has a share button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
- (void)styleRightNavigationItemWithShareIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles right navigation item so that it has a live stream button with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
- (void)styleRightNavigationItemWithStreamIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles navigation item so that it has a back button on the left with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
- (void)styleNavigationItemWithBackIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles navigation item so that it has a close button on the left with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param target Action target.
 @param action Action selector.
 */
- (void)styleNavigationItemWithCloseIcon:(UINavigationItem *)navigationItem target:(id)target action:(SEL)action;

/**
 Styles navigation item so that it has a settings button and info button on the right with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param settingsAction Settings button action selector.
 @param infoAction Info button action selector.
 @param target Action target.
 */
- (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
            settingsButtonAction:(SEL)settingsAction
                infoButtonAction:(SEL)infoAction
                          target:(id)target;

/**
 Styles navigation item so that it has a comment button and share button on the right with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param commentAction Comment button action selector.
 @param shareAction share button action selector.
 @param target Action target.
 */
- (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
             commentButtonAction:(SEL)commentAction
               shareButtonAction:(SEL)shareAction
                          target:(id)target;

/**
 Styles navigation item so that it has a font button and share button on the right with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param fontAction Font button action selector.
 @param shareAction share button action selector.
 @param target Action target.
 */
- (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
                fontButtonAction:(SEL)fontAction
               shareButtonAction:(SEL)shareAction
                          target:(id)target;

/**
 Styles navigation item so that it has a title on the right with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param action Button action selector.
 @param target Action target.
 */
- (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
                           title:(NSString *)title
                          action:(SEL)action
                          target:(id)target;
/**
 Styles navigation item so that it has a title on the left with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param action Button action selector.
 @param target Action target.
 */
- (void)styleLeftNavigationItem:(UINavigationItem *)navigationItem
                          title:(NSString *)title
                         action:(SEL)action
                         target:(id)target;

/**
 Styles navigation item so that it has a write comment button on the right with the given target and selector.
 @param navigationItem Navigation item to be styled.
 @param writeCommentAction Write comment button action selector.
 @param target Action target.
 */
- (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
        writeCommentButtonAction:(SEL)writeCommentAction
                          target:(id)target;

/**
 Styles navigation item so that it has a title logo.
 @param navigationItem Navigation item to be styled.
 */
- (void)styleWithTitleLogo:(UINavigationItem *)navigationItem;


/**
 Styles navigation item so that it has a title logo and a sub category title.
 @param navigationItem Navigation item to be styled.
 @param image Image to be shown at the top of the view. If it is null, default navigation bar title view image (title_bar_logo.png) is used.
 @param text Text to be shown at the bottom of the view.
 */
- (CBDropdownMenuNavigationBarView *)styleNavigationItem:(UINavigationItem *)navigationItem withDropDownTitleImage:(UIImage *)image titleText:(NSString *)text;

/**
 Styles navigation item so that it has a title logo and a sub category title.
 @param navigationItem Navigation item to be styled.
 @param text Text to be shown at the bottom of the view.
 @param mainTitleText Main title text to be shown at the top of the view.
 */
- (CBDropdownMenuNavigationBarView *)styleNavigationItem:(UINavigationItem *)navigationItem
                                               titleText:(NSString *)titleText
                                           mainTitleText:(NSString *)mainTitleText;

/**
 Set a text to the navigation item's title view.
 @param navigationItem Navigation item to be styled.
 @param title Navigation item title text.
 */
- (void)styleNavigationItem:(UINavigationItem *)navigationItem withTitle:(NSString *)title;

/**
 Set a text to the navigation item's title view.
 @param navigationItem Navigation item to be styled.
 @param title Navigation item title text.
 */
- (void)styleNavigationItem:(UINavigationItem *)navigationItem withCustomizableTitle:(NSString *)title;

/**
 Styles right Navigation Item
 @param navigationItem Navigation Item to be set
 @param settingsAction first button's action
 @param firstButtonImage image of the first button
 @param infoAction action of the second button
 @param secondButtonImage image of the second button
 @param target target
 */
- (void)styleRightNavigationItem:(UINavigationItem *)navigationItem
               firstButtonAction:(SEL)settingsAction
                firstButtonImage:(UIImage *)firstButtonImage
              secondButtonAction:(SEL)infoAction
               secondButtonImage:(UIImage *)secondButtonImage
                          target:(id)target;
/**
 Styles right Navigation Item
 @param navigationItem Navigation Item to be set
 @param image image of the nav item
 @param target target of the action
 @param action tap action
 */
- (void)styleRightNavigationItem:(UINavigationItem *)navigationItem image:(UIImage *)image target:(id)target action:(SEL)action;

/**
 Styles right Navigation Item
 @param navigationItem Navigation Item to be set
 @param image image of the nav item
 @param target target of the action
 @param action tap action
 */
- (void)styleLeftNavigationItem:(UINavigationItem *)navigationItem image:(UIImage *)image target:(id)target action:(SEL)action;

@end
