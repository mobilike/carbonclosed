//
//  CBStartType.h
//  Carbon
//
//  Created by Semih Cihan on 08/07/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#ifndef Carbon_CBStartType_h
#define Carbon_CBStartType_h

typedef NS_ENUM(NSUInteger, StartType) {
    StartTypeNormal,
    StartTypePush,
    StartTypeURL,
    StartTypeSuggested,
    StartTypeSlider
};

#endif
