//
//  CBManifestTestViewController.h
//  Pods
//
//  Created by Tolga Caner on 05/04/16.
//
//

#import <UIKit/UIKit.h>
#import "CBTestManifestDataModel.h"

/**
 @class CBManifestTestViewController
 @superclass SuperClass: UIViewController\n
 @discussion A simple UIViewController subclass used to add FLEX the functionality to change ad manifest on the fly. It lists the possible test manifests and handles selection events.
 */
@interface CBManifestTestViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
/**
 @brief Test data model.
 */
@property (nonatomic,strong) CBTestManifestDataModel* defaultDataModel;
/**
 @brief Data source for the tableview
 */
@property (nonatomic,strong) NSMutableArray* data;
/**
 @brief UITableView that shows the test manifests
 */
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;

@end
