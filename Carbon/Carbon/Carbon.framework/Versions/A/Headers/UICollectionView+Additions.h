//
//  UICollectionView+Additions.h
//  Carbon
//
//  Created by Necati Aydın on 26/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (Additions)

/**
 Registers the cell nib in framework bundle with the default reuse identifier. Class name is used for default reuse identifier.
 */
- (void)registerClassInFrameworkBundleForDefaultReuseIdentifier:(Class)cellClass;

/**
 Registers the cell in main bundle nib with the default reuse identifier. Class name is used for default reuse identifier.
 */
- (void)registerClassInMainBundleForDefaultReuseIdentifier:(Class)cellClass;

/**
 Calculates the content height of the collection view for the given delegate and data source.
 */
- (CGFloat)calculateCollectionViewHeightForDataSource:(id<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>)delegateDataSource;

/**
 It the given class responds to static reuseIdentifier: selector, it dequeues a cell by using that selector.
 */
- (UICollectionViewCell *)dequeueReusableCellWithCellClass:(Class)cellClass forIndexPath:(NSIndexPath *)indexPath;

@end
