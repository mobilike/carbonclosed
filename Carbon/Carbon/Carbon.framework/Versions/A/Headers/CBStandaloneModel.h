//
//  CBStandaloneModel.h
//  Carbon
//
//  Created by Tolga Caner on 16/03/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "CBBaseModel.h"

@interface CBStandaloneModel : CBBaseModel

@property (nonatomic,strong) NSArray* ads;
@property (nonatomic,strong) NSArray* adCategory;

@end
