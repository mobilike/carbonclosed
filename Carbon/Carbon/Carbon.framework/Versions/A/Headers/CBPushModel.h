//
//  CBPushModel.h
//  Carbon
//
//  Created by Necati Aydın on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBFeedModel.h"

/**
 @typedef PushType
 @description Used to indicate different push types.
 */
typedef NS_ENUM(NSUInteger, PushType) {
    PushTypeDetailArticle,
    PushTypeDetailGallery,
    PushTypeDetailVideo,
    PushTypeCategory
};

@interface CBPushModel : CBBaseModel <MTLJSONSerializing>

/**
 Alert text for the push notification.
 */
@property (nonatomic, strong) NSString *alert;

/**
 feedId Id for the detail to be opened. It can be nil.
 */
@property (nonatomic, strong) NSString *feedId;

/**
 categoryId Id for the feed to be opened. It can be nil.
 */
@property (nonatomic, strong) NSString *categoryId;

/**
 pushType for the feed to be opened.
 */
@property (nonatomic, assign) PushType pushType;

@end
