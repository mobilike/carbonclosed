//
//  CBRotationlessNavigationController.h
//  Carbon
//
//  Created by Necati Aydın on 13/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 When the root view controller is navigation controller, rotation behaviour cannot be overriden. By using this class as the parent for the root navigation controller, rotation can be limitedd to portrait only.
 */
@interface CBRotationlessNavigationController : UINavigationController

@end
