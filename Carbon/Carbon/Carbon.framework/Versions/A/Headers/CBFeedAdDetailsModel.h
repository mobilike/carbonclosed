//
//  CBFeedAdDetailsModel.h
//  Carbon
//
//  Created by Tolga Caner on 22/03/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "CBAdDetailsModel.h"

@interface CBFeedAdDetailsModel : CBAdDetailsModel

@property (nonatomic,strong) NSString* adFrequency;
@property (nonatomic,strong) NSString* adStartIndex;



@end
