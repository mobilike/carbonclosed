//
//  CBLeftMenuContainerLogic.h
//  Carbon
//
//  Created by Necati Aydın on 19/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@class CBCategoryModel;

@interface CBLeftMenuContainerLogic : CBBaseLogic <CBDataLoaderProtocol>

/**
 Current root category for menus.
 */
@property (nonatomic, strong) CBCategoryModel *rootCategory;

/**
 @description Delegate of the logic.
 */
@property (weak, nonatomic) id<CBDataLoaderDelegate> delegate;

/**
 Returns root categories first subcategory.
 */
- (CBCategoryModel *)mainPageCategory;

@end
