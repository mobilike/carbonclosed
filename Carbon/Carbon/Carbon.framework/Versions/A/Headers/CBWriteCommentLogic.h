//
//  CBWriteCommentLogic.h
//  Carbon
//
//  Created by Fırat Karataş on 03/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Carbon/Carbon.h>


@protocol CBCommentSenderDelegate <NSObject>

/**
 Called to inform the delegate that the comment sending is in progress.
 */
- (void)commentSending;

/**
 Called to inform the delegate that the comment is sent.
 */
- (void)commentSentWithSuccesMessage:(NSString *)message title:(NSString *)title;

/**
 Called to inform the delegate that there is an error occured with sending the comment.
 */
- (void)commentSentWithError:(NSString *)errorMessage;

@end

@interface CBWriteCommentLogic : CBBaseLogic

@property (nonatomic, strong) CBCommentModel *comment;

@property (nonatomic, weak) id<CBCommentSenderDelegate> delegate;

@property (strong, nonatomic) CBFeedModel *feed;

/**
 Makes service call for writing comment
 */
- (void)sendComment;

/**
 Custom init for specific detail id
 @param detailId detailId of the news.
 */
- (instancetype)initWithFeed:(CBFeedModel *)feed;

@end
