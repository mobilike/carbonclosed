//
//  UIViewController+Share.h
//  Carbon
//
//  Created by Necati Aydın on 08/09/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Share)

/**
 If it is an iPhone device, it presents an activity view controller, if not it presents a popover. It does not work with iPads with an iOS less than iOS8.
 @param title Title of the shared item. Can be nil.
 @param url Url of the shared item. Can be nil.
 @param image Image to share. Can be nil.
 @param sourceView It is used for iPads. The view that popover is presented from. Can be nil.
 @param sourceRect It is used for iPads. The rect in which popover is presented from. Can be set to CGRectZero to avoid using.
 @param popoverArrowDirection The arrow direction of the poopover. Can be set to UIPopoverArrowDirectionUnknown to avoid using.
 */
- (void)openShareViewWithTitle:(NSString *)title
                           url:(NSURL *)url
                         image:(UIImage *)image
                    sourceView:(UIView *)sourceView
                    sourceRect:(CGRect)sourceRect
         popoverArrowDirection:(UIPopoverArrowDirection)popoverArrowDirection;

@end
