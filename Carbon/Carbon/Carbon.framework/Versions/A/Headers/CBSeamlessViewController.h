//
//  CBSeamlessViewController.h
//  Carbon
//
//  Created by Semih Cihan on 17/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import "CBCollectionViewAdManager.h"
#import "CBVideoPlayer.h"

@import GoogleMobileAds;

/**
 This class is a base class for view controllers that show ads.
 */
@interface CBSeamlessViewController : CBBaseViewController <GADBannerViewDelegate, GADInterstitialDelegate>

@property (strong, nonatomic) CBCollectionViewAdManager *collectionViewAdManager;

/**
 Interstitial entity.
 */
@property(nonatomic, strong) DFPInterstitial *interstitial;
/**
 MMA ad view.
 */
@property (nonatomic, strong) DFPBannerView *mmaView;

/**
 MRE ad view.
 */
@property (nonatomic, strong) DFPBannerView *mreView;

/**
 Creates and starts the interstitial ad manager.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 */
- (void)startInterstitialAdWithEntityName:(NSString *)entityName subCategory:(NSString*)subCategory contentId:(NSString*)contentId;
/**
 Creates and starts the MMA ad view.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 @param url share url of news detail mma. If mma is shown in the feed, url string is sent empty string.
 */
- (void)startMMAAdWithEntityName:(NSString *)entityName shareUrl:(NSString *)url subCategory:(NSString*)subCategory contentId:(NSString*)contentId;

/**
 Creates and starts the MRE ad view.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 @param url share url of news detail mma. If mma is shown in the feed, url string is sent empty string.
 */
- (void)startMREAdWithEntityName:(NSString *)entityName shareUrl:(NSString *)url subCategory:(NSString*)subCategory contentId:(NSString*)contentId;

/**
 Creates and starts the collection view ad manager.
 @param entityName Entity name to be concatenated at the end of the created entity name within the method.
 @param collectionView Collection view to be used by the manager.
 @param dataSource Data source to be used by the manager.
 */
- (void)startCollectionViewAdWithEntityName:(NSString *)entityName
                             collectionView:(UICollectionView *)collectionView
                                 dataSource:(NSMutableArray *)dataSource;

/**
 Presents the video player modally.
 @param url Video url to be played.
 @param categoryName Category name corresponding to the item which will be played. Can be nil.
 @param contentId Id of the content that is linked to the video. Can be nil.
 @param shareUrl shareUrl of the video. Can be nil.
 */
- (void)presentVideoPlayerWithUrl:(NSURL *)url categoryName:(nullable NSString *)entityName contentId:(nullable NSString*)contentId shareUrl:(nullable NSString*)shareUrl;

- (void)presentVideoPlayerWithUrl:(NSURL *)url categoryName:(NSString *)categoryName contentId:(NSString*)contentId shareUrl:(NSString*)shareUrl presentationCompletionBlock:(void (^ __nullable)(CBVideoPlayer*))prCompletion playbackCompletionBlock:(void (^ __nullable)(void)) plCompletion;

- (void)presentVideoPlayerWithUrl:(NSURL *)url categoryName:(NSString *)categoryName contentId:(NSString*)contentId shareUrl:(NSString*)shareUrl presentationCompletionBlock:(void (^ __nullable)(CBVideoPlayer*))prCompletion playbackCompletionBlock:(void (^ __nullable)(void)) plCompletion playbackResumedBlock:(void (^ __nullable)(void)) plResumed playbackPausedBlock:(void (^ __nullable)(void)) plPaused;

+ (NSString *)interstitialEntityNameWithEntityName:(NSString *)entityName;
+ (NSString *)mmaEntityNameWithEntityName:(NSString *)entityName;
+ (NSString *)collectionViewEntityNameWithEntityName:(NSString *)entityName;
+ (NSString *)mreEntityNameWithEntityName:(NSString *)entityName;
+ (NSString *)videoPlayerEntityNameWithEntityName:(NSString *)entityName;

- (void)getCollectionViewAdWithEntityName:(NSString *)entityName;

- (NSDictionary*)customKeywords;

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;


@property (nonatomic,assign) BOOL interstitialAdRequestedForInstance;
@property (nonatomic,assign) BOOL mmaAdRequestedForInstance;

@end
