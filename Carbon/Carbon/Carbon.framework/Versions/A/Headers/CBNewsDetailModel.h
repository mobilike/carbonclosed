//
//  CBVideoDetailModel.h
//  Carbon
//
//  Created by Semih Cihan on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"
#import "CBCategoryModel.h"

@interface CBNewsDetailModel : CBBaseModel

@property (strong, nonatomic) NSString *detailId;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) CBCategoryModel *category;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *spot;
@property (strong, nonatomic) NSURL *imageUrl;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *galleryId;
@property (strong, nonatomic) NSURL *videoUrl;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSNumber *viewCount;
@property (strong, nonatomic) NSMutableArray *suggestedVideos;
@property (nonatomic, strong) NSMutableArray *comments;

@end
