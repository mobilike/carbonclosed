//
//  CBVideoRollAd.h
//  Pods
//
//  Created by Tolga Caner on 18/04/16.
//
//

#import "CBBaseModel.h"

/**
 @class CBVideoRollAd
 @superclass CBBaseModel
 @description Model that contains information on a video ad roll (pre,mid,post)
 */
@interface CBVideoRollAd : CBBaseModel

/**
 @brief The publisher's video tag that should be requested.
 */
@property (nonatomic, strong) NSString* publisherTag;
/**
 @brief The type as NSString, exclusively vast.
 */
@property (nonatomic, strong) NSString* type;
/**
 @brief For midrol ads, the number of minutes after a new midroll request will be made. Should be a whole number, at least 1.
 */
@property (nonatomic, strong) NSNumber* period;

@end
