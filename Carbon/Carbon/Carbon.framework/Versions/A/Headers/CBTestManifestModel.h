//
//  CBTestManifestModel.h
//  Pods
//
//  Created by Tolga Caner on 05/04/16.
//
//

#import "CBManifestModel.h"

/**
 @class CBTestManifestModel
 @superclass SuperClass: CBManifestModel\n
 @brief Manifest Model for a test manifest.
 */
@interface CBTestManifestModel : CBManifestModel
/**
 @brief the test manifest models, as arrays.
 */
@property (nonatomic,strong) NSArray* testData;

@end
