//
//  CBForceUpdateController.h
//  Pods
//
//  Created by Tolga Caner on 30/03/16.
//
//

#import <Foundation/Foundation.h>

@interface CBForceUpdateController : NSObject <UIAlertViewDelegate>

- (void) showAlert;

@property (assign) BOOL force;
@property (nonatomic,strong) NSString* titleText;
@property (nonatomic,strong) NSString* bodyText;

@end
