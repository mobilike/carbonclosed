//
//  CBVideoPlayer.h
//  Pods
//
//  Created by Tolga Caner on 12/04/16.
//
//
#import <UIKit/UIKit.h>
#import <GoogleInteractiveMediaAds/GoogleInteractiveMediaAds.h>
#import "CBVideoPlayerViewController.h"

@import GoogleMobileAds;

@interface CBVideoPlayer : UIViewController <GADInterstitialDelegate>

@property (nonatomic,strong) CBVideoPlayerViewController* videoPlayerViewController;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *loadingView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *invisibleOverlayView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *invisibleOverlayCenter;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *companionTopLeft;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *companionTopRight;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *companionBottomLeft;


- (void)adServiceDidReceiveAdEventWith:(IMAAdEvent*)event;

+ (instancetype)videoPlayerWithURL:(NSString*)url withCategoryName:(NSString*)name contentId:(NSString*)contentId shareUrl:(NSString*)shareUrl ;
+ (instancetype)videoPlayerWithURL:(NSString*)url withCategoryName:(NSString*)name contentId:(NSString*)contentId shareUrl:(NSString*)shareUrl completion: (void (^ __nullable)(void))completion;
+ (instancetype)videoPlayerWithURL:(NSString*)url withCategoryName:(NSString*)name contentId:(NSString*)contentId shareUrl:(NSString*)shareUrl completion: (void (^ __nullable)(void))completion paused: (void (^ __nullable)(void))paused resumed: (void (^ __nullable)(void))resumed;
- (void) adClicked;
- (void) setLoadingHidden:(BOOL)value;

-(NSMutableArray*) companionSlots;

@end
