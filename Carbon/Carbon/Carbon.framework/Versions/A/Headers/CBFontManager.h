//
//  FontManager.h
//  Carbon
//
//  Created by Necati Aydın on 07/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 A class for dynamically loading the custom fonts.
 */
@interface CBFontManager : NSObject

/**
 Loads the specified font.
 */
+ (void)loadFontWithName:(NSString *)fontName ofType:(NSString *)type;

@end
