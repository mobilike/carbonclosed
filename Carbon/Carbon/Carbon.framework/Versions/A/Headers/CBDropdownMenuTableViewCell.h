//
//  CBSubcategoryTableViewCell.h
//  Carbon
//
//  Created by Fırat Karataş on 26/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseTableViewCell.h"

@interface CBDropdownMenuTableViewCell : CBBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (strong, nonatomic) UIColor *selectionColor;

@end
