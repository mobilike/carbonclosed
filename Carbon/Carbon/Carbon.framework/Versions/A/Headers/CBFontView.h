//
//  CBFontView.h
//  Carbon
//
//  Created by Necati Aydın on 24/11/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CBFontView;

@protocol CBFontViewDelegate <NSObject>

@optional
- (void)fontView:(CBFontView *)fontView didChangeValue:(NSInteger)value;
- (void)animateWhileFontViewPresentedAnimated:(CBFontView *)fontView;
- (void)animateWhileFontViewDissmissedAnimated:(CBFontView *)fontView;
- (void)fontViewWillDismiss:(CBFontView *)fontView;
- (void)fontViewDidDismiss:(CBFontView *)fontView;
- (void)fontViewWillPresent:(CBFontView *)fontView;
- (void)fontViewDidPresent:(CBFontView *)fontView;

@end

@interface CBFontView : UIView

- (id)initWithSliderRange:(CGFloat)range;

@property (nonatomic, weak) id<CBFontViewDelegate> delegate;

@property (nonatomic, assign) BOOL isPresented;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UIButton *backgroundCloseButton;

@property (nonatomic, strong) NSLayoutConstraint *containerViewTopSpace;
@property (nonatomic, strong) NSLayoutConstraint *containerViewHeight;

@property (nonatomic, assign) CGFloat alphaWhenPresented;

- (void)presentAnimated:(BOOL)animated;
- (void)dismissAnimated:(BOOL)animated;

@end
