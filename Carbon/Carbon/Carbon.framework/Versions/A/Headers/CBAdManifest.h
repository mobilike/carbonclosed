//
//  CBAdManifest.h
//  Carbon
//
//  Created by Tolga Caner on 17/03/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "CBManifestModel.h"
#import "CBFirstInterstitialDelegate.h"
#import "CBSplashAnimationDelegate.h"
@import GoogleMobileAds;

#define kAdCategoryDefault                   @"default"
/**
 @class CBAdManifest
 @superclass SuperClass: NSObject\n
 @discussion The singleton class that manages and serves the ad manifest json.
 */
@interface CBAdManifest : NSObject
/**
 @brief Category name of the homepage. (e.g. "Anasayfa")
 */
@property (nullable,nonatomic, strong) NSString* homePageCategoryName;
/**
 @description The unique advertising identifier of the device that the app is running on. It's meant to be generated once and passed on to the next sessions. If at first there's a lack of this id, it's generated and saved.
 */
@property (nonatomic, strong, nullable) NSString* advertisingIdentifierString;
/**
 @brief The object that contains all the content of the manifest json.
 */
@property (nonatomic, strong,nullable) CBManifestModel* manifestModel;
/**
 @brief The object that contains all the content of the test manifest (if there's one)
 */
@property (nonatomic, strong,nullable) CBManifestModel* testManifestModel;
/**
 @brief The delegate that controls the first interstitial ad of the app to be shown (which is a special case by definition, because of DFP regulations)
 */
@property (nonatomic, weak, nullable) id <CBFirstInterstitialDelegate> firstInterstitialDelegate;
/**
 @brief Denotes whether the splash animation has finished.
 */
@property (atomic, assign) BOOL splashAnimationFinished;
/**
 @brief The delegate that is used to notify when the splash animation is finished
 */
@property (nonatomic, weak, nullable) id <CBSplashAnimationDelegate> splashAnimationDelegate;
/**
 @brief The delegate that is used to notify when the splash animation is finished
 */
@property(nonatomic, strong,nullable) DFPInterstitial *interstitial;
/**
 @brief Static singleton access to the adManifest
 @return CBAdManifest singleton object
 */
+(nonnull id)sharedAdManifest;
/**
 @brief Method to call if you want to add certain DFP keywords to all the requests you make.
 @param keywords the keyword key-value pairs you want to add.
 */
- (void) addKeywords:(nullable NSDictionary*)keywords;
/**
 @brief Gets the custom keywords
 @return NSDictionary of the keywords
 */
- (nullable NSDictionary*)customKeywords;
#pragma mark Data Accessors
/**
 @brief Returns the fallback interstitial ad unit id for video ads in case of failure 
 @return NSString video fallback interstitial ad unit id.
 */
-(nullable NSString*)videoFallbackInterstitialAdUnitId;
/**
 @brief Method to get a display ad unit id for given parameters
 @param adType The ad type as string, "MMA","MRE","CUSTOM" etc.
 @param category The name of the category in which the ad will be shown (e.g. News, Sports ) as denoted in the ad manifest.
 @param subCategory A single category name that is added to the custom targeting parameters dictionary. (currently unused)
 @return NSString An ad unit id fitting the parameters.
 */
-(nullable NSString*)standaloneAdUnitIdWithType:(nullable NSString*)adType adCategory:(nullable NSString*)category adSubCategory:(nullable NSString*)subCategory;
/**
 @brief Method to get a list of CBFeedAd objects for given parameters
 @param adType The ad type as string, "MMA","MRE","CUSTOM" etc.
 @param category The name of the category in which the ad will be shown (e.g. News, Sports ) as denoted in the ad manifest.
 @param subCategory A single category name that is added to the custom targeting parameters dictionary. (currently unused)
 @return NSArray List of feed ads fitting the requirements given in the parameters.
 */
-(nullable NSArray*)feedAdsWithType:(nullable NSString*)adType adCategory:(nullable NSString*)category adSubCategory:(nullable NSString*)subCategory;
/**
 @brief Method to get a single CBVideoRollAd object for given parameters
 @param category The name of the category in which the ad will be shown (e.g. News, Sports ) as denoted in the ad manifest.
 @param contentId The id of the content which is being played.
 @param shareUrl The share URL of the content being played.
 @return CBVideoRollAd object containing information about the video ad.
 */
-(nullable CBVideoRollAd*)videoAdUnitIdWithCategory:(nullable NSString*)category contentId:(nullable NSString*)contentId shareUrl:(nullable NSString*)shareUrl rollType:(nullable NSString*)rollType;

@end
