//
//  CBDeviceType.h
//  Carbon
//
//  Created by Semih Cihan on 08/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ScreenSizeType) {
    ScreenSizeTypeIphonePrior5,
    ScreenSizeTypeIphone5,
    ScreenSizeTypeIphone6,
    ScreenSizeTypeIphone6Plus,
    ScreenSizeTypeOther
};

@interface CBScreenSizeType : NSObject

/**
 Returns screen size based on the screen dimensions of the device.
 @return ScreenSizeType
 */
+ (ScreenSizeType)screenSizeType;

@end
