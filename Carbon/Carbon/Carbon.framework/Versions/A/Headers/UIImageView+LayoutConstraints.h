//
//  UIImageView+LayoutConstraints.h
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImageView (LayoutConstraints)

- (NSLayoutConstraint *)setAspectFitLayoutConstraint;

@end
