//
//  CBWebViewController.h
//  Carbon
//
//  Created by Necati Aydın on 22/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"

@interface CBWebViewController : CBBaseViewController

/**
 Request to be loaded by the webview.
 */
@property (nonatomic, strong) NSURLRequest *request;

/**
 Title of the news to be used when sharing.
 */
@property (nonatomic, strong) NSString *shareTitle;

@end
