//
//  CBAppConfig.h
//  Carbon
//
//  Created by Necati Aydın on 23/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/*!
 @typedef AdType
 
 @brief  An enum containing possible left menu types.
 
 @field The menu option which is shown from left and scales down the content view.
 @field The menu option which is presented in full screen.
 @field The menu option which is shown from left without scaling down the content view. It only supports category trees with a height of 2.
 @field Accordion menu option. There is no restriction on the height of the category tree to be used.
 */
typedef NS_ENUM(NSInteger, CBLeftMenuType)
{
    /**
     The menu option which is shown from left and scales down the content view.
     */
    CBLeftMenuType1=1,
    
    /**
     The menu option which is presented in full screen.
     */
    CBLeftMenuType2=2,
    
    /**
     The menu option which is shown from left without scaling down the content view. It only supports category trees with a height of 2.
     */
    CBLeftMenuType3=3,
    
    /**
     Accordion menu option. There is no restriction on the height of the category tree to be used.
     */
    CBLeftMenuType4=4,
};

/*!
 @typedef CBFeedCellType
 
 @brief  An enum containing possible feed cell types. CBFeedModel uses it to describe its own cell type.
 
 @field Used to depict that no cell type has been selected for the app.
 @field A cell that has its title and description labels at the top and its main image at the bottom.
 @field A cell that has its title label at the top and a date label at the bottom edge, with a main image covering the whole cell, with a top-down black gradient.
 @field A cell that has its title label at the top, with a main image covering the whole cell, a top-down black gradient and an image view at the bottom-right position intended to proclaim its category.
 @field Main image to the left, title, category and spot labels on the right listed vertically.
 @field A cell with a main image covering the top 70% of the cell, with its bottom 30% dedicated to its text labels and an image view at the bottom-right position intended to proclaim its category.
 @field Basically FeedCellType4, with its image changed to have margins to the top,bottom and left edges of the cell.
 */
typedef NS_ENUM(NSInteger, CBFeedCellType)
{
    /**
     Used to depict that no cell type has been selected for the app.
     */
    CBFeedCellTypeNotSpecified = 0,
    /**
     A cell that has its title and description labels at the top and its main image at the bottom.
     */
    CBFeedCellType1 = 1,
    /**
     A cell that has its title label at the top and a date label at the bottom edge, with a main image covering the whole cell, with a top-down black gradient.
     */
    CBFeedCellType2 = 2,
    /**
     A cell that has its title label at the top, with a main image covering the whole cell, a top-down black gradient and an image view at the bottom-right position intended to proclaim its category.
     */
    CBFeedCellType3 = 3,
    /**
     Main image to the left, title, category and spot labels on the right listed vertically.
     */
    CBFeedCellType4 = 4,
    /**
     A cell with a main image covering the top 70% of the cell, with its bottom 30% dedicated to its text labels and an image view at the bottom-right position intended to proclaim its category.
     */
    CBFeedCellType5 = 5,
    /**
     Basically FeedCellType4, with its image changed to have margins to the top,bottom and left edges of the cell.
     */
    CBFeedCellType7 = 7,
};
/**
 @class CBAppConfig
 @superclass SuperClass: NSObject\n
 @discussion The singleton class that manages your app's configurations. You can make a subclass of it with the name "AppConfig". Many keys belonging to the 3rd party SDKs used in the framework are defined here. All the customizable colors, images, logos are available in this class.
 */
@interface CBAppConfig : NSObject
/**
 @brief Static singleton access to the app configurations
 @return CBAppConfig singleton object
 */
+ (instancetype)sharedConfig;

#pragma mark - App Configs
/**
 Type for the left menu which shows categories. It can be 1 or 2.
 */
@property (nonatomic, assign) CBLeftMenuType appLeftMenuType;

/**
 UI type for the news detail.
 */
@property (nonatomic, assign) NSInteger appNewsDetailType;

/**
 Application id, given by Parse for push notification purposes.
 */
@property (nonatomic, strong) NSString *appParseApplicationId;

/**
 Client key, given by Parse for push notification purposes.
 */
@property (nonatomic, strong) NSString *appParseClientKey;

/**
 Seamless app token for iPad.
 */
@property (nonatomic, strong) NSString *appSeamlessAppTokenIPad;

/**
 Seamless app token for iPhone.
 */
@property (strong, nonatomic) NSString *appSeamlessAppToken;

/**
 If YES, enables seamless location based targeting.
 @warning If set to YES, you should add "NSLocationWhenInUseUsageDescription" key into your project's Info.plist file.
 */
@property (assign, nonatomic) BOOL appSeamlessLocationEnabled;

/**
 Google analytics tracking id.
 */
@property (strong, nonatomic) NSArray *appGoogleAnalyticsTrackingIds;

/**
 Gemius prefix.
 */
@property (strong, nonatomic) NSString *appGemiusPrefix;

/**
 Gemius default tracking id.
 */
@property (strong, nonatomic) NSString *appGemiusDefaultTrackingId;

/**
 Gemius main page tracking id.
 */
@property (strong, nonatomic) NSString *appGemiusMainPageTrackingId;

/**
 Gemius main category name.
 */
@property (strong, nonatomic) NSString *appGemiusMainPageCategoryName;

/**
 Flurry analytics tracking id.
 */
@property (strong, nonatomic) NSString *appFlurryAnalyticsTrackingId;

/**
 Indicates if the app has comments in news details.
 */
@property (assign, nonatomic) BOOL appCommentsEnabledForNewsDetail;

/**
 Indicates if the comments are included in the news detail content.
 */
@property (assign, nonatomic) BOOL appCommentsEnabledInNewsDetailContent;

/**
 Indicates if the app has splash animation
 */
@property (assign, nonatomic) BOOL appSplashAnimationEnabled;

/**
 In the feed and details navigation bars can be dismissed with this flag.
 */
@property (assign, nonatomic) BOOL appSwipeToDismissNavigationBarEnabled;

/**
 It is used to refresh feed after the application goes background and foreground. It is the duration to refresh in minutes. Default value of it is -1. Give 0 to refresh everytime.
 */
@property (assign, nonatomic) NSInteger appMenuContentViewControllerRefreshTimeInMinutes;

#pragma mark - Network Configs

/**
 * @description New endpoint for no-seamless framework
 */
@property (strong,nonatomic) NSString* appNewEndPoint;

/**
 * @description Manifest url string for ads.
 */
@property (strong,nonatomic) NSString* adManifestEnd;

/**
 * @description Test Manifest url string for ads.
 */
@property (strong,nonatomic) NSString* adTestManifestUrl;

/**
 * @description persistent storage location for CBManifestModel
 */
@property (strong,nonatomic) NSString* adManifestStorageLocation;

/**
 * @description holds the name string for the homepage category of the app
 */
@property (strong,nonatomic) NSString* homepageAdCategory;

/**
 * @description holds the name string for the homepage category of the app
 */
@property (strong,nonatomic) NSString* livestreamVideoAdCategory;

/**
 * @description Url for checking whether forceUpdate needed
 */
@property (strong,nonatomic) NSString* appVersionCheckEnd;

/**
 * @description AppStore Url of the App
 */
@property (strong,nonatomic) NSString* appAppStoreUrl;

/**
 * @description Base url string of the server. Request urls are concatenated with this base url string. (BaseUrl+RequestUrl)
 */
@property (strong, nonatomic) NSString *networkBaseUrl;

/**
 * @description Url string that will be used to fetch news detail data.
 */
@property (strong, nonatomic) NSString *networkNewsDetailUrl;

/**
 * @description Url string that will be used to fetch comment data.
 */
@property (strong, nonatomic) NSString *networkCommentsUrl;

/**
 * @description Url string that will be used to fetch categories.
 */
@property (strong, nonatomic) NSString *networkCategoriesUrl;

/**
 * @description Url string that will be used to fetch category feeds.
 */
@property (strong, nonatomic) NSString *networkCategoryFeedUrl;

/**
 * @description Url string that will be used to fetch category feeds.
 */
@property (strong, nonatomic) NSString *networkFeedSearchUrl;

/**
 * @description Url string that will be used to fetch gallery detail data.
 */
@property (strong, nonatomic) NSString *networkGalleryDetailUrl;

/**
 Pattern array for html contents to match and remove the links that forward to mobile website.
 */
@property (nonatomic, strong) NSArray *networkHtmlLinkDropPatternArray;

/**
 @description Url string that comment addtion will be sent
 */
@property (nonatomic, strong) NSString *networkWriteCommentUrl;

/**
 Parallax ad url.
 */
@property (nonatomic, strong) NSString *networkParallaxAdUrl;

#pragma mark - Info Configs

/**
 Html Info url as a string.
 */
@property (strong, nonatomic) NSString *infoHtmlInfoUrl;

/**
 Map image url as a string.
 */
@property (strong, nonatomic) NSString *infoMapImageUrl;

/**
 Titles array containing title strings.
 */
@property (strong, nonatomic) NSArray *infoTitles;

/**
 Informations array containing information strings.
 */
@property (strong, nonatomic) NSArray *infoInformations;

/**
 Facebook URL to be opened in web browswer.It is optional.
 */
@property (nonatomic, strong) NSString *infoFacebookUrl;

/**
 Twitter URL to be opened in web browswer.It is optional.
 */
@property (nonatomic, strong) NSString *infoTwitterUrl;

/**
 Google plus URL to be opened in web browswer.It is optional.
 */
@property (nonatomic, strong) NSString *infoGooglePlusUrl;

/**
 Youtube URL to be opened in web browswer.It is optional.
 */
@property (nonatomic, strong) NSString *infoYouTubeUrl;

/**
 Background color of the view.
 */
@property (strong, nonatomic) UIColor *infoBackgroundColor;

/**
 Cell label color. NSString because we use this for a webview.
 */
@property (strong, nonatomic) NSString *infoCellLabelColor;

/**
 Cell label font.
 */
@property (strong, nonatomic) UIFont *infoCellLabelFont;

/**
 Cell background color.
 */
@property (strong, nonatomic) UIColor *infoCellBackgroundColor;

/**
 Separator color for the bottom bar.
 */
@property (strong, nonatomic) UIColor *infoSeparatorColor;

/**
 Follow us label text color.
 */
@property (nonatomic, strong) UIColor *infoBottomBarTextColor;

/**
 Follow us label font.
 */
@property (nonatomic, strong) UIFont *infoBottomBarTextFont;

/**
 Info section header title color.
 */
@property (nonatomic, strong) UIColor *infoSectionHeaderTitleColor;

/**
 Info section header title font.
 */
@property (nonatomic, strong) UIFont *infoSectionHeaderTitleFont;

#pragma mark - Feed Configs

/**
 If YES pull to refresh enabled
 */
@property (nonatomic, assign) BOOL feedPullToRefreshEnabled;

/**
 *  If set to YES then feed has paging, otherwise there is no paging in the feed and feed service should not have a page paremeter.
 */
@property (assign, nonatomic) BOOL feedPagingEnabled;

/**
 If YES seamless enabled for collectionView
 */
@property (nonatomic, assign) BOOL feedSeamlessCollectionViewEnabled;

/**
 If YES search UI is enabled.
 */
@property (nonatomic, assign) BOOL feedSearchEnabled;

/**
 Background color of the main feed view.
 */
@property (nonatomic, strong) UIColor *feedViewBackgroundColor;

/**
 Background color of the feed cells.
 */
@property (nonatomic, strong) UIColor *feedCellBackgroundColor;

/**
 Color of group cell container.
 */
@property (nonatomic, strong) UIColor *feedGroupCellContainerColor;

/**
 Color of the borders of the cells.
 */
@property (nonatomic, strong) UIColor *feedCellBorderColor;

/**
 Color of the title of the feed cells.
 */
@property (nonatomic, strong) UIColor *feedCellTitleColor;

/**
 Font of the title of the feed cells.
 */
@property (nonatomic, strong) UIFont *feedCellTitleFont;

/**
 Color of the category label in the feed cell.
 */
@property (nonatomic, strong) UIColor *feedCellCategoryColor;

/**
 Font of the category label in the feed cell.
 */
@property (nonatomic, strong) UIFont *feedCellCategoryFont;

/**
 Color of the spot label if it is included in a cell.
 */
@property (nonatomic, strong) UIColor *feedCellSpotColor;

/**
 Font of the spot label if it is included in a cell.
 */
@property (nonatomic, strong) UIFont *feedCellSpotFont;

/**
 UI Type of the cell. Selected by the client.
 */
@property (nonatomic, assign) CBFeedCellType feedCellType;

/**
 Color of the date label.
 */
@property (nonatomic, strong) UIColor *feedCellDateColor;

/**
 Font of the date label.
 */
@property (nonatomic, strong) UIFont *feedCellDateFont;

/**
 Background color of the no image feed cells.
 */
@property (nonatomic, strong) UIColor *feedNoImageCellBackgroundColor;

/**
 Color of the borders of the no image cells.
 */
@property (nonatomic, strong) UIColor *feedNoImageCellBorderColor;

/**
 Color of the title of the feed no image cells.
 */
@property (nonatomic, strong) UIColor *feedNoImageCellTitleColor;

/**
 Font of the title of the feed no image cells.
 */
@property (nonatomic, strong) UIFont *feedNoImageCellTitleFont;

/**
 Color of the category label in the no image feed cell.
 */
@property (nonatomic, strong) UIColor *feedNoImageCellCategoryColor;

/**
 Font of the category label in the no image feed cell.
 */
@property (nonatomic, strong) UIFont *feedNoImageCellCategoryFont;

/**
 Color of the spot label if it is included in a no image cell.
 */
@property (nonatomic, strong) UIColor *feedNoImageCellSpotColor;

/**
 Font of the spot label if it is included in a no image cell.
 */
@property (nonatomic, strong) UIFont *feedNoImageCellSpotFont;

/**
 Color of the date label in no image cell.
 */
@property (nonatomic, strong) UIColor *feedNoImageCellDateColor;

/**
 Font of the date label in no image cell.
 */
@property (nonatomic, strong) UIFont *feedNoImageCellDateFont;

/**
 Font of the dropdown cell's title.
 */
@property (nonatomic, strong) UIFont *dropdownCellTitleFont;

/**
 Color of the dropdown cell's title.
 */
@property (nonatomic, strong) UIColor *dropdownCellTitleColor;

/**
 Color of the subcategory view cell's title in feed
 */
@property (nonatomic, strong) UIColor *dropdownCellSeparatorColor;

/**
 Color of the subcategory view cell's title in feed
 */
@property (nonatomic, strong) UIColor *dropdownCellSelectionColor;

/**
 Color of the subcategory view background.
 */
@property (nonatomic, strong) UIColor *feedSubCategoryViewBackgroundColor;

/**
 Color of the loading indicator used for paging.
 */
@property (strong, nonatomic) UIColor *feedPagingActivityIndicatorColor;

#pragma mark - Comment Configs

/**
 Indicates that comment writing enabled for news.
 */
@property (nonatomic, assign) BOOL commentCommentWritingEnabled;

/**
 Indicates that nickname enabled for comment writing.
 */
@property (nonatomic, assign) BOOL commentNickNameEnabled;

/**
 Message shown when comment is sent.
 */
@property (nonatomic, strong) NSString *commentSuccessMessage;

/**
 Title of succes mesage sent when comment is sent.
 */
@property (nonatomic, strong) NSString *commentSuccessMessageTitle;

/**
 Message shown when there are no comment.
 */
@property (nonatomic, strong) NSString *commentNoCommentMessage;

/**
 @description Background color of the main view.
 */
@property (nonatomic, strong) UIColor *commentViewBackgroundColor;

/**
 @description Background color of the cells.
 */
@property (nonatomic, strong) UIColor *commentCellBackgroundColor;

/**
 @description Color of the borders of the cells.
 */
@property (nonatomic, strong) UIColor *commentCellBorderColor;

/**
 @description Color of the comment label of the cells.
 */
@property (nonatomic, strong) UIColor *commentColor;

/**
 @description Font of the comment label of the cells.
 */
@property (nonatomic, strong) UIFont *commentFont;

/**
 @description Color of the detail label in the cell.
 */
@property (nonatomic, strong) UIColor *commentDetailColor;

/**
 @description Font of the detail label in the cell.
 */
@property (nonatomic, strong) UIFont *commentDetailFont;

#pragma mark - Gallery Configs

/**
 Ads are started to be displayed from this index. It is zero based.
 */
@property (nonatomic, assign) NSInteger galleryAdStartIndex;

/**
 Ads are repeated with this repetition count.
 */
@property (nonatomic, assign) NSInteger galleryAdRepetitionCount;

/**
 The html string to be displayed when there is no html.
 */
@property (nonatomic, copy) NSString *galleryEmptyAdUrl;

/**
 If set to yes navigation bar is enabled and image count and other buttons are shown on the navigation bar. If not, those UI elements are shown on a separate bar on top.
 */
@property (nonatomic, assign) BOOL galleryNavigationBarEnabled;

/**
 Background color for the complete view.
 */
@property (nonatomic, strong) UIColor *galleryViewBackgroundColor;

/**
 Background color of the top bar or navigation bar in gallery.
 */
@property (nonatomic, strong) UIColor *galleryTopBarBackgroundColor;

/**
 Separator color for the bar which includes the control buttons.
 */
@property (nonatomic, strong) UIColor *galleryBarSeparatorColor;

/**
 Text color for the photo descriptions.
 */
@property (nonatomic, strong) UIColor *galleryDescriptionTextColor;

/**
 Text color for the label which shows the current index and the total photo count.
 */
@property (nonatomic, strong) UIColor *galleryPhotoCountLabelTextColor;

/**
 Font for the photo descriptions.
 */
@property (nonatomic, strong) UIFont *galleryDescriptionFont;

/**
 Font for the label which shows the current index and the total photo count.
 */
@property (nonatomic, strong) UIFont *galleryBottomBarPhotoCountFont;

#pragma mark - Navigation Bar Configs

/**
 Background color of the navigation bar.
 */
@property (nonatomic, strong) UIColor *navigationBarBackgroundColor;

/**
 Color of the navigation bar buttons
 */
@property (nonatomic, strong) UIColor *navigationBarTintColor;

/**
 @description Color of the title of the navigation bar.
 */
@property (nonatomic, strong) UIColor *navigationBarTitleColor;

/**
 @description Font of the title of the navigation bar.
 */
@property (nonatomic, strong) UIFont *navigationBarTitleFont;

/**
 @description Color of the bar button items of the navigation bar.
 */
@property (nonatomic, strong) UIColor *navigationBarBarButtonItemColor;

/**
 Determines the status bar color. When set to YES status bar text color is white, otherwise it's dark.
 */
@property (assign, nonatomic) BOOL navigationBarStatusBarHasLightContent;

/**
 CBSubcategoriesNavigationBarView title color;
 */
@property (nonatomic, strong) UIColor *navigationBarSubcategoryTitleColor;

/**
 CBSubcategoriesNavigationBarView font color;
 */
@property (nonatomic, strong) UIFont *navigationBarSubcategoryTitleFont;

/**
 Color of the arrow image.
 */
@property (nonatomic, strong) UIColor *navigationBarSubcategoryArrowColor;


#pragma mark - Left Menu Configs

/**
 Background color for the view.
 */
@property (nonatomic, strong) UIColor *leftMenuViewBackgroundColor;

/**
 Title color for the page header.
 */
@property (nonatomic, strong) UIColor *leftMenuHeaderTitleColor;

/**
 Title font for the page header.
 */
@property (nonatomic, strong) UIFont *leftMenuHeaderTitleFont;

/**
 Text color for the menu item's text.
 */
@property (nonatomic, strong) UIColor *leftMenuMenuItemTextColor;

/**
 Text font for the menu item's text.
 */
@property (nonatomic, strong) UIFont *leftMenuMenuItemTextFont;

/**
 Text color for the menu item's text for highlighted state.
 */
@property (nonatomic, strong) UIColor *leftMenuMenuItemHighlightTextColor;

/**
 Text font for the menu item's text for highlighted state.
 */
@property (nonatomic, strong) UIFont *leftMenuMenuItemHighlightTextFont;

/**
 Separator color between the page header and the menu.
 */
@property (nonatomic, strong) UIColor *leftMenuSeparatorColor;

/**
 Color for the menu item highlighting view.
 */
@property (nonatomic, strong) UIColor *leftMenuHighlightViewColor;

/**
 Tint color for the arrow image.
 */
@property (nonatomic, strong) UIColor *leftMenuArrowColor;

/**
 Tint color for the arrow image highlighted state.
 */
@property (nonatomic, strong) UIColor *leftMenuHighlightArrowColor;

#pragma mark - News Detail Configs

/**
 Background color for the complete view.
 */
@property (nonatomic, strong) UIColor *newsDetailViewBackgroundColor;

/**
 Color of the title text.
 */
@property (nonatomic, strong) UIColor *newsDetailTitleColor;

/**
 Font of the title text.
 */
@property (nonatomic, strong) UIFont *newsDetailTitleFont;

/**
 Color of the spot text.
 */
@property (nonatomic, strong) UIColor *newsDetailSpotColor;

/**
 Font of the spot text.
 */
@property (nonatomic, strong) UIFont *newsDetailSpotFont;

/**
 Color of the date text.
 */
@property (nonatomic, strong) UIColor *newsDetailDateColor;

/**
 Font of the date text.
 */
@property (nonatomic, strong) UIFont *newsDetailDateFont;

/**
 Color of the content text.
 */
@property (nonatomic, strong) NSString *newsDetailContentColor;

/**
 Font of the content text.
 */
@property (nonatomic, strong) UIFont *newsDetailContentFont;

/**
 Color of the view count text.
 */
@property (nonatomic, strong) UIColor *newsDetailViewCountColor;

/**
 Font of the view count text.
 */
@property (nonatomic, strong) UIFont *newsDetailViewCountFont;

/**
 @description Background color of the feedSmallImage cells.
 */
@property (nonatomic, strong) UIColor *newsDetailCellBackgroundColor;

/**
 @description Color of the borders of the cells.
 */
@property (nonatomic, strong) UIColor *newsDetailCellBorderColor;

/**
 @description Color of the title of the feedSmallImage cells.
 */
@property (nonatomic, strong) UIColor *newsDetailCellTitleColor;

/**
 @description Font of the title of the feedSmallImage cells.
 */
@property (nonatomic, strong) UIFont *newsDetailCellTitleFont;

/**
 @description Color of the category label in the feedSmallImage cell.
 */
@property (nonatomic, strong) UIColor *newsDetailCellCategoryColor;

/**
 @description Font of the category label in the feedSmallImage cell.
 */
@property (nonatomic, strong) UIFont *newsDetailCellCategoryFont;

/**
 @description Color of the spot label in the feedSmallImage cell.
 */
@property (nonatomic, strong) UIColor *newsDetailCellSpotColor;

/**
 @description Font of the spot label in the feedSmallImage cell.
 */
@property (nonatomic, strong) UIFont *newsDetailCellSpotFont;

/**
 @description Color of the suggested videos background view.
 */
@property (nonatomic, strong) UIColor *newsDetailSuggestedVideosBackgroundColor;

/**
 @description Color of the suggested videos title.
 */
@property (nonatomic, strong) UIColor *newsDetailSuggestedVideosTitleColor;

/**
 @description Font of the suggested videos title.
 */
@property (nonatomic, strong) UIFont *newsDetailSuggestedVideosTitleFont;

#pragma mark - Web View Configs

/**
 Background color for the complete view;
 */
@property (nonatomic, strong) UIColor *webViewViewBackgroundColor;

/**
 Separator color for the bar.
 */
@property (nonatomic, strong) UIColor *webViewBarSeparatorColor;

#pragma mark - Left Menu 2 Configs

/**
 Background color of the view. Some amount of alpha is given to the color before using it.
 */
@property (nonatomic, strong) UIColor *leftMenu2ViewBackgroundColor;

/**
 Label color.
 */
@property (nonatomic, strong) UIColor *leftMenu2CategoryLabelColor;

/**
 Label color when selected.
 */
@property (nonatomic, strong) UIColor *leftMenu2CategoryLabelSelectedColor;

/**
 Label font when selected.
 */
@property (nonatomic, strong) UIFont *leftMenu2CategoryLabelSelectedFont;

/**
 Selected cell background color.
 */
@property (nonatomic, strong) UIColor *leftMenu2SelectedCellBackgroundColor;

/**
 Cell separator color.
 */
@property (nonatomic, strong) UIColor *leftMenu2SeparatorColor;

/**
 Label font.
 */
@property (nonatomic, strong) UIFont *leftMenu2CategoryLabelFont;

/**
 Back title text color;
 */
@property (nonatomic, strong) UIColor *leftMenu2BackTitleColor;

/**
 Back button title font.
 */
@property (nonatomic, strong) UIFont *leftMenu2BackFont;

/**
 Arrow color;
 */
@property (nonatomic, strong) UIColor *leftMenu2ArrowColor;

/**
 Back arrow color;
 */
@property (nonatomic, strong) UIColor *leftMenu2BackArrowColor;

#pragma mark - Left Menu 3 Configs

/**
 Background color of the view. Some amount of alpha is given to the color before using it.
 */
@property (nonatomic, strong) UIColor *leftMenu3ViewBackgroundColor;

/**
 Label color.
 */
@property (nonatomic, strong) UIColor *leftMenu3CategoryLabelColor;

/**
 Label font.
 */
@property (nonatomic, strong) UIFont *leftMenu3CategoryLabelFont;

/**
 Section title label color.
 */
@property (nonatomic, strong) UIColor *leftMenu3TitleCategoryLabelColor;

/**
 Section title label font.
 */
@property (nonatomic, strong) UIFont *leftMenu3TitleCategoryLabelFont;

/**
 Cell separator color.
 */
@property (nonatomic, strong) UIColor *leftMenu3SeparatorColor;

/**
 Arrow color;
 */
@property (nonatomic, strong) UIColor *leftMenu3ArrowColor;


#pragma mark - Left Menu 4 Configs

/**
 Background color of the view. Some amount of alpha is given to the color before using it.
 */
@property (nonatomic, strong) UIColor *leftMenu4ViewBackgroundColor;

#pragma mark - Error Configs

/**
 Text color of the error messages.
 */
@property (strong, nonatomic) UIColor *errorTextColor;

/**
 Font of the error messages.
 */
@property (strong, nonatomic) UIFont *errorTextFont;

#pragma mark - Loading Configs

/**
 Background color of the loading view.
 */
@property (strong, nonatomic) UIColor *loadingBackgroundColor;

/**
 Activity indicatior color.
 */
@property (strong, nonatomic) UIColor *loadingIndicatorColor;

#pragma mark - Settings Configs

/**
 Background color of the view.
 */
@property (strong, nonatomic) UIColor *settingsBackgroundColor;

/**
 Notifications label color.
 */
@property (strong, nonatomic) UIColor *settingsNotificationsLabelColor;

/**
 Notifications label font.
 */
@property (strong, nonatomic) UIFont *settingsNotificationsLabelFont;

/**
 Font size label text color.
 */
@property (strong, nonatomic) UIColor *settingsFontSizeLabelColor;

/**
 Font size label font.
 */
@property (strong, nonatomic) UIFont *settingsFontSizeLabelFont;

/**
 Slider tint color.
 */
@property (strong, nonatomic) UIColor *settingsSliderTintColor;

/**
 Switch tint color.
 */
@property (strong, nonatomic) UIColor *settingsSwitchTintColor;

/**
 Cell background color.
 */
@property (strong, nonatomic) UIColor *settingsCellBackgroundColor;

#pragma mark - Category Model Configs

/**
 Id of the category.
 */
@property (strong, nonatomic) NSString *categoryModelCategoryId;

/**
 Name of the category.
 */
@property (strong, nonatomic) NSString *categoryModelName;

/**
 Shows if the category has its own feed.
 */
@property (strong, nonatomic) NSString *categoryModelHasFeed;

/**
 Subcategory array of this category.
 */
@property (strong, nonatomic) NSString *categoryModelSubcategories;

/**
 The web link of the category
 */
@property (nonatomic, strong) NSString *categoryModelWebLink;


#pragma mark - News Detail Model Configs

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *newsDetailModelDetailId;

/**
 Date of the news.
 */
@property (strong, nonatomic) NSString *newsDetailModelDate;

/**
 Category of the news.
 */
@property (strong, nonatomic) NSString *newsDetailModelCategory;

/**
 Title of the news.
 */
@property (strong, nonatomic) NSString *newsDetailModelTitle;

/**
 Secondary title of the news.
 */
@property (strong, nonatomic) NSString *newsDetailModelSpot;

/**
 Image url of the news.
 */
@property (strong, nonatomic) NSString *newsDetailModelImageUrl;

/**
 Url of the video.
 */
@property (strong, nonatomic) NSString *newsDetailModelVideoUrl;

/**
 Url of the news.
 */
@property (strong, nonatomic) NSString *newsDetailModelUrl;

/**
 HTML content of the news.
 */
@property (strong, nonatomic) NSString *newsDetailModelContent;

/**
 Gallery id  the news.
 */
@property (strong, nonatomic) NSString *newsDetailModelGalleryId;

/**
 Video duration of the news detail.
 */
@property (strong, nonatomic) NSString *newsDetailModelDuration;

/**
 Video view count of the news detail.
 */
@property (strong, nonatomic) NSString *newsDetailModelViewCount;

/**
 Suggested videos of the news detail.
 */
@property (strong, nonatomic) NSString *newsDetailModelSuggestedVideos;

/**
 Comments of the news detail.
 */
@property (nonatomic, strong) NSString *newsDetailModelComments;

#pragma mark - Comment Model Configs

/**
 Id for the comment.
 */
@property (strong, nonatomic) NSString *commentModelCommentId;

/**
 Content of the comment.
 */
@property (strong, nonatomic) NSString *commentModelComment;

/**
 The date that comment has been made.
 */
@property (strong, nonatomic) NSString *commentModelDate;

/**
 Username of the commentator.
 */
@property (strong, nonatomic) NSString *commentModelUsername;

#pragma mark - Gallery Detail Model Configs

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *galleryDetailModelDetailId;

/**
 Date of the news.
 */
@property (strong, nonatomic) NSString *galleryDetailModelDate;

/**
 Category of the news.
 */
@property (strong, nonatomic) NSString *galleryDetailModelCategory;

/**
 Photos in the gallery.
 */
@property (strong, nonatomic) NSString *galleryDetailModelPhotos;

/**
 Share url of the gallery.
 */
@property (strong, nonatomic) NSString *galleryDetailModelUrl;

/**
 Title of the news.
 */
@property (strong, nonatomic) NSString *galleryDetailModelTitle;

#pragma mark - Gallery Photo Model Configs

/**
 Id for the model.
 */
@property (strong, nonatomic) NSString *galleryPhotoModelPhotoId;

/**
 Description of the photo.
 */
@property (strong, nonatomic) NSString *galleryPhotoModelPhotoDescription;

/**
 URL of the image.
 */
@property (strong, nonatomic) NSString *galleryPhotoModelImageUrl;

#pragma mark - Feed Model Configs

/**
 Type of the feed. For example: News, advertorial or video.
 */
@property (strong, nonatomic) NSString *feedModelType;

/**
 Id of the feed.
 */
@property (strong, nonatomic) NSString *feedModelFeedId;

/**
 Category of the feed.
 */
@property (strong, nonatomic) NSString *feedModelCategory;

/**
 Date of the feed.
 */
@property (strong, nonatomic) NSString *feedModelDate;

/**
 Title of the feed.
 */
@property (strong, nonatomic) NSString *feedModelTitle;

/**
 Sub title of the feed.
 */
@property (strong, nonatomic) NSString *feedModelSpot;

/**
 Web url if exists.
 */
@property (strong, nonatomic) NSString *feedModelUrl;

/**
 URL of the feed image.
 */
@property (strong, nonatomic) NSString *feedModelImageUrl;

/**
 Original width of the feed image.
 */
@property (strong, nonatomic) NSString *feedModelImageWidth;

/**
 Original height of the feed image.
 */
@property (strong, nonatomic) NSString *feedModelImageHeight;

/**
 Cell type of the feed.
 */
@property (strong, nonatomic) NSString *feedModelCellType;

#pragma mark - Group News Feed Model Config

/**
 Feed model array key.
 */
@property (strong, nonatomic) NSString *groupNewsFeedModelGroupNews;

/**
 Title key of the group news.
 */
@property (strong, nonatomic) NSString *groupNewsFeedModelTitle;
/**
 Generic content URL for feeds to use with DFP Google
 */
@property (strong, nonatomic) NSString *dfpGenericContentUrl;
#pragma mark - AdvertisingId
/**
 @brief set the Advertising UUID for the app.
 @param uuid the UUID you want set, as NSString
 */
- (void)setAdvertisingUUID:(NSString*)uuid;
/**
 @return advertising UUID for the app
 */
- (NSString*)getAdvertisingUUID;

@end
