//
//  CBCommentModel.h
//  Carbon
//
//  Created by Semih Cihan on 20/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"

@interface CBCommentModel : CBBaseModel

@property (strong, nonatomic) NSString *commentId;
@property (strong, nonatomic) NSString *comment;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *username;

@end
