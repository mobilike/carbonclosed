//
//  CBNewsDetailLogic.h
//  Carbon
//
//  Created by Semih Cihan on 06/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBNewsDetailModel.h"
#import "CBBaseLogic.h"

@class CBNewsDetailModel,CBCommentLogic;


@interface CBNewsDetailLogic : CBBaseLogic <CBDataLoaderProtocol>

@property (nonatomic, assign) BOOL isVideoDetail;

/**
 Delegate of CBNewsDetailLogic.
 */
@property (nonatomic, weak) id<CBDataLoaderDelegate> delegate;

/**
 NewsDetail object loaded by the logic.
 */
@property (nonatomic, strong) CBNewsDetailModel *newsDetail;

/**
 Feed model of the news detail.
 */
@property (nonatomic, strong) CBFeedModel *feed;

/**
 Creates an instance with the given feed.
 */
- (instancetype)initWithFeed:(CBFeedModel *)feed;

/**
 Creates and returns comment logic.
 @return CBCommentLogic object with the detail id.
 */
- (CBCommentLogic *)getCommentLogic;

- (NSString *)validateModel:(CBNewsDetailModel *)detailModel;


@end
