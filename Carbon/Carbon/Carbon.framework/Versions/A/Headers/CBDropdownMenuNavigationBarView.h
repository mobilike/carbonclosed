//
//  CBDropdownNavigationBarView.h
//  Carbon
//
//  Created by Necati Aydın on 03/11/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 A combobox like view to be used in navigation bar. It consists of an image in the top and a label in the bottom with a dropdown arrow image.
 */
@interface CBDropdownMenuNavigationBarView : UIView

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *mainTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *titleImageView;
@property (nonatomic, weak) IBOutlet UIImageView *dropDownImageView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *titleImageViewTopSpace;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *titleImageViewToTitleLabelSpace;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *titleLabelToDropDownImageViewHorizontalSpace;

/**
 Creates an instance by loading from nib.
 warning Default size for the view is (100,54). It can be modified easily by changing the frame of bounds of the view.
 */
+ (instancetype)instanceFromNib;

/**
 Animates the arrow to the top or to the down.
 */
- (void)rotateDropdownImageAnimated;

/**
 Sets the arrow color.
 */
- (void)setDropDownImageColor:(UIColor *)color;

@end
