//
//  CBCommentLogic.h
//  Carbon
//
//  Created by Semih Cihan on 21/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseLogic.h"

@interface CBCommentLogic : CBBaseLogic <CBDataLoaderProtocol>

- (instancetype)initWithFeed:(CBFeedModel *)feed;

/**
 @description Data for CommentViewController instances.
 */
@property (strong, nonatomic) NSArray *data;

/**
 @description Delegate of CBFeedLogic.
 */
@property (weak, nonatomic) id<CBDataLoaderDelegate> delegate;

/**
 @description Feed obejct that the comment object belongs to.
 */
@property (nonatomic, strong) CBFeedModel *feed;

@end
