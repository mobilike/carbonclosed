//
//  CBBaseCollectionViewCell.h
//  Carbon
//
//  Created by Semih Cihan on 08/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kCellBorderLineWidth = 0.5f;

@interface CBBaseCollectionViewCell : UICollectionViewCell

+ (NSString *)reuseIdentifier;

@end
