//
//  ParallaxFlowLayout.h
//  Carbon
//
//  Created by Necati Aydın on 13/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CBParallaxFlowLayout : UICollectionViewFlowLayout

/**
 It is the total offset when the cell is at the top of the screen or at the bottom of the screen. The more maxParallaxOffset, the more offset you will have.
 */
@property (nonatomic, assign) CGFloat maxParallaxOffset;

@end
