//
//  CBCrashManager.h
//  Carbon
//
//  Created by Necati Aydın on 12/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CBCrashManagerProtocol <NSObject>


@optional
/**
 Implement this to start crashlytics.
 */
+ (void)startCrashlytics;

@end

@interface CBCrashManager : NSObject <CBCrashManagerProtocol>



@end
