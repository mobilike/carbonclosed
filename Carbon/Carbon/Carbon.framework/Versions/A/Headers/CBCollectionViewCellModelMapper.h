//
//  CBCollectionViewCellCustomizer.h
//  Carbon
//
//  Created by Necati Aydın on 18/01/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBBaseCollectionViewCell, CBBaseModel, CBFeedCell, CBFeedModel, CBParallaxCell, CBParallaxAdModel, CBCommentCell, CBCommentModel;

@interface CBCollectionViewCellModelMapper : NSObject

+ (instancetype)sharedInstance;

- (void)configureCell:(CBBaseCollectionViewCell *)cell withModel:(CBBaseModel *)model;


- (void)configureFeedCell:(CBFeedCell *)feedCell withFeedModel:(CBFeedModel *)feed;
- (void)configureParallaxCell:(CBParallaxCell *)parallaxCell withParallaxModel:(CBParallaxAdModel *)adModel;
- (void)configureCommentCell:(CBCommentCell *)cell withCommentModel:(CBCommentModel *)model;

@end
