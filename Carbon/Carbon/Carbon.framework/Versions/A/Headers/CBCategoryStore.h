//
//  CBCategoryStore.h
//  Carbon
//
//  Created by Necati Aydın on 24/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBCategoryModel;

@interface CBCategoryStore : NSObject

@property (nonatomic, strong, readonly) NSArray *categories;

@property (strong, nonatomic) CBCategoryModel *selectedCategory;

/**
 Singleton object.
 */
+ (instancetype)sharedStore;

/**
 Fetches categories on the first call using the networkmanager and caches the data. For the following calls returns the cached data.
 @param completion Completion block to be called. NSArray holds categories on successfull calls, nil otherwise.
 */
- (void)getCategoriesWithCompletion:(void (^)(NSArray *categories, NSError *error))completion;

/**
 Using the data fetched from the service, searches the array of category models (and their subcategories) for the given categoryId and returns the category if found.
 @param categoryId Searched categoryId.
 @return Model object if found, nil otherwise.
 */
- (CBCategoryModel *)getCategoryWithId:(NSString *)categoryId;

/**
 Using the data fetched from the service, searches the array of category models (and their subcategories) for the given category name and returns the category if found.
 @param categoryName Searched category name.
 @return Model object if found, nil otherwise.
 */
- (CBCategoryModel *)getCategoryWithName:(NSString *)categoryName;

/**
 *  Gets a category from the store if categories are fetched. Otherwise it fetches the categories before getting the category from the store.
 *
 *  @param categoryId Id of the category to be fetched.
 *  @param completion Block to be executed after fetching.
 */
- (void)getCategoryByFetchingCategoriesWithId:(NSString *)categoryId
                                   completion:(void (^)(CBCategoryModel *category, NSError *error))completion;

@end
