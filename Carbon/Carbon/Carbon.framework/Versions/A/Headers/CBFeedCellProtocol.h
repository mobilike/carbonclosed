//
//  CBFeedCellProtocol.h
//  Carbon
//
//  Created by Semih Cihan on 24/06/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Feed cells need to conform to CBFeedCellProtocol.
 */
@protocol CBFeedCellProtocol <NSObject>

/**
 Reuse identifier to be used in collection view cell.
 */
+ (NSString *)reuseIdentifier;

/**
 Returns the calculated size for the given width.
 @param width Width of the cell.
 @return Calculated size for the given width.
 */
+ (CGSize)sizeForWidth:(CGFloat)width;

@end
