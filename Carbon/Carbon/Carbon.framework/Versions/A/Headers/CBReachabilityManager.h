//
//  ReachabilityManager.h
//  Carbon
//
//  Created by Semih Cihan on 09/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "Reachability.h"

@interface CBReachabilityManager : Reachability

/**
 Starts reachability observing. Whenever reachability is lost, pops up an alert view. The only action of the alert is to retry reachability. If reachable popup is dismissed, if not popup is shown again.
 */
+ (void)startReachability;

@end
