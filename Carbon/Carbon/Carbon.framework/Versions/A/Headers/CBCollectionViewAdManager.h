//
//  CBCollectionViewAdManager.h
//  Carbon
//
//  Created by Tolga Caner on 22/03/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @protocol CBNewsDetailFeedAdDelegate
 @description A delegate for CBNewsDetailViewController's usage. Handles operations for News Detail Ad Operations.
 */
@protocol CBNewsDetailFeedAdDelegate <NSObject>

/**
 @brief called to inform the delegate about a new ad.
 @param size size of the ad received.
 */
- (void) didReceiveFeedAdOfSize:(CGSize)size;

@end

@import GoogleMobileAds;

/**
 @class CBCollectionViewAdManager
 @superclass SuperClass: NSObject\n
 @discussion Manages the feed ads in collection views. Does not support UITableView. Meant to be used by instances of CBFeedViewController to manage the ads in the feeds.
 */
@interface CBCollectionViewAdManager : NSObject <GADBannerViewDelegate, UICollectionViewDelegate>

/**
 @brief Data source of the collection view
 */
@property (nonatomic, weak, readwrite) NSMutableArray * dataSource;
/**
 @brief The UIViewController subclass the instance belongs to.
 */
@property (nonatomic, weak, readwrite) id viewController;
/**
 @brief Contains the list of cells which are visible when the view appears first.
 */
@property (nonatomic, strong) NSArray *firstVisibleCells;
/**
 @brief ads that are inserted into the feed
 */
@property (nonatomic, strong) NSMutableArray * ads;
/**
 @brief Set containing all the cells in the collection view
 */
@property (nonatomic, strong) NSMutableSet *cells;
/**
 @brief Delegate used to inform the News Detail subclass on ads loaded.
 */
@property (nonatomic, weak) id <CBNewsDetailFeedAdDelegate> newsDetailfeedAdDelegate;
/**
 @brief Delegate used to inform the News Detail subclass on ads loaded.
 */
@property (nonatomic, weak) id delegate;

/**
 @brief Method to get a display ad unit id for given parameters
 @param collectionView the collection view the ads shall be inserted to.
 @param dataSource initial dataSource of the collection view
 @param viewController UIViewController that hosts the UICollectionView instance.
 @return CBCollectionViewAdMananger initializes and returns an instance with the given parameters.
 */
-(id)initWithCollectionView:(UICollectionView*)collectionView
                 dataSource:(NSMutableArray*)dataSource
             viewController:(id)viewController;

/**
 @brief Method called by CBSeamlessViewController to fill the collectionview with ads.
 @param entity name of the category for the ads (e.g. "News", "Sports")
 @param subCategory deprecated parameter
 @param successBlock deprecated parameter
 @param failureBlock deprecated parameter
 */
-(void)getAdsWithEntity:(NSString*)entity
            subCategory:(NSString*)subCategory
           successBlock:(void(^)(void))successBlock
           failureBlock:(void(^)(NSError * error))failureBlock;

/**
 @brief Should be called to decide if an ad should be shown on for specific index path
 @param indexPath the index path to check
 */
-(BOOL)shouldShowAdAtIndexPath:(NSIndexPath*)indexPath;

/**
 @brief Called to change the data source and reset all the ads of a collection view.
 @param dataSource new data source.
 */
-(void)changeDataSource:(NSMutableArray *)dataSource;

/**
 @brief cleans the data source, deleting all the relevant data.
 */
-(void)cleanDataSource;
/**
 @brief clears ad cells that are queued to be removed.
 */
-(void)clearAdsToRemove;

/**
 @brief UICollectionViewDelegate replica method
 */
-(UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath;
/**
 @brief UICollectionViewFlowLayout replica method
 */
-(CGSize)sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

@end
