//
//  CBGalleryDetailViewController.h
//  Carbon
//
//  Created by Necati Aydın on 20/07/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBGalleryViewController.h"
#import "CBGalleryDetailLogic.h"
#import "CBStartType.h"

@interface CBGalleryDetailViewController : CBGalleryViewController  <CBDataLoaderDelegate>

@property (nonatomic, strong) CBGalleryDetailLogic *logic;
@property (nonatomic, assign) StartType startType;
@property (nonatomic, copy) NSString *feedTitle;

@end
