//
//  CBNewsDetailView.h
//  Carbon
//
//  Created by Necati Aydın on 18/12/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBBaseView.h"

@class CBNewsDetailViewController,CBFontView, CBNewsDetailThumbnailView, CBNewsDetailContentView, CBNewsDetailSuggestedVideosView,CBNewsDetailCommentView, MASConstraint;

/**
 This class is intended to be used by CBNewsDetailViewController.
 */
@interface CBNewsDetailView : CBBaseView

- (instancetype)initWithFrame:(CGRect)frame viewController:(CBNewsDetailViewController *)viewController;

@property (nonatomic, weak) CBNewsDetailViewController *viewController;

//subviews of main view
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) CBFontView *fontView;
@property (nonatomic, strong) UIView *mmaContainerView;

//subviews of scroll view
@property (nonatomic, strong) CBNewsDetailThumbnailView *thumbnailView;
@property (nonatomic, strong) CBNewsDetailContentView *contentView;
@property (nonatomic, strong) UIView *mreContainerView;
@property (nonatomic, strong) CBNewsDetailCommentView *commentView;
@property (nonatomic, strong) CBNewsDetailSuggestedVideosView *suggestedVideosView;

//layout constraints
@property (nonatomic, strong) MASConstraint *suggestedVideosViewHeight;
@property (nonatomic, strong) MASConstraint *thumbnailViewAspectRatio;

/**
 Adds any view to the scroll view at the given vertical index.
 */
- (void)addView:(UIView *)view toScrollViewAtIndex:(NSInteger)index completion:(void (^)())completion;

/**
 Updates web view height constraint by calculating the web view size.
 */
- (void)updateWebViewHeight;

/**
 It first set web view height to 1 by setting the height constraint. Then upates web view height by calculating the height.
 */
- (void)forceUpdateWebViewHeight;

/**
 Calculates the height of the view and updates it by updating its' constraint.
 */
- (void)updateSuggestedVideosViewHeight;

/**
 If comment view does not exist, it creates the comment view and it to the scroll view.
 */
- (void)addCommentViewWithCommentExists:(BOOL)commentExists;

- (void)updateThumbnailImageRatio:(CGFloat)ratio;
- (void)addMREView:(UIView *)mreView;
- (void)addMMAView:(UIView *)mmaView;
- (void)setThumbnailImageAnimated:(UIImage *)image;

@end


#pragma mark - Thumbnail View

@interface CBNewsDetailThumbnailView : CBBaseView

@property (nonatomic, strong) UIImageView *thumbnailImageView;
@property (nonatomic, strong) UIImageView *playImageView;

@end


#pragma mark - Content View

@interface CBNewsDetailContentView : CBBaseView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *spotLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIWebView *webView;

@end


#pragma mark - Suggested Videos View

@interface CBNewsDetailSuggestedVideosView : CBBaseView

@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UICollectionView *collectionView;

@end


#pragma mark - News Detail Comment View

@interface CBNewsDetailCommentView : CBBaseView

- (instancetype)initWithCommentExists:(BOOL)commentExists;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *allCommentsButton;
@property (nonatomic, strong) UILabel *noCommentLabel;
@property (nonatomic, strong) UIImageView *noCommentImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *writeCommentButton;

@end