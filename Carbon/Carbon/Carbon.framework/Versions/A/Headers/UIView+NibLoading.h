//
//  UIView+NibLoading.h
//  Carbon
//
//  Created by Necati Aydın on 03/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIView(NibLoading)

/**
 Creates an instance by loading it from the nib in framwork bundle.
 */
+ (instancetype)loadFromNIBInFrameworkBundle;

/**
 Creates an instance by loading it from the nib in main bundle.
 */
+ (instancetype)loadFromNIB;

+ (instancetype)loadFromNIBWithName:(NSString *)nibName;

/**
 Let's you access the first view controller which is controlling your view
 */
- (UIViewController *)parentViewController;
/* //TODO: Add in a new file for UIViews
 - (UIViewController *)parentViewController {
 UIResponder *responder = self;
 while ([responder isKindOfClass:[UIView class]])
 responder = [responder nextResponder];
 return (UIViewController *)responder;
 }
*/


@end
