//
//  CBNetworkRequestUrlBuilder.h
//  Carbon
//
//  Created by Necati Aydın on 29/01/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBCategoryModel, CBFeedModel, CBCommentModel;

@interface CBNetworkRequestUrlBuilder : NSObject

+ (instancetype)sharedInstance;

- (NSString *)getBaseUrl;

- (NSString *)getCategoriesUrl;

- (NSString *)getCategoryFeedUrlWithCategory:(CBCategoryModel *)category page:(NSInteger)page;
- (NSString *)getSearchFeedUrlWithSearchString:(NSString *)searchString page:(NSInteger)page;

- (NSString *)getNewsDetailUrlWithFeed:(CBFeedModel *)feed;
- (NSString *)getGalleryDetailUrlWithFeed:(CBFeedModel *)feed;

- (NSString *)getCommentsUrlWithFeed:(CBFeedModel *)feed;
- (NSString *)sendCommentUrlWithComment:(CBCommentModel *)comment feed:(CBFeedModel *)feed parameters:(NSDictionary **)params;

- (NSString *)getParallaxAdInfoUrl;

- (NSString *)getManifestUrlWithBundleId:(NSString *)bundleId;
- (NSString *)getTestManifestsUrlWithParameter:(NSString *)parameter;
- (NSString *)getVersionCheckUrlWithBundleId:(NSString *)bundleId version:(NSString*)version isWiFi:(BOOL)wifi;

@end
