//
//  CBWebContentViewController.h
//  Carbon
//
//  Created by Fırat Karataş on 14/07/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseViewController.h"
#import "CBWebContentLogic.h"
#import "CBLeftMenu.h"

@class CBLeftMenuContainerViewController;

@interface CBWebContentViewController : CBBaseViewController

@property (nonatomic, strong) CBWebContentLogic *logic;

/**
 Delegate of the view controller.
 */
@property (weak, nonatomic) id<CBMenuContentViewControllerDelegate> delegate;

@end
