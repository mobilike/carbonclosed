//
//  CBVideoPlayerViewController.h
//  Carbon
//
//  Created by Tolga Caner on 13/04/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "GoogleMediaFramework.h"
#import <GoogleInteractiveMediaAds/IMAAdEvent.h>
@interface CBVideoPlayerViewController : GMFPlayerViewController

- (void) dismiss;
- (void) adClicked;

- (void) setMidRollTag:(NSString*)midRollTag midRollFrequency:(NSNumber*)frequency withPostrollTag:(NSString*)postRollTag;
- (void) adServiceDidReceiveAdEvent:(IMAAdEvent*) event;

@property (nonatomic,assign) BOOL dismissUponPostrollFinish;

- (void) setParentVideoViewController:(UIViewController*)viewController;

@end
