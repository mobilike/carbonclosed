//
//  CBNewsDetailViewController.h
//  Carbon
//
//  Created by Semih Cihan on 06/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBSeamlessViewController.h"
#import "CBNewsDetailLogic.h"
#import "CBStartType.h"
#import "CBFontView.h"
#import "CBNewsDetailView.h"

@class CBFeed4Cell, CBCommentCell;


#pragma mark - Class

@interface CBNewsDetailViewController : CBSeamlessViewController <CBDataLoaderDelegate, UIWebViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CBFontViewDelegate>

@property (strong, nonatomic) CBNewsDetailView *view;

#pragma mark -

@property (nonatomic, assign) StartType startType;
@property (nonatomic, strong) CBNewsDetailLogic *logic;
@property (nonatomic, assign) BOOL commentButtonIncludedInNavigationBar;
@property (nonatomic, assign) BOOL commentsIncludedInContent;

- (void)fontViewAnimationCompleted:(CBFontView *)fontView;

#pragma mark - Image Handling

/**
 Default value is -1. If this value is overriden with a value which is greater and equal than 0, image view aspect ratio is updated. The value should be given as widht/height;
 */
@property (nonatomic, assign) CGFloat imageViewAspectRatio;

/**
 Called when the image is loading. Image view  ratio is updated depending on the loaded image and if the image is not loaded a placeholder image is set.
 */
- (void)handleImageLoading:(UIImage *)image error:(NSError *)error;

#pragma mark - Action

/**
 Back button action.
 */
- (IBAction)backButtonTapped:(id)sender;

/**
 Font button action. It opens the font view.
 */
- (IBAction)fontButtonTapped:(id)sender;

/**
 Comment button action. Presents a view controller to display comments.
 */
- (IBAction)commentButtonTapped:(id)sender;

/**
 Write comment button action. Presents a view controller to write comments.
 */
- (IBAction)writeCommentButtonTapped:(id)sender;

/**
 When an image is tapped, first it is checked that if it is video detail or not.
 If it is video detail and there is video url, video is displayed, if not thumnbail image is displayed.
 If it is not video detail and there is gallery id, gallery is displayed, if not thumbnail image is displayed.
 */
- (IBAction)thumbnailImageTapped:(id)sender;

/**
 Presents the thumbnail image full screen.
 */
- (void)actionThumbnailImagePresent;

/**
 Checks if there is gallery id, if there is presents gallery, if not presents thumbnail image.
 */
- (void)actionGalleryPresent;

/**
 Checks if there is video url, if there is presents gallery, if not presents thumbnail image.
 */
- (void)actionVideoPresent;

/**
 Checks if there is video url and calls the completion block after presenting the video player, if there is presents gallery, if not presents thumbnail image.
 */
- (void)actionVideoPresentWithCompletion:(void (^ __nullable)(CBVideoPlayer* videoPlayer))completion playbackCompletion:(void (^ __nullable)(void))playbackCompletion;

- (void)actionVideoPresentWithCompletion:(void (^ __nullable)(CBVideoPlayer* videoPlayer))completion playbackCompletion:(void (^ __nullable)(void))playbackCompletion playbackResumed:(void (^ __nullable)(void))playbackResumed playbackPaused:(void (^ __nullable)(void))playbackPaused;

/**
 Share button action.
 */
- (IBAction)shareButtonTapped:(id)sender;

#pragma mark - Navigation Bar

/**
 It is called from customizeNavigationBar if the detail is not contained. If it is contained, this method is intended to be called from outside when it is needed.
 */
- (void)styleNavigationItems;

/**
 It is called from styleNavigationItems: to style the right navigation item. commentsEnabled parameter is given a value from the app config.
 */
- (void)styleRightNavigationItem:(BOOL)commentsEnabled;

/**
 Contained view controllers decide the navigation bar should be displayed or not wheh swiped among other ones.
 */
- (BOOL)shouldHideNavigationBar;

/**
 Contained view controllers decide the navigation bar should be hidden with the swipe animation.
 */
- (BOOL)shouldHideNavigationBarWithSwipe;

/**
 Enables/disables navigation bar swipe to dismiss if it is permitted from the config.
 */
- (void)enableNavigationBarSwipeToDismiss:(BOOL)enabled;

#pragma mark - Style and Configure

/**
 Model is mapped to the UI elements in this method. Then UI is styled. Should be sublcassed for custom behaviour.
 */
- (void)configure;

/**
 Model is mapped to the UI elements in this method. Should be sublcassed for custom behaviour.
 */
- (void)configureWithModel;

/**
 Styles the view elemens besides web view.
 @param fontIncrementAmount Increment or decrement amount for the fonts.
 */
- (void)styleByIncrementingFontBy:(CGFloat)fontIncrementAmount;

/**
 Returns the full html string.
 @param content HTML content without css and the other tags such as <html> or <body>. It should be only news content in some kind of html tags.
 */
- (NSString *)styleContent:(NSString *)content withCSSForMaximumWidth:(CGFloat)width fontIncrementAmount:(CGFloat)fontIncrementAmount;

/**
 The css file name. It should be given without .css extension.
 */
- (NSString *)cssFileName;

/**
 Loaded or created css string including the font increment amount.
 */
- (NSString *)cssStringWithFontIncrementAmount:(CGFloat)fontIncrementAmount;

/**
 Returns the default unformatted CSS String
 */
- (nonnull NSString *)unformattedDefaultCSSString;

#pragma mark - Ads

/**
 Entity name which does not contain the ad type. For example it can be, Economy-Detail.
 */
- (NSString *)entityNameWithoutType;

/**
 Method to start detail ads.
 */
- (void)startAds;

/**
 Method to remove detail ads.
 */
- (void)removeAds;

#pragma mark - UICollection View

/**
 Cell to be used in suggested news. It can be used to add custom cell classes which is a subclass of CBFeed4Cell.
 */
- (CBFeed4Cell *)reusableSuggestedVideoCellForIndexPath:(NSIndexPath *)indexPath;

/**
 Cell to be used in comments. It can be used to add custom cell classes which is a subclass of CBCommentCell.
 */
- (CBCommentCell *)reusableCommentCellForIndexPath:(NSIndexPath *)indexPath;
/**
 Hook method that is called when an ad is received to be displayed inside the collection view
 */
- (void) didReceiveFeedAdOfSize:(CGSize)size;

#pragma Colection View Delegate & Data Source Methods
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section;
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section;
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;



@end

