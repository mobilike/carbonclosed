//
//  CBHTTPRequestOperationManager.h
//  Carbon
//
//  Created by Semih Cihan on 13/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
@class CBNewsDetailModel, CBGalleryDetailModel, CBCategoryModel, CBCommentModel, CBFeedModel, CBParallaxAdModel;

typedef void (^FailureBlock)(NSError *error);

@interface CBNetworkManager : AFHTTPRequestOperationManager

/**
 Returns the singleton shared instance.
 @return Singleton object.
 */
+ (instancetype)sharedInstance;

/**
 Override this method to make all the configurations you want to make on the CBNetworkManager knowing it is a subclass of AFHTTPRequestOperationManager.
 */
- (void)makeInitializationConfigurations;

/**
 Cancels all the requests in the operation queue of the CBNetworkManager.
 @return void
 */
- (void)cancelRunningServices;

/**
 Fetches new manifest json from the surver.
 No parameters as of now but that will likely be changed.
 */
- (void)getManifest: (void (^)(id))successBlock
       failureBlock:(FailureBlock)failureBlock;

- (void)getTestManifests: (void (^)(id))successBlock
       failureBlock:(FailureBlock)failureBlock;

- (void)checkVersionUpdate: (void (^)(id))successBlock failureBlock:(FailureBlock)failureBlock;

/**
 Fetches news detail data from the server.
 @param feed Feed object.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getNewsDetailWithFeed:(CBFeedModel *)feed
                 successBlock:(void (^)(CBNewsDetailModel *newsDetailModel))successBlock
                 failureBlock:(FailureBlock)failureBlock;


/**
 Fetches comments of a news detail from the server.
 @param feed Feed object.
 @param successBlock The block that will be executed when/if the request successfully finishes. Objects in NSArray of successBlock are of class CBCommentModel.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getCommentsWithFeed:(CBFeedModel *)feed
               successBlock:(void (^)(NSArray *comments))successBlock
               failureBlock:(FailureBlock)failureBlock;


/**
 Fetches gallery detail for the given feedId from the server.
 @param feedId Feed ID.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getGalleryDetailWithFeed:(CBFeedModel *)feed
                    successBlock:(void (^)(CBGalleryDetailModel *galleryDetailModel))successBlock
                    failureBlock:(FailureBlock)failureBlock;

/**
 Fetches categories from the server.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getCategoriesWithSuccessBlock:(void (^)(NSArray *categories))successBlock
                         failureBlock:(FailureBlock)failureBlock;

/**
 Fetches feeds of the category with the given categoryId from the server.
 @param category Category of the feed to be fetched.
 @param page Indicates the page of the feeds to be fetched.
 @param successBlock The block that will be executed when/if the request successfully finishes. Objects in NSArray of successBlock are of class CBFeedModel.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)getCategoryFeedWithCategory:(CBCategoryModel *)category
                               page:(NSInteger)page
                       successBlock:(void (^)(NSArray *feeds))successBlock
                       failureBlock:(FailureBlock)failureBlock;

/**
 Sends the comment for given given news detail id
 @param comment Comment of the news to be sent.
 @param feed    Feed object.
 @param successBlock The block that will be executed when/if the request successfully finishes.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)sendCommentWithCommentModel:(CBCommentModel *)comment
                               feed:(CBFeedModel *)feed
                       successBlock:(void (^)(id))successBlock
                       failureBlock:(FailureBlock)failureBlock;

/**
 Fetches feeds by searching the given search string in the server.
 @param searchString String to be searched.
 @param page Indicates the page of the feeds to be fetched.
 @param successBlock The block that will be executed when/if the request successfully finishes. Objects in NSArray of successBlock are of class CBFeedModel.
 @param failureBlock The block that will be executed when/if the request fails.
 @return void
 */
- (void)searchFeedsWithString:(NSString *)searchString
                         page:(NSInteger)page
                 successBlock:(void (^)(NSArray *feeds))successBlock
                 failureBlock:(FailureBlock)failureBlock;

/**
 Fetches the parallax ad info and the corresponding images returned in the request.
 @param successBlock Block to be executed in success case.
 @param failureBlock Block to be executed in failure case.
 */
- (void)getParallaxAdInfoWithSuccessBlock:(void (^)(CBParallaxAdModel *parallaxAd))successBlock
                             failureBlock:(FailureBlock)failureBlock;


#pragma mark - Network handlers

/**
 Generic handler which is used in all the handlers. Should not be modified in the subclasses, but can be used in subclasses.
 @param operation AFHTTPRequestOperation.
 @param responseObject Response dictionary.
 @param classToMap Class to be mapped with mantle.
 @param isArray If the responseObject is expected to be an array it should be set YES.
 @param error Error pointer.
 @param defaulErrorMessage If the error has a code that is not handled, an NSError object with this message is created. Leave nil for not using it.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleWithOperation:(AFHTTPRequestOperation *)operation
             responseObject:(id)responseObject
                 classToMap:(Class)classToMap
                    isArray:(BOOL)isArray
                      error:(NSError *)error
        defaultErrorMessage:(NSString *)defaultErrorMessage
               successBlock:(void (^)(id))successBlock
               failureBlock:(FailureBlock)failureBlock;

- (void)handleWithOperation:(AFHTTPRequestOperation *)operation
             responseObject:(id)responseObject
                 classToMap:(Class)classToMap
                    isArray:(BOOL)isArray
                      error:(NSError *)error
               successBlock:(void (^)(id))successBlock
               failureBlock:(FailureBlock)failureBlock __attribute((deprecated(("Use the method handleWithOperation:responseObject:classToMap:isArray:error:defaultErrorMessage:successBlock:failureBlock instead."))));

/**
 Handler for the get request. This method can be overridden in the subclasses for customized handling, or can be used for custom getNewsDetailById network call.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleGetWithOperation:(AFHTTPRequestOperation *)operation
                responseObject:(id)responseObject
                         error:(NSError *)error
                  successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                  failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


/**
 Handler for the getVideoDetailById request. It includes mantle conversion code and default error handling. This method can be overridden in the subclasses for customized handling, or can be used for custom getVideoDetailById network call.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param classToMap Class to be mapped with mantle
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleGetNewsDetailWithOperation:(AFHTTPRequestOperation *)operation
                          responseObject:(id)responseObject
                              classToMap:(Class)classToMap
                                   error:(NSError *)error
                            successBlock:(void (^)(CBNewsDetailModel *))successBlock
                            failureBlock:(FailureBlock)failureBlock;

/**
 Handler for the getCommentsByDetailId request. It includes mantle conversion code and default error handling. This method can be overridden in the subclasses for customized handling, or can be used for custom getCommentsByDetailId network call.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param classToMap Class to be mapped with mantle
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleGetCommentsWithOperation:(AFHTTPRequestOperation *)operation
                        responseObject:(id)responseObject
                            classToMap:(Class)classToMap
                                 error:(NSError *)error
                          successBlock:(void (^)(NSArray *))successBlock
                          failureBlock:(FailureBlock)failureBlock;

/**
 Handler for the getGalleryDetailById request. It includes mantle conversion code and default error handling. This method can be overridden in the subclasses for customized handling, or can be used for custom getGalleryDetailById network call.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param classToMap Class to be mapped with mantle
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleGetGalleryDetailWithOperation:(AFHTTPRequestOperation *)operation
                             responseObject:(id)responseObject
                                 classToMap:(Class)classToMap
                                      error:(NSError *)error
                               successBlock:(void (^)(CBGalleryDetailModel *))successBlock
                               failureBlock:(FailureBlock)failureBlock;

/**
 Handler for the getCategories request. It includes mantle conversion code and default error handling. This method can be overridden in the subclasses for customized handling, or can be used for custom getCategories network call.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param classToMap Class to be mapped with mantle
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleGetCategoriesWithOperation:(AFHTTPRequestOperation *)operation
                          responseObject:(id)responseObject
                              classToMap:(Class)classToMap
                                   error:(NSError *)error
                            successBlock:(void (^)(NSArray *))successBlock
                            failureBlock:(FailureBlock)failureBlock;

/**
 Handler for the getCategoryFeedWithCategoryId request. It includes mantle conversion code and default error handling. This method can be overridden in the subclasses for customized handling, or can be used for custom getCategoryFeedWithCategoryId network call.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param classToMap Class to be mapped with mantle
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleGetCategoryFeedWithOperation:(AFHTTPRequestOperation *)operation
                            responseObject:(id)responseObject
                                classToMap:(Class)classToMap
                                     error:(NSError *)error
                              successBlock:(void (^)(NSArray *))successBlock
                              failureBlock:(FailureBlock)failureBlock;

/**
 Handler for the sendCommentWithCommentModel request.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleSendCommentWithOperation:(AFHTTPRequestOperation *)operation
                        responseObject:(id)responseObject
                                 error:(NSError *)error
                          successBlock:(void (^)(id))successBlock
                          failureBlock:(FailureBlock)failureBlock;

/**
 Handler for the searchFeedsWithString request. It includes mantle conversion code and default error handling. This method can be overridden in the subclasses for customized handling, or can be used for custom getCategoryFeedWithCategoryId network call.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param classToMap Class to be mapped with mantle
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleSearchFeedsWithOperation:(AFHTTPRequestOperation *)operation
                        responseObject:(id)responseObject
                            classToMap:(Class)classToMap
                                 error:(NSError *)error
                          successBlock:(void (^)(NSArray *))successBlock
                          failureBlock:(FailureBlock)failureBlock;


/**
 Handler for getParallaxAdInfoRequest. It includes mantle conversion code and default error handling. Also it fetches the images for the ads.
 @param operation AFHTTPRequestOperation
 @param responseObject Response dictionary.
 @param classToMap Class to be mapped with mantle
 @param error Error pointer.
 @param successBlock The success handler used by the network call.
 @param failureBlock The error handler used by the network call.
 */
- (void)handleGetParallaxAdInfoWithOperation:(AFHTTPRequestOperation *)operation
                              responseObject:(id)responseObject
                                  classToMap:(Class)classToMap
                                       error:(NSError *)error
                                successBlock:(void (^)(CBParallaxAdModel *parallaxAd))successBlock
                                failureBlock:(FailureBlock)failureBlock;

/**
 It is called by handleGetParallaxAdInfoWithOperation. It calls loadParallaxImagesForParallaxAd to load the images of the ads. This method can be subclassed to do additional customization before loading the ads.
 */
- (void)handleParallaxAdInfoLoad:(CBParallaxAdModel *)parallaxAd completion:(void (^)(CBParallaxAdModel *parallaxAd))completion;

/**
 Loads the images to the model.
 @param parallaxAd The Parallax ad model.
 @param completion The block to be executed at the end.
 */
- (void)loadParallaxImagesForParallaxAd:(CBParallaxAdModel *)parallaxAd completion:(void (^)(CBParallaxAdModel *parallaxAd))completion;

/**
 *  Dispatch multiple GET requests asynchronously.
 *
 *  @param URLStrings URLs to make the requests.
 *  @param parameters Parameters array of the requests. Give nil if none of the requests need any parameters. Put [NSNull null] if you want to set nil for an auxiliary array element of parameters.
 *  @param failForAll Eeach BOOL as NSNumber represents seperate requests. Setting YES for a request means that when that request fails failure block of this method will be called. Set this array to nil if you want the success block to be called when all the requests succeed.
 *  @param success    Success block to be called. Each element is the response for the related request. If a request fails and success block is still called (according to failForAll parameter), that element of the response will be nil.
 *  @param failure    Failure block to be called.
 */
- (void)dispatchMultipleGET:(NSArray *)URLStrings
                 parameters:(NSArray *)parameters
                 failForAll:(NSArray *)failForAll
               successBlock:(void (^)(id responseObject))success
               failureBlock:(void (^)(NSError *error))failure;

#pragma mark - Helpers

/**
 Helper method that takes data (which should be a valid json array containing modelClass objects) and returns an array of modelClass objects.
 @param response Json data to be converted to array.
 @param modelClass Returning array contains instances of this class.
 @param error Error pointer used in json parsing.
 @return NSArray Contains modelClass objects.
 */
+ (NSArray *)getModelClassArrayFromResponse:(id)response modelClass:(Class)class error:(NSError **)error;

@end
