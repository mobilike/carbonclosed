//
//  CBGroupNewsCell.h
//  Carbon
//
//  Created by Fırat Karataş on 20/10/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import "CBFeedCell.h"

@class CBGroupNewsCell;

@protocol CBGroupNewsCellDataSource <NSObject>

/**
 @param model Group news model.
 @return cell class for group cell at index.
 */
- (Class<CBFeedCellProtocol>)groupNewsCellClassForModel:(id)model;

/**
 @param model Group news model.
 @return CGFloat Bottom space between the outside cell and inner cells.
 */
- (CGFloat)groupNewsCellBottomSpaceOfCellForModel:(id)model;

/**
 Returns the cell number for specific groupcell at index.
 @param model Group news model.
 @return cell number.
 */
- (NSInteger)groupNewsNumberOfItemsOfCellForModel:(id)model;

/**
 Method to configure each cell in group cell.
 @param cell cell to be configured.
 @param model Group news model.
 @param index cell index in group cell.
 */
- (void)configureCell:(UICollectionViewCell *)cell withModel:(id)model atIndex:(NSInteger)index;

/**
 Method to inform that cell is selected at index.
 @param model Group news model.
 @param index Inde of selected cell in group cell.
 */
- (void)groupCellWithModel:(id)model selectedAtIndex:(NSInteger)index;

/**
 Method to get distance between cells in group cell.
 @param model Group news model.
 @return distance between cells.
 */
- (CGFloat)distanceBetweenCellsForModel:(id)model;

/**
 Method to get header view.
 @param model Group news model.
 @return header view for group cell.
 */
- (UIView *)headerViewForGroupCellForModel:(id)model;

/**
 Method to inform that page index of group cell changed.
 @param model Group news model.
 @param pageIndex changed index.
 */
- (void)groupCellWithModel:(id)model changedPageToIndex:(NSInteger)pageIndex;

/******************/
@optional
- (Class<CBFeedCellProtocol>)groupNewsCellClassOfCellAtIndex:(NSInteger)groupCellIndex
__attribute((deprecated(("Use the method groupNewsCellClassForModel: instead."))));

- (CGFloat)groupNewsCellBottomSpaceOfCellAtIndex:(NSInteger)groupCellIndex
__attribute((deprecated(("Use the method groupNewsCellBottomSpaceOfCellForModel: instead."))));

- (NSInteger)numberOfItemsOfCellAtIndex:(NSInteger)groupCellIndex
__attribute((deprecated(("Use the method groupNewsNumberOfItemsOfCellForModel: instead."))));

- (void)configureCell:(UICollectionViewCell *)cell atGroupCell:(NSInteger)groupCellIndex atIndex:(NSInteger)index
__attribute((deprecated(("Use the method configureCell:withModel:atIndex: instead."))));

- (void)groupCell:(NSInteger)groupCellIndex selectedAtIndex:(NSInteger)index
__attribute((deprecated(("Use the method groupCellWithModel:selectedAtIndex: instead."))));

- (CGFloat)distanceBetweenCellsForGroupCellIndex:(NSInteger)groupCellIndex
__attribute((deprecated(("Use the method distanceBetweenCellsForModel: instead."))));

- (UIView *)headerViewForGroupCellAtIndex:(NSInteger)groupCellIndex
__attribute((deprecated(("Use the method headerViewForGroupCellForModel: instead."))));

- (void)groupCellAtIndex:(NSInteger)groupCellIndex changedPageToIndex:(NSInteger)pageIndex
__attribute((deprecated(("Use the method groupCellWithModel:changedPageToIndex: instead."))));

@end

@interface CBGroupNewsCell : CBFeedCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentScrollViewRightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentScrollViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentScrollViewBottomConstraint;
@property (weak, nonatomic) id<CBGroupNewsCellDataSource> dataSource;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (nonatomic, strong) NSMutableDictionary *cellArrayDictionary;
@property (nonatomic, assign, readonly) NSInteger currentPage;

@property (nonatomic, strong) id model;

- (void)addCellsWithModel:(id)model fromStartingIndex:(NSInteger)startingIndex;



+ (CGSize)groupNewsCellSizeForCellSize:(CGSize)size;

/**
 Set a block to calculate height of the header views.
 @param headerViewHeightBlock A block to calculate the header view heights for a given group cell index. If you have a constant header view height then the collectionView:layout:sizeForItemAtIndexPath method will calculate the sizes of your CBGroupNewsCells correctly, but if you have different header view heights for different indexes then you need to implement collectionView:layout:sizeForItemAtIndexPath in your CBFeedViewController subclass to give the right sizes for your CBGroupNewsCells.
 */
+ (void)setHeaderViewHeightBlock:(CGFloat (^)(id model))block;


@property (nonatomic, assign) NSInteger groupCellIndex
__attribute((deprecated(("It is no longer used. Use the model property instead."))));

- (void)addCellsInIndex:(NSInteger)index fromStartingIndex:(NSInteger)startingIndex
__attribute((deprecated(("Use the method addCellsWithModel:fromStartingIndex: instead."))));

@end
