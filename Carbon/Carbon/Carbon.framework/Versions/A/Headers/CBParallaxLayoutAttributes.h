//
//  ParallaxLayoutAttributes.h
//  Carbon
//
//  Created by Necati Aydın on 13/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBParallaxLayoutAttributes : UICollectionViewLayoutAttributes

/**
 Calculated offset value for the layout attribute.
 */
@property (nonatomic) CGPoint parallaxOffset;

@end
