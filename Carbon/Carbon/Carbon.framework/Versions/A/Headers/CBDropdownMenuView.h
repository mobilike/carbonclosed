//
//  CBDropdownMenuView.h
//  Carbon
//
//  Created by Fırat Karataş on 25/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CBDropdownViewBackgroundStyle)
{
    CBDropdownViewBackgroundStyleNone = 0,
    CBDropdownViewBackgroundStyleImage = 1, // blurry image
    CBDropdownViewBackgroundStyleView = 2, // a view with alpha
};

#pragma mark - Delegate Protocol

@class CBDropdownMenuView, CBDropdownMenuTableViewCell, CBDropdownMenuNavigationBarView;

@protocol CBDropdownMenuViewDelegate <NSObject>

@required

/**
 Delegate method to get title for the given index path.
 @param dropdownMenuView DropdownMenuView view that is selected.
 @param indexPath Index path of the title.
 @return Title of the dropdown cell.
 */
- (NSString *)dropdownMenuView:(CBDropdownMenuView *)dropdownMenuView titleAtIndexPath:(NSIndexPath *)indexPath;

/**
 Data source method to get number of rows.
 @return number of rows.
 */
- (NSInteger)numberOfRowsAtDropdownMenuView:(CBDropdownMenuView *)dropdownMenuView;

@optional

/**
 The navigation bar view which works with this view.
 */
- (CBDropdownMenuNavigationBarView *)navigationBarViewForDropdownMenuView:(CBDropdownMenuView *)dropdownMenuView;

/**
 Informs delegate that category is selected.
 @param dropdownMenuView DropdownMenuView view that is selected.
 @param indexPath index path selected.
 */
- (void)dropdownMenuView:(CBDropdownMenuView *)dropdownMenuView didSelectAtIndexPath:(NSIndexPath *)indexPath;

/**
 Informs the delegate that the view is about to be presented.
 */
- (void)dropdownMenuViewWillPresent:(CBDropdownMenuView *)dropdownMenuView;

/**
 Informs the delegate that the view is completely presented.
 */
- (void)dropdownMenuViewDidPresent:(CBDropdownMenuView *)dropdownMenuView;

/**
 Informs the delegate that the view is about to be dismissed.
 */
- (void)dropdownMenuViewWillDismiss:(CBDropdownMenuView *)dropdownMenuView;

/**
 Informs the delegate that the view is completely dismissed.
 */
- (void)dropdownMenuViewDidDismiss:(CBDropdownMenuView *)dropdownMenuView;

@end

#pragma mark - Class


@interface CBDropdownMenuView : UIView <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
/**
 ImageView is created and set outside of the dropdown view. To get a blurred background below CBDropdownMenuView and to get its opening and closing animations right, this should be added to the superview of CBDropdownMenuView.
 */
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;

@property (nonatomic, weak) id<CBDropdownMenuViewDelegate> delegate;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, readonly) BOOL isShown;
@property (nonatomic, strong) UIColor *separatorColor;
@property (nonatomic, assign) CBDropdownViewBackgroundStyle backgroundStyle;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

- (void)styleCell:(CBDropdownMenuTableViewCell *)cell;
- (void)present;
- (void)presentWithBackgroundImage:(UIImage *)backgroundImage;
- (void)dismiss;

@end
