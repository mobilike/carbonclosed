//
//  CBUrlSchemeManager.h
//  Carbon
//
//  Created by Fırat Karataş on 06/08/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBUrlSchemeManager : NSObject

/**
 Class method to check if request is eligible for deep linking by checking url schemes that are fetched from info.plist.
 @return result of checking
 @param request Request that will be checked
 */
+ (BOOL)checkSchemesWithURLRequest:(NSURLRequest *)request;

@end
