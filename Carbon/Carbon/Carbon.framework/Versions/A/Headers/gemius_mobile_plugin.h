#ifndef _GEMIUS_MOBILE_PLUGIN_H_
#define _GEMIUS_MOBILE_PLUGIN_H_


/* gemius_mobile_plugin_init - module initialization routine
 *
 * The best practice is to call it at the application launch and activation, usually in the following methods:
 * application:didFinishLaunchingWithOptions: or applicationDidFinishLaunching: 
 * and 
 * applicationWillEnterForeground:
 *
 * Function parameters:
 * serverprefix - DNS prefix of gemius server assigned to your account
 * cookiefilename - optional path to the file where your application will store gemius-http-cookies, use NULL for default path
 *
 * Function returns values:
 *     0 - success
 *    -1 - error occured
 */


int gemius_mobile_plugin_init(const char *serverprefix,const char *cookiefilename);



/* gemius_mobile_plugin_hit - main routine which makes http request to gemius server. 
 * Call it on every interesting event you want to track, eg application launch and activation.
 *
 * Function parameters:
 * identifier - gemius script identifier
 * extraparameters - NULL or optional string with extra parameters in the form: "key1=value1|key2=value2|(...)"
 * pvtype:
 *     GEMIUS_PVTYPE_FULL - every static component of view was reloaded
 *     GEMIUS_PVTYPE_PARTIAL - when some static components of view were not reloaded
 *     GEMIUS_PVTYPE_DEFAULT - no additional data about pageview type will be sent
 * 
 * Function returns values:
 *     0 - success
 *    -1 - error occured
 */

typedef enum {
	GEMIUS_PVTYPE_DEFAULT = 0,
	GEMIUS_PVTYPE_FULL,
	GEMIUS_PVTYPE_PARTIAL} pageview_t;

int gemius_mobile_plugin_hit(const char *identifier,const char *extraparameters, pageview_t pvtype);



/* gemius_mobile_plugin_free - free memory allocated by gemius_mobile_plugin_init routine and finish pending tasks. 
 * Call it on exit and deactivation of application, usually in the following methods:
 * dealloc
 * and
 * applicationDidEnterBackground: 
 * and
 * applicationWillTerminate:
 */

void gemius_mobile_plugin_free(void);


#endif
