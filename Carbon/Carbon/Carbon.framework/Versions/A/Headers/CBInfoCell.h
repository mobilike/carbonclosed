//
//  CBInfoLabelCell.h
//  Carbon
//
//  Created by Semih Cihan on 10/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseTableViewCell.h"

static const CGFloat kVerticalWebViewMargin = 10.f;

@interface CBInfoCell : CBBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) NSString *textColor;

@end
