//
//  CBDFPKeywords
//  Pods
//
//  Created by Tolga Caner on 23/05/16.
//
//

#import <Foundation/Foundation.h>

/*!
 @class CBDFPKeywords
 @description Class containing the DFP keywords to be used within a request.
 */
@interface CBDFPKeywords : NSObject

/*!
 @return NSDictionary containing the keywords as key-value pairs. Note that this class needs to be overridden as DFPKeywords in your implementation.
 */
- (NSDictionary*)keywords;

@end
