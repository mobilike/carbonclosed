//
//  CBFeedAd.h
//  Pods
//
//  Created by Tolga Caner on 23/03/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UICollectionViewCell.h>
@import GoogleMobileAds;
/*!
 @typedef AdType
 
 @brief  An enum containing possible ad types.
 
 @discussion
 Some of the values are not used in the recent versions of the framework.
 
 @field AdTypeBanner 320x50 banner ads
 @field AdTypeRichMedia Rich media ads, (unused)
 @field AdTypeCustomSize Custom sized ads, scaled according to the screen size
 @field AdTypeNative Native ads (unused)
 @field AdTypeLeaderboard Banner ads for iPad
 @field AdTypeMediumRectangle MRE (300x250) ads.
 @field AdTypeMAIA (unused)
 @field AdTypeInterstitial 320x480, full screen ads
 @field AdTypeInterstitialLandscape 480x320, full screen ads (unused)
 @field AdTypeUnknown Unkown type of ad, stops the ad request if met.
 */
typedef enum {
    AdTypeBanner,
    AdTypeRichMedia,
    AdTypeCustomSize,
    AdTypeNative,
    AdTypeLeaderboard,
    AdTypeMediumRectangle,
    AdTypeMAIA,
    AdTypeInterstitial,
    AdTypeInterstitialLandscape,
    AdTypeUnknown
}AdType;

/*!
 @class CBFeedAd
 @discussion The ad object containing properties for a single ad in a feed managed by the CBCollectionViewAdManager.
 */
@interface CBFeedAd : NSObject

/*!
 @brief The ad should be shown here in the collection view it's in.
 */
@property (nonatomic, readonly, strong) NSIndexPath * indexPath;
/*!
 @brief If its a recurring ad in the collection view (e.g. shown every n cells), the index where its sequence first starts.
 */
@property (nonatomic) NSInteger startIndex;
/*!
 @brief If its a recurring ad in the collection view (e.g. shown every n cells), the frequency of the ad (the number n in the example before)
 */
@property (nonatomic) NSInteger frequency;
/*!
 @brief The type of the ad
 */
@property (nonatomic, readonly) AdType adType;
/*!
 @brief The size of the ad
 */
@property (nonatomic, readonly) CGSize adSize;
/*!
 @brief The view of the ad which handles the requesting and displaying
 */
@property (nonatomic, strong) DFPBannerView* adView;
/*!
 @brief The position (row) in which the ad should be displayed.
 */
@property (nonatomic,assign) int position;
/*!
 @brief Depicts whether the ad's view has loaded the ad.
 */
@property (assign) BOOL adViewLoaded;
/*!
 @brief Depicts whether the ad should be shown in the feed. Should be NO if it has not loaded yet.
 */
@property (assign) BOOL showAd;

/*!
 @description Initializes and returns an instace of CBFeedAd given the dictionary. The dictionary should contain 2 key-value pairs as "type" and "size"
 */
-(id)initWithDictionary:(NSDictionary*)dictionary;

@end
