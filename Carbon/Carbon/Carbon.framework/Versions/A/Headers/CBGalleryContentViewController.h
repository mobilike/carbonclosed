//
//  CBGalleryContentViewController.h
//  Carbon
//
//  Created by Necati Aydın on 09/02/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBImageViewController.h"

/**
 Image view controller subclass with a text view, which has webview in it.
 */
@interface CBGalleryContentViewController : CBImageViewController


/**
 Hides or shows photo description scroll view. It is initially hidden.
 */
- (void)enablePhotoDescriptionView:(BOOL)enable;

/**
 Sets text view alpha.
 */
- (void)hidePhotoDescriptionView:(BOOL)hide;

/**
 Photo description label.
 */
@property (nonatomic, weak) IBOutlet UILabel *photoDescriptionLabel;

@end
