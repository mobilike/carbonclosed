//
//  CBFeed2Cell.h
//  Carbon
//
//  Created by Necati Aydın on 30/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBBaseCollectionViewCell.h"
#import "CBFeedCell.h"

/**
 Feed cell type 2.
 */
@interface CBFeed2Cell : CBFeedCell



@end
