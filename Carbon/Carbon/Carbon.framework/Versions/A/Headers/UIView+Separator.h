//
//  UIView+Separator.h
//  Carbon
//
//  Created by Necati Aydın on 12/01/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UIView(Separator)

/**
 @description Creates four separator views to make a frame around the view with the given line width and adds them to the top of the view as subviews.
 @param lineWidth Thickness of the separators.
 @return NSArray containing four separator views (top, left, bottom, right respectively) created by this method.
 */
- (NSArray *)addFrameWithLineWidth:(CGFloat)lineWidth;

/**
 @description Creates a separator view and adds it to the top of the view as a subview.
 @param lineWidth Thickness of the separators.
 @return The separator view created by this method.
 */
- (UIView *)addSeparatorAtTopWithLineWidth:(CGFloat)lineWidth;

/**
 @description Creates a separator view and adds it to the bottom of the view as a subview.
 @param lineWidth Thickness of the separators.
 @return The separator view created by this method.
 */
- (UIView *)addSeparatorAtBottomWithLineWidth:(CGFloat)lineWidth;

/**
 @description Creates a separator view and adds it to the left of the view as a subview.
 @param lineWidth Thickness of the separators.
 @return The separator view created by this method.
 */
- (UIView *)addSeparatorAtLeftWithLineWidth:(CGFloat)lineWidth;

/**
 @description Creates a separator view and adds it to the right of the view as a subview.
 @param lineWidth Thickness of the separators.
 @return The separator view created by this method.
 */
- (UIView *)addSeparatorAtRightWithLineWidth:(CGFloat)lineWidth;

@end
