//
//  CBSettingsViewControllerTableViewController.h
//  Carbon
//
//  Created by Semih Cihan on 09/03/15.
//  Copyright (c) 2015 mobilike. All rights reserved.
//

#import "CBTableViewController.h"

@class CBSettingsLogic;

@interface CBSettingsViewController : CBTableViewController

@end
