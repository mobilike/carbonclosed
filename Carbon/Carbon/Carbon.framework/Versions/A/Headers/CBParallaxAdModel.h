//
//  ParallaxAdModel.h
//  Milliyet
//
//  Created by Necati Aydın on 08/12/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#import "CBBaseModel.h"

@interface CBParallaxAdModel : CBBaseModel

@property (nonatomic, strong) NSURL *adImageUrl;
@property (nonatomic, strong) NSURL *adFrameImageUrl;
@property (nonatomic, assign) NSInteger adIndex;
@property (nonatomic, strong) NSURL *adUrl;

@property (nonatomic, assign) CGFloat frameRatio;

@property (nonatomic, strong) UIImage *adImage;
@property (nonatomic, strong) UIImage *adFrameImage;

- (CGFloat)adImageRatio;

@end
