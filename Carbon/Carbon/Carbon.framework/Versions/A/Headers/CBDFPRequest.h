//
//  CBDFPRequest.h
//  Pods
//
//  Created by Tolga Caner on 20/04/16.
//
//

#import <GoogleMobileAds/GoogleMobileAds.h>

/*!
 @description A wrapper around GoogleMobileAds' DFPRequest class, adds capability to initialize DFPRequest objects with custom targeting options.
 @discussion Do not try to re-extend with the name DFPRequest and expect it to work in your implementation as conventional as it sounds in Carbon framework, this has not been developer tested.
 */
@interface CBDFPRequest : DFPRequest
/**
 @brief Returns the ad udid. Generates one if there's no udid at the time.
 */
+ (NSString*) advertisingIdentifier;
/**
 Returns A CBDFPRequest object with generic content url set as depicted in CBAppConfig
 */
+ (instancetype) requestWithContentURL:(NSString*)contentURL;

@end
