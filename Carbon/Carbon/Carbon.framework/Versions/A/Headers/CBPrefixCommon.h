//
//  CBPrefixCommon.h
//  Carbon
//
//  Created by Necati Aydın on 19/11/15.
//  Copyright © 2015 mobilike. All rights reserved.
//

#ifndef CBPrefixCommon_h
#define CBPrefixCommon_h

#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#ifdef DEBUG
#define DebugLog(...) NSLog(__VA_ARGS__)
#else
#define DebugLog( s, ... )
#endif

#endif /* CBrefixCommon_h */
