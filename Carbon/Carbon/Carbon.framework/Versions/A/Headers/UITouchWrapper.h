//
//  UITouchWrapper.h
//  Pods
//
//  Created by Tolga Caner on 31/03/16.
//
//

#import "UIKit/UIKit.h"

@interface UITouchWrapper : NSObject

- (instancetype)initWithTouch:(UITouch*)touch point:(CGPoint)point;

@property (nonatomic,strong) UITouch* touch;
@property (nonatomic) CGPoint point;
@property (nonatomic) CGFloat firstDistance;

@end
