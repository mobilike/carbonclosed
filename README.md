How to Build an App With Carbon
===================

Carbon is a splendid framework to create apps for iOS that you can serve contents to users. News apps form the usage area majority of the Carbon. It is intended to build apps as fast as possible by providing the basis of the app, all you need to do is configuring the Carbon conforming to your desires! 

Carbon is already a working app, however, you have to connect it to your API and configure mostly anything you want to make it yours. 

----------


How to Get Started
-------------

- Open XCode and **"Create a new Xcode Project"**.
- Under iOS / Application, create **Single View Application**.
- Go to your project directory and create **Podfile** 

```
$ pod init
$ open -a Xcode Podfile
```
and add this dependency to you **Podfile**:

> pod 'Carbon', :git => 'git@bitbucket.org:mobilike/carbonclosed.git' , :tag => '1.0.1'

- Then  run **pod install**.
- Open the **.xcworkspace** file which will be created automatically in your directory.
- When the workspace is opened, you will see the Carbon and other helper frameworks under the Pods folder.
-  Delete following files under your project: **AppDelegate.h** , **AppDelegate.m** , **ViewController.h** , **ViewController.m** , **Main.storyboard** . 
- Go to your project, under the tab **General**, clear the **Main Interface** field.
- Go to your **main.m** file and edit the code: 
```Objective-C
#import <UIKit/UIKit.h>
#import <Carbon/CBAppDelegate.h>

int main(int argc, char * argv[]) {
@autoreleasepool {
return UIApplicationMain(argc, argv, nil, NSStringFromClass([CBAppDelegate class]));
}
}
```
- Open your **Info.plist** file and add **NSAppTransportSecurity** property as a dictionary and add **NSAllowsArbitraryLoads** as the item of NSAppTransportSecurity property, then set it to **YES**.

----------
General Notes
----------------

- To configure any part of the project, classes have to be subclassed. For example to change the cells of the feed you need to subclass **CBFeedViewController** which contains feed related codes. 
- Subclassing works like this: remove the "**CB**" from the name of the original class and give it to the child class like **FeedViewController**. Now **FeedViewController** is used. Carbon overrides the **alloc** methods to make compiler allocate CB'less classes instead of original ones if they exist. You can change/override anything you want in your **FeedViewController**. 
- All configuration properties are stored in **CBAppConfig** class so you can subclass it as **AppConfig** and change the desired default configs by overriding **init** method and changing it after calling **[super init]**. You can check which configs can be updated from he **CBAppConfig.h** file.

----------

Network Configurations
-----------

- As stated above, to make changes in specific part of the class, It must be subclassed. To change network configurations, you must subclass **CBNetworkManager** and name it **NetworkManager**.
- Before diving into the **NetworkManager**, you need to set the urls (base url, categories url etc.) of the service in **init** method of the **AppConfig** as stated above. For example:
```Objective-C
self.networkBaseUrl = @"http://shiftdelete.net/api/";
self.networkNewsDetailUrl = @"articleDetail?id=%@";
self.networkVideoDetailUrl = @"videoDetail?id=%@";
self.networkCommentsUrl = @"comments?id=%@";
self.networkCategoriesUrl = @"mobileMenu";
self.networkCategoryFeedUrl = @"feed?cat=%@&p=%d";
self.networkFeedSearchUrl = @"search?q=%@&p=%ld";
self.networkGalleryDetailUrl = @"photoDetail?id=%@";
self.networkWriteCommentUrl = @"http://shiftdelete.net/ajax/comment/add";
```
- After that model objects must be mapped. All model object properties have prefix according to their model type. For example; category model properties have **categoryModel** prefix like **categoryModelCategoryId** or **categoryModelName**. Default values don't have prefixes. For **categoryModelCategoryId**, value is **id**. Assume your category model id is key is **category_id** in service so you need to assign **categoryModelCategoryId** like: 
``` Objective-C
self.categoryModelCategoryId = @"category_id";
```
- When you set all model mappings and network end-points, you are ready to go if you don't need any network configurations. If you do, you need to do it in the **NetworkManager**. As you can see in the **CBNetworkManager.h** there are two methods for every service call.
- Network Manager has several features to fetch data and each feature has 2 methods one is to fetch data and the other one is to handle fetched data. First methods have prefix **get** second methods have prefix **handle**.

Getting Categories
-------------
----------
As mentioned above, **CBNetworkManager** has 2 methods for getting categories: 
``` Objective-C
- (void)getCategoriesWithSuccessBlock:(void (^)(NSArray *categories))successBlock failureBlock:(FailureBlock)failureBlock;
```
and
``` Objective-C
- (void)handleGetCategoriesWithOperation:(AFHTTPRequestOperation *)operation
responseObject:(id)responseObject
classToMap:(Class)classToMap
error:(NSError *)error
successBlock:(void (^)(NSArray *))successBlock
failureBlock:(FailureBlock)failureBlock;
```
First method gets success block has type of NSArray and failure block. If you need something to modify before service call do it here by overriding and call super after. Besides, you can manage your own call here and then call handle method after manually. **categories** array contains **CBcategoryModel** objects by default.

Handle method is to map response object to actual model objects. **classToMap** is the class of the actual model object. It is **CBCategoryModel** by default. If you overridden   the **CBCategoryModel** and created a **CategoryModel** you should give this when you call super method. Example use case of the handle method can be removing duplicates from response object. 

Example:
Assume you want to fetch menu icons before you fetch the menu icons and you created a method called **fetchMenuIcons**. So you may call the **fetchMenuIcons** method in the **getCategory** method before calling the super method:
``` objective-c
- (void)getCategoriesWithSuccessBlock:(void (^)(NSArray *categories))successBlock failureBlock:(FailureBlock)failureBlock {

[[NetworkManager sharedInstance]        fetchMenuIconsWithSuccessBlock:^(NSArray *menuIcons)
{
[NetworkManager sharedInstance].menuIcons = menuIcons;

} failureBlock:nil];

[super getCategoriesWithSuccessBlock:successBlock failureBlock:failureBlock];
}
```
Now assume that response of the call contains some duplicate data. You may remove these duplicate items in the handle method for fetching categories because this where you have the response object.

``` objective-c
- (void)handleGetCategoriesWithOperation:(AFHTTPRequestOperation *)operation
responseObject:(id)responseObject
classToMap:(Class)classToMap
error:(NSError *)error
successBlock:(void (^)(NSArray *))successBlock
failureBlock:(FailureBlock)failureBlock {

if ([response isKindOfClass:[NSArray class]]) {

// filter out duplicate category objects
NSArray *responseArray = response;
NSMutableDictionary *idDictionary = [@{} mutableCopy];

NSMutableArray *filteredResponse = [@[] mutableCopy];
for (NSInteger i = 0; i < responseArray.count; i++) {

NSString *categoryName = ((CBCategoryModel *)responseArray[i]).name;
if (idDictionary[categoryName] == nil) {
idDictionary[categoryName] = @YES;
[filteredResponse addObject:responseArray[i]];
}
}

[super handleGetCategoriesWithOperation:operation responseObject:filteredResponse error:error successBlock:successBlock failureBlock:failureBlock];

} else {

[super handleGetCategoriesWithOperation:operation responseObject:nil error:error successBlock:successBlock failureBlock:failureBlock];
}
}
```                          
- There is a generic method that is called in all handler methods. You can directly call this method instead of calling super method. The method is: 
``` objective-c
- (void)handleWithOperation:(AFHTTPRequestOperation *)operation
responseObject:(id)responseObject
classToMap:(Class)classToMap
isArray:(BOOL)isArray
error:(NSError *)error
defaultErrorMessage:(NSString *)defaultErrorMessage
successBlock:(void (^)(id))successBlock
failureBlock:(FailureBlock)failureBlock;
```

View Controller - Logic - Network Manager Architecture
-------------

For handling the data in application level and organizing the communication between CBBaseViewController subclasses and CBNetworkManager, an extra layer of CBBaseLogic subclasses are created. That means for every view controller there exists a logic object. That is ensured in the CBBaseViewController and CBBaseLogic. The logic objects main responsibilities are:

- Keeping the data.
- Making network requests.
- Handling all data related logical operations such as validations.
- Passing the data to view controllers after it is done with it.

So the basic principles while implementing view controllers and logics are:

- The view controllers should never interact with the network manager directly. It should interact with logic instead.
- The view controllers should never hold the data.
- The view controllers should not import CBNetworkManager or its subclass.
- The logic subclasses should never do anything UI related.



Interaction between View Controller - Logic - Network Manager
-------------------------------------------------------------

To provide the communication between the network manager and a logic class, **blocks** are used. There should be success and failure blocks on the network manager methods. On the contrary, to handle the interaction between view controllers and logics, **delegation** is used. The most generic protocols to handle this delegation is **CBDataLoaderProtocol** and **CBDataLoaderDelegate**:
``` objective-c
@protocol CBDataLoaderProtocol <NSObject>

- (void)loadData;

@end


@protocol CBDataLoaderDelegate <NSObject>

- (void)dataLoading;
- (void)dataLoaded;
- (void)dataLoadedWithError:(NSString *)errorMessage;

@end

``` 

View controllers can conform the delegate protocol and the logics can conform to the other one for data loading. Also this protocols can be subclassed if there are additional requests or requirements. For example CBFeedViewController protocols are subclassing both protocols:

``` objective-c
@protocol CBDataLoaderWithPagingAndRefreshProtocol <CBDataLoaderProtocol>

- (void)refreshData;
- (BOOL)loadNextPage;
- 
@property (assign, nonatomic, readonly) BOOL hasMorePageToLoad;
@property (assign, nonatomic, readonly) BOOL isLoading;

@end


@protocol CBDataLoaderWithPagingAndRefreshDelegate <CBDataLoaderDelegate>

- (void)dataLoadedForRange:(NSRange)range;
- (void)dataLoadingWithPaging;

@end

```

Configure and Style
-----------------
In carbon, configuring means that mapping the model object to UI such as setting the text of UILabel or mapping the values of a cell. Styling is to decide how the view appears such as deciding the background colour of a UIView or font of a UILabel. In view controllers, there are related methods based on this abstraction and they can be subclassed or overridden as needed.

There are 2 places that you can look for  the styling and configuration methods. 1) View Controllers. 2) CBCollectionViewCellStyler and CBCollectionViewCellModelMapper. So as you understand from the name, for collection view cells, we have separate classes. 

For example, In the CBNewsDetailViewController the styling and configuration methods are:

``` objective-c
- (void)styleNavigationItems;
- (void)styleByIncrementingFontBy:(CGFloat)fontIncrementAmount;
- (NSString *)styleContent:(NSString *)content withCSSForMaximumWidth:(CGFloat)width fontIncrementAmount:(CGFloat)fontIncrementAmount;
- (void)configure;
- (void)configureWithModel;
```

In the CBCollectionViewCellModelMapper we have the following methods.

``` objective-c
@interface CBCollectionViewCellModelMapper : NSObject

+ (instancetype)sharedInstance;

- (void)configureCell:(CBBaseCollectionViewCell *)cell withModel:(CBBaseModel *)model;

- (void)configureFeedCell:(CBFeedCell *)feedCell withFeedModel:(CBFeedModel *)feed;
- (void)configureParallaxCell:(CBParallaxCell *)parallaxCell withParallaxModel:(CBParallaxAdModel *)adModel;
- (void)configureCommentCell:(CBCommentCell *)cell withCommentModel:(CBCommentModel *)model;

@end
```

It is advised that you use configureCell:withModel method if you need to configure a cell, not the other ones directly. In that method the cell class and model class is checked and the necessary method is called. However, if you need to customise configuration of a feedCell for example, you need to change that behaviour in the subclass. 

The usage of CBCollectionViewCellStyler is exactly the same. When you need to style a cell, call  styleCell:withModel: . If you need to customise, override other methods.

Custom Cells In Feed
--------------------

For customizing the feed cell, there are two methods: 

1 ) First one is to create cells that conforms to **CBFeedCellProtocol** and override the bottom method. By overriding this method, you can decide on which cell to choose based on model and indexPath. After that you need to specify the configuration and styling of the cell by again in the subclass as explained in "**Configure and Style**" chapter. Also, do not forget to register the cells' nib in the viewDidLoad method of your subclass view controller.

``` objective-c
- (Class<CBFeedCellProtocol>)cellClassForFeedModel:(CBFeedModel *)feedModel indexPath:(NSIndexPath *)indexPath;
```

2 ) The second method requires more effort. But the bright side of this method is, you do not have to use CBFeedModel instance for configuring and your cells do not have to conform to CBFeedCellProtocol. **In this method you need to override collection view delegate methods.**Also, do not forget to register the cells' nib in the viewDidLoad method of your subclass view controller. When you do those, you won't need to implement cellClassForFeedModel:indexPath method and configuration and styling methods should be handled by yourself. So basically you will be overriding whole cell structure so do not expect anything to automatically happen. 

Custom Cells In Other View Controllers
--------------------

There is no generic rule to use custom cells which apply all over carbon. In most of the view controllers, the method that creates the cell is a separate method in order you to subclass the cell. For example in CBNewsDetailViewController here are the methods that you can customise:

``` objective-c

- (CBFeed4Cell *)reusableSuggestedVideoCellForIndexPath:(NSIndexPath *)indexPath
{
CBFeed4Cell *feedCell = (CBFeed4Cell *)[self.view.suggestedVideosView.collectionView dequeueReusableCellWithReuseIdentifier:[CBFeed4Cell reuseIdentifier] forIndexPath:indexPath];

return feedCell;
}

- (CBCommentCell *)reusableCommentCellForIndexPath:(NSIndexPath *)indexPath
{
CBCommentCell *commentCell = (CBCommentCell *)[self.view.commentView.collectionView dequeueReusableCellWithReuseIdentifier:[CBCommentCell reuseIdentifier] forIndexPath:indexPath];

return commentCell;
}

```

So if you subclass that cells and give those in the methods,  you are fine. But if you do not want to subclass the cell and use a completely different cell you need to override all of the collection view delegate methods.

## View Controller Creation ##
CBViewControllerBuilder is one of the class that is subclassable. It's responsibility is to create and initialize view controllers. So every view controller creation with it's logic should be done in this class or it's subclass. In carbon, there is a simple guideline that we follow, when creating a view controller:

- Every view controller should have a separate storyboard.
- Every view controller should have a logic class.
- If logic exists before, it should be tied to the view controller when the view controller is created in view controller builder. If it does not exist, it should be created in the view controller builder by getting the necessary parameters (such as models or model ids). 
- If you subclassed a view controller but you still need a separate builder method, you should be implementing it in the ViewControllerBuilder class and benefit from the respective method in CBViewControllerBuilder. 
- While creating the view controllers and logics, do not forget to tie both of those object to each other. View controller's logic should be set and the logic's delegate should be set to each other.

## Asset Handling ##
If you want an asset to be used instead of the one that is provided by Carbon, you should name it as the same as the Carbon's asset and it will be used automatically. 

## Navigation Bar ##
First of all, there are some properties in CBAppConfig that are used for styling the navigation bar. Those are:
``` objective-c
@property (nonatomic, strong) UIColor *navigationBarBackgroundColor;
@property (nonatomic, strong) UIColor *navigationBarTintColor;
@property (nonatomic, strong) UIColor *navigationBarTitleColor;
@property (nonatomic, strong) UIFont *navigationBarTitleFont;
@property (nonatomic, strong) UIColor *navigationBarBarButtonItemColor;
@property (assign, nonatomic) BOOL navigationBarStatusBarHasLightContent;
``` 
Those names are self explanatory, there is no further explanation needed except for navigationBarStatusBarHasLightContent. When you use that, do not forget to set "**View controller-based status bar appearance**" to **NO** in **info.plist** file.

In Carbon, navigation bar styling is handled in a separate class called **CBNavigationBarStyler** and the properties above are used in this class for styling. This class is also subclassable, so if you want additional navigation bar styling, you should subclass or override this class. 

Also, there is a method called **customizeNavigationBar** in CBBaseViewController. It is called in viewDidLoad method of this class. So this method's main purpose to apply changes to navigation controller and provide a standard to subclasses. Therefore, it is preferred that you also put your navigation bar related changes in this method. But if you want something specific, for example to style navigation bar when view did appear, do not hesitate to style navigation bar in viewDidAppear method or somewhere else.

## How to Release a Build ##

1.In carbon repository (not carbonclosed) switch to the dev branch.

2.Open carbon.podspec in carbon's folder and upgrade "s.version".

3.Open "Carbon.h" and update the version.

4.After making sure the code compiles and runs successfully, commit to the dev branch and merge dev branch to the master branch.

5.Tag the version.

6.Build the project from selecting the framework target in CarbonApp workspace.

![framework.png](https://bitbucket.org/repo/G4GpB6/images/3407044226-framework.png)

7.From Xcode, File Inspector find Carbon.bundle, right click and select show in Finder. In finder, copy Carbon.bundle and Carbon.framework.

![ShowInFinder.png](https://bitbucket.org/repo/G4GpB6/images/2575372707-ShowInFinder.png)

8.Go to the carbonclosed folder. 

9.Replace Carbon.bundle and Carbon.framework with the ones you copied. Do not forget the replace **dependencies** if you replace them in carbon. For example, if you updated seamless framework, you have to update it in carbonclosed too. 

10.Open carbon.podspec and update the version same as in the step 2.

11.Commit and push to the master in carbonclosed repository.

12.Tag the version.

## Updating the Carbon Version of Apps ##
There are some basic rules that we follow while maintaining applications: 

1.When you are working you can work on carbon's dev branch. For example in your podfile:

pod 'Carbon', :path => '/Users/necati/Projects/carbon', :branch => 'dev'
Or, you can make commits and pushes which includes carbon on dev branch too:

pod 'Carbon', :git => 'git@bitbucket.org:mobilike/carbon.git', :branch => 'dev'

2.But if you are submitting the app, you should always release a carbon version and change podfile accordingly. That way, next time you open the application, you will know which version does that application was using.

pod 'Carbon', :git => 'git@bitbucket.org:mobilike/carbon.git', :tag => '1.5.3'

3.When you submit an app, do not forget to tag the application version in its' repository.