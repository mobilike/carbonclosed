Pod::Spec.new do |s|

s.name         = "Carbon"
s.version      = "2.0.18"
s.summary      = "A content based application framework for Mobilike iOS applications."

s.homepage     = "http://www.mobilike.com"
s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
s.author             = { "Semih Cihan" => "semih@mobilike.com" }
s.platform     = :ios
s.ios.deployment_target = "7.1"
s.source       = { :git => "https://bitbucket.org/mobilike/carbonclosed.git", :tag => s.version }

s.frameworks = "Security", "CoreData", "AdSupport", "Foundation", "UIKit", "CoreGraphics", "CoreText", "MediaPlayer"
s.resources = "Carbon/Carbon/*.{bundle}", "Carbon/Carbon/ReleaseNotes.md", "README.md"
s.libraries = "sqlite3", "z"
s.vendored_frameworks = "Carbon/Carbon/Carbon.framework", "Carbon/Carbon/Parse.framework", "Carbon/Carbon/ParseFacebookUtils.framework", "Carbon/Carbon/ParseUI.framework", "Carbon/Carbon/GoogleMobileAds.framework", "Carbon/Carbon/GoogleInteractiveMediaAds.framework"

s.dependency 'FBAudienceNetwork' , '4.7.0'

end
